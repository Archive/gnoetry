#!/usr/bin/perl -w

my $dump_stream = 0;

while (@ARGV && $ARGV[0] =~ /^\-\-/) {
    my $arg = shift @ARGV;

    $dump_stream = 1 if $arg eq "--dump";
}

###############################################################################

my %terminal = (
		"<period>" => 1,
		"<exclamation>" => 1,
		"<question>" => 1
		);

my $outside=1;

my @token_stream = ();

sub add_token {

    foreach my $token (@_) {
	
	if ($outside == 1) {
	    push (@token_stream, "<start>");
	    $outside = 0;
	}

	push (@token_stream, $token);

	if ($terminal{$token} && $outside == 0) {
	    push (@token_stream, "<stop>");
	    $outside = 1;
	}
    }
}

while (defined (my $line = <>)) {

    chomp $line;

    # Ignore lines that don't contain any lower-case letters.
    # This filters out block-caps chapter headings.
    next unless $line =~ /[a-z]/;

    # Ignore lines that look like chapter headings
    next if $line =~ /^\s*CHAPTER/;

    # Ignore lines starting with stars.  Footnotes?
    next if $line =~ /^\*/;

    # Ignore lines that look like footnotes.
    next if $line =~ /^\s*\d+\)\s*$/;

    # Remove any existing tag-like structures
    $line =~ s/\<[^\>]+\>//g;
    $line =~ s/\[[^\]]+\]//g;

    # Don't confuse the period in common abbreviations with the end of
    # a sentence.
    $line =~ s/(\W|\b)(Mr|Mrs|Dr|St|Jr|Etc)\./$1$2\[\[dot\]\]/ig;

    # Handle apostrophes.
    $line =~ s/(\w)\'(\w)/$1\[\[apo\]\]$2/g;

    # Convert & to "and".
    $line =~ s/\&/ and /g;
        

    # Convert punctuation to tags.

    $line =~ s/(\.\s*){3,}/ \<ellipses\> /g;

    $line =~ s/\-{2,}/ \<dash\> /g;
    $line =~ s/\-\s+/ \<dash\> /g;
    $line =~ s/\s+\-/ \<dash\> /g;

    $line =~ s/\,/ \<comma\> /g;
    $line =~ s/\:/ \<colon\> /g;
    $line =~ s/\;/ \<semicolon\> /g;
    $line =~ s/\./ \<period\> /g;
    $line =~ s/\!+/ \<exclamation\> /g;
    $line =~ s/\?+/ \<question\> /g;

    # Remove stray quotation marks, parens, etc.
    $line =~ tr/\'\"\`\(\)\*//d;
    
    # Remove things that look like footnote marks.
    $line =~ s/\{\d+\}//g;
    $line =~ s/\(\d+\)//g;
    $line =~ s/\[\d+\]//g;

    # Put our apostrophes and abbeviation-dots back in.
    $line =~ s/\[\[apo\]\]/\'/g;
    $line =~ s/\[\[dot\]\]/\./g;

    add_token (split (" ", $line));
}

##############################################################################

### Now collapse adjacent tokens as necessary.

my @old_token_stream = @token_stream;
@token_stream = ();

my %glue_to_next = ( "i" => 1, "a" => 1, "an" => 1, "the" => 1 );
my %span_across = ( "<comma>" => ",_",
		    "<dash>" => "_--_",
		    "<colon>" => ":_",
		    "<semicolon>" => ";_" );

my %conversion = (
		  "<comma>" => "|,",
		  "<period>" => "|.",
		  "<ellipses>" => "|...",
		  "<dash>" => "--",
		  "<colon>" => "|:",
		  "<semicolon>" => "|;",
		  "<question>" => "|?",
		  "<exclamation>" => "|!"
		  );
		  

$outside = 2;
while (@old_token_stream) {
    my $t = shift @old_token_stream;

    if (@old_token_stream) {
	
	if ($glue_to_next{lc $t}
	    && !($old_token_stream[0] =~ /^\</)) {
	    my $nt = $t . "_" . (shift @old_token_stream);
	    add_token ($nt);
	    next;
	}

	if ($span_across{lc $old_token_stream[0]} 
	    && !($t =~ /^\</)) {
	    my $t2 = $span_across{shift @old_token_stream};
	    my $t3 = shift @old_token_stream;
	    
	    my $nt = $t . $t2 . $t3;
	    add_token ($nt);
	    next;
	}

    }

    $t = $conversion{$t} || $t;
    add_token ($t);
}

##############################################################################


# Build token table

my %token_table = (
		   "<start>" => 0,
		   "<stop>"  => 1
		   );
my $tt=2;
foreach my $tok (@token_stream) {

    unless (exists ($token_table{lc $tok})) {
	$token_table{lc $tok} = $tt;
	++$tt;
    }
}

#my $show=0;
#foreach my $tok (@token_stream) {
#    if ($tok eq "a") {
#	$show += 5;
#    }

#    if ($show) {
#	print "$tok ";
#	--$show;
#	print "\n" if $show == 0;
#    }
#}

if ($dump_stream) {

    my $len = 0;

    foreach my $t (@token_stream) {

	if ($len + length($t) > 77 || $t eq "<start>") {
	    print "\n";
	    print "\n" if $t eq "<start>";
	    $len = 0;
	}

	if ($len != 0) {
	    print " ";
	    ++$len;
	}

	print "$t";
	$len += length($t);
    }
    print "\n";
    
    exit;
}

##############################################################################


print "Format: Gnoetry/0.1\n";
print "Title: Unknown\n";
print "Author: Unknown\n";
print "Source: Project Gutenberg\n";
print "Lingua: en\n";
print "Tokens: $tt\n";
print "Length: ", 1+$#token_stream, "\n";

print "\n";

my @tks = ();
while (my ($k, $v) = each %token_table) {
    push (@tks, [$v, $k]);
}


@tks = sort { $a->[0] <=> $b->[0] } @tks;
foreach (@tks) {
    my ($num, $tok) = @$_;
#    $tok =~ s/^i$/I/g;
    $tok =~ s/(\b|_)i(\b|_)/$1I$2/g;
#    $tok =~ s/_i\b/_I/g;
    printf ("%x %s\n", $num, $tok);
}

my $str = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
die unless length ($str) == 62;

sub num2code {
    my $num = shift;

    return "[" if $num == 0;
    return "]" if $num == 1;

    my $x = $num % 62;
    $num = ($num - $x) / 62;

    my $y = $num % 62;
    $num = ($num - $y) / 62;

    my $z = $num;

    substr ($str, $z, 1) . substr ($str, $y, 1) . substr ($str, $x, 1);
}

my $i=0;
foreach my $token (@token_stream) {
    if (exists $token_table{lc $token}) {
	my $str = num2code($token_table{lc $token});
	print $str;
	$i += length($str);
	if ($i > 77) {
	    print "\n";
	    $i = 0;
	}
    } else {
	print "\nProblem with [$token]\n";
	die;
    }
}
print "\n";


