
my $seen_gut = 0;
my $mode = 0;
my $count = 0;

while (defined (my $line = <>)) {
    chomp $line;

    $seen_gut = $seen_gut || ($line =~ /gutenberg/i);

    $line =~ tr/\r//d;

    $mode = 0 if $line =~ /\*{4}The Project Gutenberg Etext/;
    $line = "" if $line =~ /\betext\b/i;
    $line = "" if $line =~ /email/i;

    $line = "" if $line =~ /^\s*CHAPTER/;
    $line = "" if $line =~ /^\s*PART/;
    $line = "" if $line =~ /^[A-Z\t ]*$/;
    $line = "" if $line =~ /^\s*[IXV]+\.\s*$/;

    if ($mode && $line) {
	print $line, "\n" if $count > 1;
	++$count;
    }

    if ($mode == 0 && $line =~ /\*END\*THE SMALL PRINT/) {
	while (defined (my $line = <>) && !$line =~ /\b[a-z]+\b/) { 
	    print "<$line>\n";
	}
	$mode = 1;
    }
}

if (!$seen_gut) {
    print STDERR "Are you sure that was a Project Gutenberg file?\n";
}
