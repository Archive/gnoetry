/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * text-dialog.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <gnome.h>
#include "gnoetry-text-select.h"
#include "text.h"
#include "text-dialog.h"

void
show_text_dialog (void)
{
  GtkWidget *dialog;
  GtkWidget *text_select;
  GtkWidget *swin;
  gint rv;
  gint active_count = 0;

  dialog = gnome_dialog_new (_("Select Our Texts"),
			     GNOME_STOCK_BUTTON_OK,
			     GNOME_STOCK_BUTTON_CANCEL,
			     NULL);
  gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
  gtk_window_set_policy (GTK_WINDOW (dialog), TRUE, TRUE, FALSE);

  swin = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (swin),
				  GTK_POLICY_NEVER,
				  GTK_POLICY_AUTOMATIC);

  text_select = gtk_frame_new (NULL);
  gtk_container_add (GTK_CONTAINER (text_select),
		     gnoetry_text_select_new ());

  gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (swin),
					 text_select);

  gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog)->vbox),
		      swin, TRUE, TRUE, 0);

  gtk_widget_set_usize (GTK_WIDGET (swin), 575, 500);

  gtk_widget_show_all (dialog);

  text_info_save_active_flags ();
  
  while (active_count == 0) {
    
    rv = gnome_dialog_run (GNOME_DIALOG (dialog));

    if (rv == 0)
      text_info_declare_from_active_flags ();
    else if (rv == 1)
      text_info_restore_active_flags ();
    else {
      /* The dialog was closed by some other means. */

      /* If we close the dialog by some other means when it pops up
	 at start-up, just exit. */
      if (gtk_main_level () == 0)
	exit (0);

      text_info_restore_active_flags ();
      return;
    }

    active_count = text_info_count_active ();

    if (active_count == 0) {
      GtkWidget *err_dialog;

      err_dialog = gnome_error_dialog (_("You must select at least one text."));
      gnome_dialog_run (GNOME_DIALOG (err_dialog));
    }
  }

  if (rv >= 0)
    gtk_widget_destroy (dialog);

}




/* $Id$ */
