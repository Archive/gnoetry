/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * gnoetry-text-info-viewer.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "gnoetry-text-info-viewer.h"

static GtkObjectClass *parent_class = NULL;

enum {
  ARG_0
};

enum {
  LAST_SIGNAL
};

static guint gnoetry_text_info_viewer_signals[LAST_SIGNAL] = { 0 };

static void
gnoetry_text_info_viewer_get_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
gnoetry_text_info_viewer_set_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
gnoetry_text_info_viewer_destroy (GtkObject *obj)
{
  if (parent_class->destroy)
    parent_class->destroy (obj);
}

static void
gnoetry_text_info_viewer_finalize (GtkObject *obj)
{
  GnoetryTextInfoViewer *x = GNOETRY_TEXT_INFO_VIEWER(obj);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
gnoetry_text_info_viewer_class_init (GnoetryTextInfoViewerClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;

  parent_class = gtk_type_class (GTK_TYPE_HBOX);

  object_class->get_arg = gnoetry_text_info_viewer_get_arg;
  object_class->set_arg = gnoetry_text_info_viewer_set_arg;
  object_class->destroy = gnoetry_text_info_viewer_destroy;
  object_class->finalize = gnoetry_text_info_viewer_finalize;

#if 0
  /* Signal definition template */
  gnoetry_text_info_viewer_signals[CHANGED] =
    gtk_signal_new ("changed",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GnoetryTextInfoViewerClass, changed),
                    gtk_marshal_NONE__NONE, GTK_TYPE_NONE, 0);
#endif

  gtk_object_class_add_signals (object_class, gnoetry_text_info_viewer_signals,
                                LAST_SIGNAL);
}

static void
gnoetry_text_info_viewer_init (GnoetryTextInfoViewer *obj)
{

}

GtkType
gnoetry_text_info_viewer_get_type (void)
{
  static GtkType gnoetry_text_info_viewer_type = 0;
  if (!gnoetry_text_info_viewer_type) {
    static const GtkTypeInfo gnoetry_text_info_viewer_info = {
      "GnoetryTextInfoViewer",
      sizeof (GnoetryTextInfoViewer),
      sizeof (GnoetryTextInfoViewerClass),
      (GtkClassInitFunc)gnoetry_text_info_viewer_class_init,
      (GtkObjectInitFunc)gnoetry_text_info_viewer_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    gnoetry_text_info_viewer_type = gtk_type_unique (GTK_TYPE_HBOX, &gnoetry_text_info_viewer_info);
  }
  return gnoetry_text_info_viewer_type;
}

GtkWidget *
gnoetry_text_info_viewer_new (void)
{
  return GTK_WIDGET (gtk_type_new (gnoetry_text_info_viewer_get_type ()));
}
