/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * text.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_TEXT_H
#define _INC_TEXT_H

#include <glib.h>
#include <stdio.h>
#include "words.h"

typedef struct _TextInfo TextInfo;
typedef struct _Text Text;

struct _TextInfo {

  gchar *filename;

  gint format_major_version, format_minor_version;

  gchar *title;
  gchar *author;
  gchar *sort_author;
  gchar *source;
  gchar *copyright;

  gchar lingua[3];

  gint token_count;
  gint text_length;

  guint32 checksum;

  gboolean active;
  gboolean saved_active;

  Text *text;
};

struct _Text {

  TextInfo *info;

  gchar **token_table;
  token_t *token_stream;
};


void text_info_free (TextInfo *);
void text_free (Text *);

void text_info_dump (TextInfo *);
void text_dump (Text *);

TextInfo *text_info_load (const gchar *file);

#define text_info_filename(ti) ((const gchar *)(ti)->filename)
#define text_info_format_major_version(ti) ((ti)->format_major_version)
#define text_info_format_minor_version(ti) ((ti)->format_minor_version)
#define text_info_title(ti) ((const gchar *)(ti)->title)
#define text_info_author(ti) ((const gchar *)(ti)->author)
#define text_info_source(ti) ((const gchar *)(ti)->source)
#define text_info_copyright(ti) ((const gchar *)(ti)->copyright)
#define text_info_lingua(ti) ((const gchar *)(ti)->lingua)
#define text_info_token_count(ti) ((ti)->token_count)
#define text_info_text_length(ti) ((ti)->text_length)
#define text_info_active(ti) ((ti)->active)
Text *text_info_text (TextInfo *info);

void text_info_locate_files (const gchar *path);
void text_info_foreach_file (void (*fn) (TextInfo *, gpointer), gpointer);
gint text_info_count (void);
gint text_info_count_active (void);

void text_info_save_active_flags (void);
void text_info_restore_active_flags (void);
void text_info_declare_from_active_flags (void);

#endif /* _INC_TEXT_H */

/* $Id$ */
