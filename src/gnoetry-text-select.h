/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * gnoetry-text-select.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GNOETRY_TEXT_SELECT_H
#define _INC_GNOETRY_TEXT_SELECT_H

#include <gnome.h>

typedef struct _GnoetryTextSelect GnoetryTextSelect;
typedef struct _GnoetryTextSelectClass GnoetryTextSelectClass;

struct _GnoetryTextSelect {
  GtkHBox parent;
};

struct _GnoetryTextSelectClass {
  GtkHBoxClass parent_class;
};

#define GNOETRY_TYPE_TEXT_SELECT (gnoetry_text_select_get_type ())
#define GNOETRY_TEXT_SELECT(obj) (GTK_CHECK_CAST((obj),GNOETRY_TYPE_TEXT_SELECT,GnoetryTextSelect))
#define GNOETRY_TEXT_SELECT0(obj) ((obj) ? (GNOETRY_TEXT_SELECT(obj)) : NULL)
#define GNOETRY_TEXT_SELECT_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GNOETRY_TYPE_TEXT_SELECT,GnoetryTextSelectClass))
#define GNOETRY_IS_TEXT_SELECT(obj) (GTK_CHECK_TYPE((obj), GNOETRY_TYPE_TEXT_SELECT))
#define GNOETRY_IS_TEXT_SELECT0(obj) (((obj) == NULL) || (GNOETRY_IS_TEXT_SELECT(obj)))
#define GNOETRY_IS_TEXT_SELECT_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GNOETRY_TYPE_TEXT_SELECT))

#define g_return_unless_is_gnoetry_text_select(x) (g_return_if_fail((x)&&GNOETRY_IS_TEXT_SELECT((x))))
#define g_return_val_unless_is_gnoetry_text_select(x,y) (g_return_val_if_fail((x)&&GNOETRY_IS_TEXT_SELECT((x)),(y)))

GtkType gnoetry_text_select_get_type (void);

void gnoetry_text_select_construct (GnoetryTextSelect *);
GtkWidget *gnoetry_text_select_new (void);

#endif /* _INC_GNOETRY_TEXT_SELECT_H */

/* $Id$ */
