/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * gnoetry-line-view.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "gnoetry-line-view.h"

static GtkObjectClass *parent_class = NULL;

enum {
  ARG_0
};

static void
gnoetry_line_view_get_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
gnoetry_line_view_set_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
gnoetry_line_view_destroy (GtkObject *obj)
{
  if (parent_class->destroy)
    parent_class->destroy (obj);
}

static void
gnoetry_line_view_finalize (GtkObject *obj)
{
  GnoetryLineView *x = GNOETRY_LINE_VIEW(obj);

  if (x->linesig)
    gtk_signal_disconnect (GTK_OBJECT (x->line), x->linesig);
  x->linesig = 0;

  gtk_object_unref (GTK_OBJECT (x->line));

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
gnoetry_line_view_class_init (GnoetryLineViewClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;

  parent_class = gtk_type_class (GTK_TYPE_HBOX);

  object_class->get_arg = gnoetry_line_view_get_arg;
  object_class->set_arg = gnoetry_line_view_set_arg;
  object_class->destroy = gnoetry_line_view_destroy;
  object_class->finalize = gnoetry_line_view_finalize;

}

static void
gnoetry_line_view_init (GnoetryLineView *obj)
{

}

GtkType
gnoetry_line_view_get_type (void)
{
  static GtkType gnoetry_line_view_type = 0;
  if (!gnoetry_line_view_type) {
    static const GtkTypeInfo gnoetry_line_view_info = {
      "GnoetryLineView",
      sizeof (GnoetryLineView),
      sizeof (GnoetryLineViewClass),
      (GtkClassInitFunc)gnoetry_line_view_class_init,
      (GtkObjectInitFunc)gnoetry_line_view_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    gnoetry_line_view_type = gtk_type_unique (GTK_TYPE_HBOX, &gnoetry_line_view_info);
  }
  return gnoetry_line_view_type;
}

static void
changed_cb (GnoetryLine *line, GtkLabel *label)
{
  const gchar *str = gnoetry_line_string (line);

  gtk_label_set_text (label, str ? str : "");
}

static void
button_press_cb (GtkWidget *w, GdkEventButton *ev, gpointer user_data)
{
  if (ev->state & GDK_SHIFT_MASK)
    gnoetry_line_regenerate_related (GNOETRY_LINE (user_data));
  else
    gnoetry_line_generate (GNOETRY_LINE (user_data));
}

void
gnoetry_line_view_construct (GnoetryLineView *view, GnoetryLine *line)
{
  GtkWidget *pm;
  GtkWidget *b;
  GtkWidget *txt;

  g_return_unless_is_gnoetry_line_view (view);
  g_return_unless_is_gnoetry_line (line);
  g_return_if_fail (view->line == NULL);

  view->line = line;
  gtk_object_ref (GTK_OBJECT (line));

  pm = gnome_stock_new_with_icon (GNOME_STOCK_MENU_REDO);
  b = gtk_button_new ();
  gtk_container_add (GTK_CONTAINER (b), pm);

  txt = gtk_label_new (gnoetry_line_string (view->line));
  if (view->line->line_can_be_long)
    gtk_label_set_line_wrap (GTK_LABEL (txt), TRUE);

  gtk_signal_connect (GTK_OBJECT(b),
		      "button_press_event",
		      GTK_SIGNAL_FUNC (button_press_cb),
		      view->line);

  view->linesig = 
    gtk_signal_connect (GTK_OBJECT (view->line),
			"changed",
			GTK_SIGNAL_FUNC (changed_cb),
			txt);

  gtk_box_pack_start (GTK_BOX (view), b, FALSE, FALSE, 4);
  gtk_box_pack_start (GTK_BOX (view), txt, TRUE, TRUE, 4);

  gtk_widget_show_all (b);
  gtk_widget_show_all (txt);
  
}

GtkWidget *
gnoetry_line_view_new (GnoetryLine *line)
{
  GnoetryLineView* view;

  view = GNOETRY_LINE_VIEW (gtk_type_new (gnoetry_line_view_get_type ()));
  gnoetry_line_view_construct (view, line);

  return GTK_WIDGET (view);

}
