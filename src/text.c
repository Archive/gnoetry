/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * text.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <gtk/gtk.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <zlib.h>
#include "text.h"

void
text_info_free (TextInfo *info)
{
  if (info != NULL) {
    g_free (info->filename);
    g_free (info->title);
    g_free (info->author);
    g_free (info->sort_author);
    g_free (info->source);
    g_free (info->copyright);
    g_free (info);
  }
}

void
text_free (Text *text)
{
  if (text != NULL) {
    gsize i;

    for (i=0; i<text->info->token_count; ++i)
      g_free (text->token_table[i]);

    g_free (text->token_table);
    text->token_table = NULL;
    
    g_free (text->token_stream);
    text->token_stream = NULL;

    text_info_free (text->info);
    text->info = NULL;

    g_free (text);
  }
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

void
text_info_dump (TextInfo *info)
{
  g_print (" Filename: %s\n", info->filename);
  g_print ("   Format: %d.%d\n",
	   info->format_major_version, info->format_minor_version);
  g_print ("    Title: %s\n", info->title);
  g_print ("   Author: %s\n", info->author);
  g_print ("   Source: %s\n", info->source);
  g_print ("Copyright: %s\n", info->copyright);
  g_print (" Language: %s\n", info->lingua);
  g_print ("   Tokens: %d\n", info->token_count);
  g_print ("   Length: %d\n", info->text_length);
}

void
text_dump (Text *text)
{
  gsize i;
  for (i=0; i<text->info->text_length; ++i) {
    gchar *w = text->token_table[text->token_stream[i]];
    g_print ("[%s]", w);
    if (strcmp (w, "<stop>") == 0) g_print ("\n");
  }
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

static gboolean
line_is_empty (const gchar *str)
{
  while (*str) {
    if (!isspace ((gint) *str) && *str != '\n')
      return FALSE;
    ++str;
  }
  return TRUE;
}

static TextInfo *
text_info_stream_load (gzFile in)
{
  gchar buffer[4096];
  gint bufferlen = 4096;
  TextInfo *info = NULL;
  gboolean first_line = TRUE;
  gchar *c;

  g_return_val_if_fail (in, NULL);

  while (gzgets (in, buffer, bufferlen) && !line_is_empty (buffer)) {

    gchar *key = buffer;
    gchar *val;
    gint i;

    for (i=0; buffer[i] && buffer[i] != '\n' && i < bufferlen; ++i);
    if (i < bufferlen)
      buffer[i] = '\0';

    if (*key && isspace ((gint) *key))
      ++key;

    val = key;
    while (*val && *val != ':')
      ++val;
    if (*val == ':') {
      *val = '\0';
      ++val;
      while (*val && isspace ((gint) *val))
	++val;
    }

    /* Our first line needs to be the format line. */
    if (first_line) {
      if (g_strcasecmp (key, "Format")) 
	return NULL;
      else
	info = g_new0 (TextInfo, 1);
      first_line = FALSE;
    }

    if (*key && *val) {
    
      if (!g_strcasecmp (key, "Format")) {
	gint maj, min;

	if (sscanf (val, "Gnoetry/%d.%d", &maj, &min) == 2) {
	  info->format_major_version = maj;
	  info->format_minor_version = min;
	} else {
	  g_message ("Ill-formed format \"%s\"", val);
	}
	
      } else if (!g_strcasecmp (key, "Title")) {

	info->title = g_strdup (val);
	
      } else if (!g_strcasecmp (key, "Author")) {

	info->author = g_strdup (val);
	
      } else if (!g_strcasecmp (key, "AuthorSort")) {

	info->sort_author = g_strdup (val);

      } else if (!g_strcasecmp (key, "Source")) {

	info->source = g_strdup (val);
	
      } else if (!g_strcasecmp (key, "Copyright")) {

	info->copyright = g_strdup (val);
	
      } else if (!g_strcasecmp (key, "Lingua")) {

	strncpy (info->lingua, val, 2);
	info->lingua[2] = '\0';
	
      } else if (!g_strcasecmp (key, "Tokens")) {

	info->token_count = atoi (val);
	
      } else if (!g_strcasecmp (key, "Length")) {

	info->text_length = atoi (val);

      } else {
	g_message ("Unknown key/value pair: \"%s\", \"%s\"", key, val);
      }

    } else {

      if (*key && !*val)
	g_message ("Key \"%s\" has empty value.", key);

    }

  }

  if (info == NULL)
    return NULL;

  /* Compute checksum */
  info->checksum = 0xdeadd00d;
  
  c = info->author;
  while (c && *c) {
    info->checksum = 17 * info->checksum + (guint32)*c;
    ++c;
  }

  c = info->title;
  while (c && *c) {
    info->checksum = 17 * info->checksum + (guint32)*c;
    ++c;
  }

  c = info->source;
  while (c && *c) {
    info->checksum = 17 * info->checksum + (guint32)*c;
    ++c;
  }

  c = info->copyright;
  while (c && *c) {
    info->checksum = 17 * info->checksum + (guint32)*c;
    ++c;
  }

  info->checksum = 17 * info->checksum + (guint32)info->token_count;
  info->checksum = 17 * info->checksum + (guint32)info->text_length;
  
  return info;
}

TextInfo *
text_info_load (const gchar *filename)
{
  FILE *in;
  TextInfo *info;

  g_return_val_if_fail (filename, NULL);

  in = gzopen (filename, "r");
  if (in == NULL)
    return NULL;

  info = text_info_stream_load (in);
  if (info)
    info->filename = g_strdup (filename);

  return info;
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

static void
zero_cb (GtkWidget *w, gpointer user_data)
{
  g_message ("Bang!");
  *(GtkWidget **)user_data = NULL;
}

static void
text_progress_window (TextInfo *info, GtkWidget **target)
{
  GtkWidget *win;
  GtkWidget *box;
  GtkWidget *prog;
  GtkWidget *frame;
  gchar *s;

  g_return_if_fail (info);
  g_return_if_fail (target);

  win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_position (GTK_WINDOW (win), GTK_WIN_POS_CENTER);
  gtk_window_set_title (GTK_WINDOW (win), "Loading");

  frame = gtk_frame_new (NULL);
  
  box = gtk_vbox_new (FALSE, 3);
  
  s = g_strdup_printf ("Loading \"%s\"", text_info_title (info));
  gtk_box_pack_start (GTK_BOX (box), gtk_label_new (s), TRUE, TRUE, 2);
  g_free (s);

  s = g_strdup_printf ("by %s", text_info_author (info));
  gtk_box_pack_start (GTK_BOX (box), gtk_label_new (s), TRUE, TRUE, 2);
  g_free (s);

  prog = gtk_progress_bar_new ();
  gtk_progress_bar_set_bar_style (GTK_PROGRESS_BAR (prog),
				  GTK_PROGRESS_CONTINUOUS);
  gtk_box_pack_start (GTK_BOX (box), prog, TRUE, TRUE, 2);
  gtk_object_set_data (GTK_OBJECT (win), "prog", prog);

  gtk_container_add (GTK_CONTAINER (frame), box);
  gtk_container_add (GTK_CONTAINER (win), frame);

  gtk_signal_connect (GTK_OBJECT (win),
		      "delete_event",
		      GTK_SIGNAL_FUNC (zero_cb),
		      target);

  gtk_widget_show_all (win);

  while (gtk_events_pending ())
    gtk_main_iteration ();

  *target = win;
}

static void
text_progress_update (GtkWidget *prog_win, gint N, gint total)
{
  GtkProgress *prog;

  if (prog_win == NULL)
    return;
  
  prog = GTK_PROGRESS (gtk_object_get_data (GTK_OBJECT (prog_win), "prog"));
  gtk_progress_set_percentage (prog, N / (double)total);

  while (gtk_events_pending ())
    gtk_main_iteration ();
}

static void
text_progress_done (GtkWidget *prog_win)
{
  if (prog_win == NULL)
    return;

  gtk_widget_destroy (prog_win);
  while (gtk_events_pending ())
    gtk_main_iteration ();
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

static gint
char2num (gchar x)
{
  if ('0' <= x && x <= '9')
    return (gint)x - (gint)'0';
  if ('a' <= x && x <= 'z')
    return 10 + (gint)x - (gint)'a';
  if ('A' <= x && x <= 'Z')
    return 36 + (gint)x - (gint)'A';

  g_assert_not_reached ();
  return -1;
}

static gint 
chars2code (const gchar *c)
{
  return char2num (c[0])*62*62 + char2num (c[1])*62 + char2num (c[2]);
}

static Text *
get_text_body_format_0_1 (gzFile in, TextInfo *info)
{
  Text *text;
  gint token_count=-1, stream_length=-1;
  gint i;

  gint total=0, run=0;
  GtkWidget *progwin;

  gchar buffer[4096];
  gint bufferlen = 4096;

  g_return_val_if_fail (in, NULL);
  g_return_val_if_fail (info, NULL);

  token_count = info->token_count;
  stream_length = info->text_length;

  text_progress_window (info, &progwin);
  total = token_count + stream_length;
  
  g_return_val_if_fail (token_count > 0, NULL);
  g_return_val_if_fail (stream_length > 0, NULL);

  text = g_new0 (Text, 1);

  text->token_table = g_new0 (gchar *, token_count);
  text->token_stream = g_new0 (token_t, stream_length);

  for (i=0; i<token_count; ++i) {
    gint code=-1;
    gchar word[128];
    gchar *c;
    if (gzgets (in, buffer, bufferlen)
	&& sscanf (buffer, "%x %s", &code, word) == 2) {
      
      g_assert (0 <= code && code < token_count);
      g_assert (text->token_table[code] == NULL);

      c = word;
      while (*c) {
	if (*c == '_') *c = ' ';
	++c;
      }

      text->token_table[code] = g_strdup (word);

      ++run;
      if ((run & 0xff) == 0)
	text_progress_update (progwin, run, total);
      
    } else {
      g_error ("Read error on \"%s\"", info->filename);
      return NULL;
    }
  }

  i = 0;
  while (gzgets (in, buffer, bufferlen)) {
    gchar *p = buffer;
    while (*p && *p != '\n') {
      gint code;
      if (*p == '[') {
	code = 0;
	++p;
      } else if (*p == ']') {
	code = 1;
	++p;
      } else {
	code = chars2code (p);
	p += 3;
      }

      g_assert (0 <= code && code < token_count);
      text->token_stream[i] = code;
      ++i;

      ++run;
      if ((run & 0xfff) == 0)
	text_progress_update (progwin, run, total);
    }
  }
  g_assert (i == stream_length);

  gzclose (in);

  text_progress_done (progwin);

  return text;  
}

static void
text_load (TextInfo *info)
{
  gzFile in;
  TextInfo *file_info;
  guint32 cs;
  gboolean trying = FALSE;

  g_return_if_fail (info);

  if (info->text)
    return;

  in = gzopen (text_info_filename (info), "r");
  if (!in)
    return;

  file_info = text_info_stream_load (in);
  cs = file_info->checksum;
  text_info_free (file_info);
  file_info = NULL;

  g_assert (cs == info->checksum);
  
  if (info->format_major_version == 0 
      && info->format_minor_version == 1) {
    trying = TRUE;
    info->text = get_text_body_format_0_1 (in, info);
  }

  if (!trying) {
    g_warning ("Gnoetry %s doesn't support source text format version %d.%d",
	       VERSION,
	       info->format_major_version,
	       info->format_minor_version);
  } else if (info->text == NULL) {
    g_warning ("Text load failed.");
  }

  if (info->text)
    info->text->info = info;
  
}

static void
text_declare (Text *text)
{
  TokenInfo *prev = NULL;
  TokenInfo *now;
  gsize i;

  if (text == NULL)
    return;

  for (i=0; i<text->info->text_length; ++i) {
    gchar *w = text->token_table[text->token_stream[i]];
    now = declare_token (w);

    if (prev) {
      declare_paring (prev, now);
    }

    prev = now;
  }
}

Text *
text_info_text (TextInfo *info)
{
  g_return_val_if_fail (info, NULL);

  if (!info->text)
    text_load (info);

  return info->text;
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/



static GList *text_info_list = NULL;

static gboolean
valid_filename (const gchar *f)
{
  gint len;

  g_return_val_if_fail (f && *f, FALSE);

  if (!strcmp (f, ".") || !strcmp (f, ".."))
    return FALSE;

  len = strlen (f);
  if (len == 0)
    return FALSE;

  /* Ignore emacs back-up files. */
  if (f[len-1] == '~')
    return FALSE;
  if (f[0] == '#' && f[len-1] == '#')
    return FALSE;

  return TRUE;
  
}

void
text_info_locate_files (const gchar *path)
{
  DIR *d;
  struct dirent *f;

  d = opendir (path);
  if (d == NULL)
    return;

  while ( (f = readdir (d)) ) {
    if (valid_filename (f->d_name)) {
      gchar *full = g_strdup_printf ("%s%c%s",
				     path, G_DIR_SEPARATOR, f->d_name);
      struct stat buf;

      if (stat (full, &buf) >= 0) {

	if (S_ISDIR (buf.st_mode)) {

	  /* Descend recursively. */
	  text_info_locate_files (full);

	} else {

	  TextInfo *info = text_info_load (full);
	  if (info)
	    text_info_list = g_list_append (text_info_list, info);

	}
      }

      g_free (full);
    }
  }
}

static const gchar *
last_word (const gchar *str)
{
  const gchar *last_whitespace = str;
  while (*str) {
    if (isspace ((gint) *str))
      last_whitespace = str;
    ++str;
  }
  while (isspace ((gint) *last_whitespace))
    ++last_whitespace;
  return last_whitespace;
}

static gint
text_sort (gconstpointer a, gconstpointer b)
{
  TextInfo *ta = (TextInfo *) a;
  TextInfo *tb = (TextInfo *) b;
  const gchar *sa;
  const gchar *sb;

  if (ta->sort_author)
    sa = ta->sort_author;
  else
    sa = last_word (ta->author);

  if (tb->sort_author)
    sb = tb->sort_author;
  else
    sb = last_word (tb->author);
  
  return g_strcasecmp (sa, sb);
}

void
text_info_foreach_file (void (*fn) (TextInfo *, gpointer),
			gpointer user_data)
{
  GList *iter;

  g_return_if_fail (fn);

  text_info_list = g_list_sort (text_info_list, text_sort);

  for (iter = text_info_list; iter; iter = g_list_next (iter)) {
    fn ((TextInfo *)iter->data, user_data);
  }
}

static void
count_cb (TextInfo *info, gpointer ptr)
{
  gint *n = (gint *) ptr;
  ++*n;
}

static void
count_active_cb (TextInfo *info, gpointer ptr)
{
  gint *n = (gint *) ptr;
  if (info->active)
    ++*n;
}

gint
text_info_count (void)
{
  gint n = 0;

  text_info_foreach_file (count_cb, &n);
  return n;
}

gint
text_info_count_active (void)
{
  gint n = 0;

  text_info_foreach_file (count_active_cb, &n);
  return n;
}


/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

static void
save_flag_cb (TextInfo *info, gpointer ptr)
{
  info->saved_active = info->active;
}

static void
restore_flag_cb (TextInfo *info, gpointer ptr)
{
  info->active = info->saved_active;
}

void
text_info_save_active_flags (void)
{
  text_info_foreach_file (save_flag_cb, NULL);
}

void
text_info_restore_active_flags (void)
{
  text_info_foreach_file (restore_flag_cb, NULL);
}

static void
declare_cb (TextInfo *info, gpointer ptr)
{
  if (info->active)
    text_declare (text_info_text (info));
}

void
text_info_declare_from_active_flags (void)
{
  reset_token_info ();
  text_info_foreach_file (declare_cb, NULL);
}


/* $Id$ */
