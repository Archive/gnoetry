/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * gnoetry-form.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <ctype.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include "text.h"
#include "gnoetry-form.h"

static GtkObjectClass *parent_class = NULL;

enum {
  ARG_0
};

enum {
  CHANGED,
  LAST_SIGNAL
};

static guint gnoetry_form_signals[LAST_SIGNAL] = { 0 };

static void
gnoetry_form_get_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
gnoetry_form_set_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
gnoetry_form_finalize (GtkObject *obj)
{
  GnoetryForm *form = GNOETRY_FORM (obj);
  GList *i;

  g_free (form->form_name);
  g_free (form->form_params);

  for (i=form->lines; i; i=g_list_next(i)) {
    if (i->data)
      gtk_object_unref (GTK_OBJECT (i->data));
  }
  g_list_free (form->lines);
  form->lines = NULL;

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
gnoetry_form_class_init (GnoetryFormClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;

  parent_class = gtk_type_class (GTK_TYPE_OBJECT);

  object_class->get_arg = gnoetry_form_get_arg;
  object_class->set_arg = gnoetry_form_set_arg;
  object_class->finalize = gnoetry_form_finalize;

  gnoetry_form_signals[CHANGED] =
    gtk_signal_new ("changed",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GnoetryFormClass, changed),
                    gtk_marshal_NONE__NONE, GTK_TYPE_NONE, 0);

  gtk_object_class_add_signals (object_class, gnoetry_form_signals,
                                LAST_SIGNAL);
}

static void
gnoetry_form_init (GnoetryForm *obj)
{

}

GtkType
gnoetry_form_get_type (void)
{
  static GtkType gnoetry_form_type = 0;
  if (!gnoetry_form_type) {
    static const GtkTypeInfo gnoetry_form_info = {
      "GnoetryForm",
      sizeof (GnoetryForm),
      sizeof (GnoetryFormClass),
      (GtkClassInitFunc)gnoetry_form_class_init,
      (GtkObjectInitFunc)gnoetry_form_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    gnoetry_form_type = gtk_type_unique (GTK_TYPE_OBJECT, &gnoetry_form_info);
  }
  return gnoetry_form_type;
}

GnoetryForm *
gnoetry_form_new (void)
{
  return GNOETRY_FORM (gtk_type_new (gnoetry_form_get_type ()));
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

gint
gnoetry_form_lines (GnoetryForm *form)
{
  GList *i;
  gint N=0;

  g_return_val_unless_is_gnoetry_form (form, 0);

  for (i=form->lines; i; i=g_list_next(i)) {
    if (i->data)
      ++N;
  }

  return N;
}

gint
gnoetry_form_breaks (GnoetryForm *form)
{
  GList *i;
  gint N=0;

  g_return_val_unless_is_gnoetry_form (form, 0);

  for (i=form->lines; i; i=g_list_next(i)) {
    if (i->data == NULL)
      ++N;
  }

  return N;
}

void
gnoetry_form_set_id (GnoetryForm *form,
		     const gchar *name,
		     const gchar *params)
{
  g_return_unless_is_gnoetry_form (form);
  g_free (form->form_name);
  form->form_name = g_strdup (name);
  g_free (form->form_params);
  form->form_params = g_strdup (params);
}

const gchar *
gnoetry_form_name (GnoetryForm *form)
{
  g_return_val_unless_is_gnoetry_form (form, NULL);
  return form->form_name;
}

const gchar *
gnoetry_form_params (GnoetryForm *form)
{
  g_return_val_unless_is_gnoetry_form (form, NULL);
  return form->form_params;  
}

void
gnoetry_form_clear (GnoetryForm *form)
{
  GList *i;

  g_return_unless_is_gnoetry_form (form);

  for (i=form->lines; i; i=g_list_next(i)) {
    if (i->data)
      gtk_object_unref (GTK_OBJECT (i->data));
  }
  g_list_free (form->lines);
  form->lines = NULL;

  gtk_signal_emit (GTK_OBJECT (form), gnoetry_form_signals[CHANGED]);
}

void
gnoetry_form_append_line (GnoetryForm *form, GnoetryLine *line)
{
  g_return_unless_is_gnoetry_form (form);
  g_return_unless_is_gnoetry_line (line);

  form->lines = g_list_append (form->lines, line);
  gtk_object_ref (GTK_OBJECT (line));
  gtk_object_sink (GTK_OBJECT (line));
}

void
gnoetry_form_append_break (GnoetryForm *form)
{
  g_return_unless_is_gnoetry_form (form);
  
  /* Don't allow leading breaks or adjacent breaks. */
  if (form && g_list_last (form->lines)->data)
    form->lines = g_list_append (form->lines, NULL);
}

void
gnoetry_form_recalc (GnoetryForm *form)
{
  GList *i;
  g_return_unless_is_gnoetry_form (form);

  for (i=form->lines; i; i=g_list_next (i))
    if (i->data)
      gnoetry_line_clear (GNOETRY_LINE (i->data));

  for (i=form->lines; i; i=g_list_next (i))
    if (i->data)
      gnoetry_line_generate (GNOETRY_LINE (i->data));
}

void
gnoetry_form_foreach_line (GnoetryForm *form,
			   void (*fn) (GnoetryLine *, gpointer),
			   gpointer user_data)
{
  GList *i;

  g_return_unless_is_gnoetry_form (form);
  g_return_if_fail (fn);

  for (i=form->lines; i; i=g_list_next (i))
    fn (GNOETRY_LINE0 (i->data), user_data);
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

static void
build_str_cb (GnoetryLine *line, gpointer user_data)
{
  gchar **str = (gchar **) user_data;

  if (*str) {
    gchar *ns;
    ns = g_strconcat (*str,
		      "\n",
		      line ? gnoetry_line_string (line) : "",
		      NULL);
    g_free (*str);
    *str = ns;
  } else {
    *str = line ? g_strdup (gnoetry_line_string (line)) : g_strdup ("");
  }
}

gchar *
gnoetry_form_multiline_string (GnoetryForm *form)
{
  gchar *str = NULL;

  g_return_val_unless_is_gnoetry_form (form, NULL);

  gnoetry_form_foreach_line (form, build_str_cb, &str);

  return str;
}

static void
strv_cb (GnoetryLine *line, gpointer user_data)
{
  gchar ***strvp = (gchar ***) user_data;
  **strvp = g_strdup (line ? gnoetry_line_string (line) : "");
  ++*strvp;
}

gchar **
gnoetry_form_strv (GnoetryForm *form)
{
  gchar **strv;
  gchar **cpy;
  gint N=0;

  g_return_val_unless_is_gnoetry_form (form, NULL);

  N = gnoetry_form_lines (form) + gnoetry_form_breaks (form) + 1;
  cpy = strv = g_new0 (gchar *, N);
  
  gnoetry_form_foreach_line (form, strv_cb, &cpy);

  return strv;
}

gchar *
gnoetry_form_html (GnoetryForm *form)
{
  gchar **strv;
  gchar *old;
  gint i;
  
  g_return_val_unless_is_gnoetry_form (form, NULL);

  strv = gnoetry_form_strv (form);

  if (strv == NULL || strv[0] == NULL)
    return NULL;

  old = strv[0];
  strv[0] = g_strdup_printf ("<html><body><p>%s", strv[0]);
  g_free (old);

  for (i=0; strv[i]; ++i);
  --i;

  old = strv[i];
  strv[i] = g_strdup_printf ("%s</p></body></html", strv[i]);
  g_free (old);

  return g_strjoinv("<br>", strv);
}

static void
latex_text_info_cb (TextInfo *info, gpointer user_data)
{
  FILE *out = user_data;

  if (text_info_active (info))
    fprintf (out, 
	     "%%%% %s / %s\n",
	     text_info_title (info),
	     text_info_author (info));
}

void
gnoetry_form_save_as_latex (GnoetryForm *form)
{
  FILE *out;
  gchar *filename_buf;
  gchar *filename_clean;
  gchar *filename;
  gchar **strv;
  time_t now;
  const struct tm *now_tm;
  gchar now_str[128];
  gchar *src, *dest;
  gint i;

  g_return_unless_is_gnoetry_form (form);

  strv = gnoetry_form_strv (form);
  filename_buf = g_strdup (strv[0]);
  filename_clean = g_malloc0 (strlen (filename_buf)+5);

  g_strdown (filename_buf);
  src = filename_buf;
  dest = filename_clean;

  while (*src) {
    if (isalpha ((gint) *src) || *src == ' ') {
      *dest = *src == ' ' ? '_' : *src;
      ++dest;
    }
    ++src;
  }
  strcpy (dest, ".tex");

  /* If they already exist, just fail silently */
  mkdir ("gnoems", 0777);
  mkdir ("gnoems/latex", 0777);

  filename = g_strdup_printf ("./gnoems/latex/%s", filename_clean);
  out = fopen(filename, "w");
  g_free (filename);
  g_free (filename_buf);
  g_free (filename_clean);

  time (&now);
  now_tm = localtime (&now);
  strftime (now_str, 128, "%B %d, %Y, %X", now_tm);
  
  fprintf (out, "%%%% generated %s\n", now_str);
  fprintf (out, "%%%% form: %s\n", gnoetry_form_name (form));
  fprintf (out, "%%%%\n");
  if (gnoetry_form_params (form))
    fprintf (out, "%%%% params: %s\n", gnoetry_form_params (form));
  text_info_foreach_file (latex_text_info_cb, out);
  fprintf (out, "\n");
  fprintf (out, "\\begin{untitledpoem}{%s}\n", strv[0]);
  for (i=0; strv[i]; ++i)
    if (strv[i][0] == '\0')
      fprintf (out, "\n\\stanzabreak\n\n");
    else
      fprintf (out, "%s\n", strv[i]);
  fprintf (out, "\\end{untitledpoem}\n");
  
  fclose (out);
  g_strfreev (strv);
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

static void
add_fixed_syllable_lines (GnoetryForm *form, gint* syl)
{
  GnoetryLine *line;
  gint i;

  g_return_unless_is_gnoetry_form (form);
  g_return_if_fail (syl);

  for (i=0; syl[i] >= 0; ++i) {

    if (syl[i] > 0) {
      line = gnoetry_line_new ();
      line->min_syl = line->max_syl = syl[i];
      gnoetry_form_append_line (form, line);
    } else {
      gnoetry_form_append_break (form);
    }
  }
}

static void
add_bounded_syllable_lines (GnoetryForm *form, gint *syl)
{
  GnoetryLine *line;
  gint i;

  g_return_unless_is_gnoetry_form (form);
  g_return_if_fail (syl);

  i = 0;
  while (syl[i] >= 0) {

    if (syl[i] == 0) {
      gnoetry_form_append_break (form);
      ++i;
    } else {
      gint min = syl[i];
      gint max = syl[i+1];

      if (min > max) {
	gint t = min;
	min = max;
	max = t;
      }
      
      line = gnoetry_line_new ();
      line->min_syl = min;
      line->max_syl = max;
      gnoetry_form_append_line (form, line);
      
      i += 2;
    }
  }
}

GnoetryForm *
gnoetry_form_new_haiku (void)
{
  GnoetryForm *form = gnoetry_form_new ();
  gint syls[] = { 5, 7, 5, -1 };

  gnoetry_form_set_id (form, "haiku", NULL);

  add_fixed_syllable_lines (form, syls);

  return form;
}

GnoetryForm *
gnoetry_form_new_tanka (void)
{
  GnoetryForm *form = gnoetry_form_new ();
  gint syls[] = { 5, 7, 5, 0, 7, 7, -1 };

  gnoetry_form_set_id (form, "tanka", NULL);

  add_fixed_syllable_lines (form, syls);

  return form;
}

GnoetryForm *
gnoetry_form_new_renga (void)
{
  GnoetryForm *form = gnoetry_form_new ();
  gint syls[] = { 5, 7, 5, 0, 7, 7, 0, 5, 7, 5, -1 };

  gnoetry_form_set_id (form, "renga", NULL);

  add_fixed_syllable_lines (form, syls);

  return form;
}

GnoetryForm *
gnoetry_form_new_5line (void)
{
  GnoetryForm *form = gnoetry_form_new ();
  gint syls[] = { 8, 14,
		  8, 14,
		  5, 9,
		  5, 9,
		  8, 14, 
		  -1 };

  add_bounded_syllable_lines (form, syls);

  return form;
}

static void
rhyming_lines (gint min_syl, gint max_syl, GnoetryLine **a, GnoetryLine **b)
{
  g_return_if_fail (a);
  g_return_if_fail (b);

  *a = gnoetry_line_new ();
  *b = gnoetry_line_new ();

  (*a)->linked_with = *b;
  (*a)->link_type = LINK_END_WORD_RHYME;

  (*b)->linked_with = *a;
  (*b)->link_type = LINK_END_WORD_RHYME;

  (*a)->min_syl = (*b)->min_syl = min_syl;
  (*a)->max_syl = (*b)->max_syl = max_syl;
}

static void
dup_end_word_lines (gint min_syl, gint max_syl, GnoetryLine **a, GnoetryLine **b)
{
  g_return_if_fail (a);
  g_return_if_fail (b);

  *a = gnoetry_line_new ();
  *b = gnoetry_line_new ();

  (*a)->linked_with = *b;
  (*a)->link_type = LINK_END_WORD_MATCH;

  (*b)->linked_with = *a;
  (*b)->link_type = LINK_END_WORD_MATCH;

  (*a)->min_syl = (*b)->min_syl = min_syl;
  (*a)->max_syl = (*b)->max_syl = max_syl;
}

GnoetryForm *
gnoetry_form_new_ABAB (void)
{
  GnoetryForm *form = gnoetry_form_new ();
  GnoetryLine *a1, *b1, *a2, *b2;

  gnoetry_form_set_id (form, "Basic Rhyming", "ABAB scheme");

  rhyming_lines (8, 12, &a1, &a2);
  rhyming_lines (8, 12, &b1, &b2);

  gnoetry_form_append_line (form, a1);
  gnoetry_form_append_line (form, b1);
  gnoetry_form_append_line (form, a2);
  gnoetry_form_append_line (form, b2);

  return form;
}



GnoetryForm *
gnoetry_form_new_ABAB_CDCD (void)
{
  GnoetryForm *form = gnoetry_form_new ();
  GnoetryLine *a1, *b1, *a2, *b2;
  GnoetryLine *c1, *d1, *c2, *d2;

  gnoetry_form_set_id (form, "Basic Rhyming", "ABAB CDCD scheme");

  rhyming_lines (8, 12, &a1, &a2);
  rhyming_lines (8, 12, &b1, &b2);
  rhyming_lines (8, 12, &c1, &c2);
  rhyming_lines (8, 12, &d1, &d2);

  gnoetry_form_append_line (form, a1);
  gnoetry_form_append_line (form, b1);
  gnoetry_form_append_line (form, a2);
  gnoetry_form_append_line (form, b2);

  gnoetry_form_append_break (form);

  gnoetry_form_append_line (form, c1);
  gnoetry_form_append_line (form, d1);
  gnoetry_form_append_line (form, c2);
  gnoetry_form_append_line (form, d2);

  return form;
}

#define IAMBIC_PENTAMETER "u/u/u/u/u/"
GnoetryForm *
gnoetry_form_new_sonnet (void)
{
  GnoetryForm *form = gnoetry_form_new ();
  GnoetryLine *a1, *b1, *a2, *b2;
  gint i;

  gnoetry_form_set_id (form, "Sonnet", NULL);

  for (i=0; i<3; ++i) {
    rhyming_lines (0, 0, &a1, &a2);
    rhyming_lines (0, 0, &b1, &b2);

    gnoetry_line_set_meter (a1, IAMBIC_PENTAMETER);
    gnoetry_line_set_meter (a2, IAMBIC_PENTAMETER);
    gnoetry_line_set_meter (b1, IAMBIC_PENTAMETER);
    gnoetry_line_set_meter (b2, IAMBIC_PENTAMETER);
    
    gnoetry_form_append_line (form, a1);
    gnoetry_form_append_line (form, b1);
    gnoetry_form_append_line (form, a2);
    gnoetry_form_append_line (form, b2);
    
    gnoetry_form_append_break (form);
  }

  rhyming_lines (0, 0, &a1, &a2);
  gnoetry_line_set_meter (a1, IAMBIC_PENTAMETER);
  gnoetry_line_set_meter (a2, IAMBIC_PENTAMETER);
  gnoetry_form_append_line (form, a1);
  gnoetry_form_append_line (form, a2);
    
  return form;
}

GnoetryForm *
gnoetry_form_new_sapphic (void)
{
  GnoetryForm *form = gnoetry_form_new ();
  const gchar *meter[] = { "/u/u/uu/u/u",
			   "/u/u/uu/u/u",
			   "/u/u/uu/u/u",
			   "/uu/u",
			   NULL };
  gint i, j;

  gnoetry_form_set_id (form, "Sapphic", NULL);

  for (j=0; j<5; ++j) {

    if (j)
      gnoetry_form_append_break (form);

    for (i=0; meter[i]; ++i) {
      GnoetryLine *line = gnoetry_line_new ();
      gnoetry_line_set_meter (line, meter[i]);
      gnoetry_form_append_line (form, line);
    }
  }

  return form;
}

GnoetryForm *
gnoetry_form_new_hendecasyllabic (void)
{
  GnoetryForm *form = gnoetry_form_new ();
  gint i;

  gnoetry_form_set_id (form, "Hendecasyllabic", NULL);

  for (i=0; i<13; ++i) {
    GnoetryLine *line = gnoetry_line_new ();
    gnoetry_line_set_meter (line, "///uu/u/u//");
    gnoetry_form_append_line (form, line);
  }

  return form;
}

GnoetryForm *
gnoetry_form_new_blank_verse (gint stanzas, gint lines_per_stanza)
{
  GnoetryForm *form = gnoetry_form_new ();
  gchar *params;
  gint i, j;

  params = g_strdup_printf ("%d stanzas, %d lines per stanza",
			    stanzas, lines_per_stanza);

  gnoetry_form_set_id (form, "Blank Verse", params);

  g_free (params);

  for (i=0; i<stanzas; ++i) {

    if (i)
      gnoetry_form_append_break (form);

    for (j=0; j<lines_per_stanza; ++j) {
      GnoetryLine *line = gnoetry_line_new ();
      gnoetry_line_set_meter (line, IAMBIC_PENTAMETER);
      gnoetry_form_append_line (form, line);
    }
  }

  return form;
}

GnoetryForm *
gnoetry_form_new_blank_verse_4x4 (void)
{
  return gnoetry_form_new_blank_verse (4, 4);
}

GnoetryForm *
gnoetry_form_new_italian_sestet (void)
{
  GnoetryForm *form = gnoetry_form_new ();
  GnoetryLine *lines[6];
  gint i;

  gnoetry_form_set_id (form, "Italian Sestet", NULL);

  rhyming_lines (0, 0, &lines[0], &lines[3]);
  rhyming_lines (0, 0, &lines[1], &lines[4]);
  rhyming_lines (0, 0, &lines[2], &lines[5]);

  for (i=0; i<6; ++i) {
    gnoetry_line_set_meter (lines[i], IAMBIC_PENTAMETER);
    gnoetry_form_append_line (form, lines[i]);
  }

  return form;
}

GnoetryForm *
gnoetry_form_new_sicilian_sestet (void)
{
  GnoetryForm *form = gnoetry_form_new ();
  GnoetryLine *lines[6];
  gint i;

  gnoetry_form_set_id (form, "Sicilian Sestet", NULL);

  for (i=0; i<6; ++i) {
    lines[i] = gnoetry_line_new ();
    lines[i]->link_type = LINK_END_WORD_RHYME;
    gnoetry_line_set_meter (lines[i], IAMBIC_PENTAMETER);
    gnoetry_form_append_line (form, lines[i]);
  }

  lines[2]->linked_with = lines[0];
  lines[4]->linked_with = lines[2];
  lines[0]->linked_with = lines[4];

  lines[3]->linked_with = lines[1];
  lines[5]->linked_with = lines[3];
  lines[1]->linked_with = lines[5];

  return form;
}

GnoetryForm *
gnoetry_form_new_heroic_sestet (void)
{
  GnoetryForm *form = gnoetry_form_new ();
  GnoetryLine *lines[6];
  gint i;

  gnoetry_form_set_id (form, "Heroic Sestet", NULL);

  rhyming_lines (0, 0, &lines[0], &lines[2]);
  rhyming_lines (0, 0, &lines[1], &lines[3]);
  rhyming_lines (0, 0, &lines[4], &lines[5]);

  for (i=0; i<6; ++i) {
    gnoetry_line_set_meter (lines[i], IAMBIC_PENTAMETER);
    gnoetry_form_append_line (form, lines[i]);
  }


  return form;
}


GnoetryForm *
gnoetry_form_new_sestina (void)
{
  GnoetryForm *form = gnoetry_form_new ();
  GnoetryLine *lines[36];
  gint pattern[6][6] = { { 0, 7, 15, 22, 26, 35 }, /* A */
			 { 1, 9, 16, 20, 29, 30 }, /* B */
			 { 2, 11, 12, 19, 27, 34 }, /* C */
			 { 3, 10, 14, 23, 24, 31 }, /* D */
			 { 4, 8, 17, 18, 25, 33 }, /* E */
			 { 5, 6, 13, 21, 28, 32 } /* F */
  };

  gint i, j;

  gnoetry_form_set_id (form, "Sestina", "no envoy");

  for (i=0; i<36; ++i) {
    lines[i] = gnoetry_line_new ();
    if (i && ((i % 6) == 0))
      gnoetry_form_append_break (form);

    lines[i]->link_type = LINK_END_WORD_MATCH;
    lines[i]->min_syl = 4;
    lines[i]->max_syl = 14;
    
    gnoetry_form_append_line (form, lines[i]);
  }

  for (i=0; i<6; ++i) {
    for (j=0; j<6; ++j) {
      gint k=j+1, a, b;
      if (k == 6)
	k = 0;

      a = pattern[i][j];
      b = pattern[i][k];

      if (k)
	lines[MAX(a,b)]->linked_with = lines[MIN(a,b)];
      else
	lines[MIN(a,b)]->linked_with = lines[MAX(a,b)];
    }
  }

  return form;
}

GnoetryForm *
gnoetry_form_new_rant (void)
{
  GnoetryForm *form = gnoetry_form_new ();
  gint i;

  gnoetry_form_set_id (form, "Rant", NULL);

  for (i=0; i<5; ++i) {
    GnoetryLine *line = gnoetry_line_new ();
    line->line_can_be_long = TRUE;
    gnoetry_form_append_line (form, line);
  }
  return form;
}
