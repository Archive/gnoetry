/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * phrase.h
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __PHRASE_H__
#define __PHRASE_H__

#include <glib.h>
#include "words.h"

typedef enum {
  PHRASE_GROW_FORWARD,
  PHRASE_GROW_BACKWARD,
  PHRASE_GROW_RANDOM,
  PHRASE_GROW_LAST
} PhraseGrowType;

enum {
  PHRASE_FLAG_NONE             = 0,
  PHRASE_FLAG_FORCE_HEAD       = 1<<0,
  PHRASE_FLAG_FORCE_TAIL       = 1<<1,
  PHRASE_FLAG_STOP_ON_BOUNDARY = 1<<2,
  PHRASE_FLAG_REQUIRE_STOP     = 1<<3
};

typedef struct _Phrase Phrase;
struct _Phrase {
  GList *word_list;
  gint words;
  gint syllables;
};

typedef void (*PhraseFunction) (TokenInfo *, gpointer);

Phrase    *phrase_new  (void);
Phrase    *phrase_new_head (void);
Phrase    *phrase_new_tail (void);
void       phrase_free (Phrase *);
void       phrase_clear (Phrase *);

gboolean   phrase_empty         (Phrase *);
gboolean   phrase_nonempty      (Phrase *);
gboolean   phrase_is_head       (Phrase *);
gboolean   phrase_is_tail       (Phrase *);
gboolean   phrase_is_sentence   (Phrase *);
gboolean   phrase_contains_stop (Phrase *);

TokenInfo *phrase_first_word (Phrase *);
TokenInfo *phrase_last_word  (Phrase *);
gint       phrase_words      (Phrase *);
gint       phrase_syllables  (Phrase *);

void       phrase_foreach  (Phrase *, PhraseFunction, gpointer user_data);

void       phrase_prepend         (Phrase *, TokenInfo *);
void       phrase_append          (Phrase *, TokenInfo *);
void       phrase_prepend_start   (Phrase *);
void       phrase_append_stop     (Phrase *);
void       phrase_drop_first_word (Phrase *);
void       phrase_drop_last_word  (Phrase *);

gint       phrase_grow (Phrase *p, PhraseGrowType type,
			gint count, gint max_words, gint max_syllables,
			gboolean stop_on_boundary);

Phrase    *phrase_new_predecessor (Phrase *p,
				   gint min_words, gint max_words,
				   gint min_syllables, gint max_syllables,
				   guint flags);

Phrase    *phrase_new_successor (Phrase *p,
				 gint min_words, gint max_words,
				 gint min_syllables, gint max_syllables,
				 guint flags);

void       phrase_dump (Phrase *p);
gchar     *phrase_to_string (Phrase *p);

#endif /* __PHRASE_H__ */

