/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * gnoetry-form-view.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "gnoetry-line-view.h"
#include "gnoetry-form-view.h"

static GtkObjectClass *parent_class = NULL;

enum {
  ARG_0
};

static void
gnoetry_form_view_get_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
gnoetry_form_view_set_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
gnoetry_form_view_destroy (GtkObject *obj)
{
  if (parent_class->destroy)
    parent_class->destroy (obj);
}

static void
gnoetry_form_view_finalize (GtkObject *obj)
{
  GnoetryFormView *x = GNOETRY_FORM_VIEW(obj);
  
  gtk_object_unref (GTK_OBJECT (x->form));

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
gnoetry_form_view_class_init (GnoetryFormViewClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;

  parent_class = gtk_type_class (GTK_TYPE_FRAME);

  object_class->get_arg = gnoetry_form_view_get_arg;
  object_class->set_arg = gnoetry_form_view_set_arg;
  object_class->destroy = gnoetry_form_view_destroy;
  object_class->finalize = gnoetry_form_view_finalize;

}

static void
gnoetry_form_view_init (GnoetryFormView *obj)
{

}

GtkType
gnoetry_form_view_get_type (void)
{
  static GtkType gnoetry_form_view_type = 0;
  if (!gnoetry_form_view_type) {
    static const GtkTypeInfo gnoetry_form_view_info = {
      "GnoetryFormView",
      sizeof (GnoetryFormView),
      sizeof (GnoetryFormViewClass),
      (GtkClassInitFunc)gnoetry_form_view_class_init,
      (GtkObjectInitFunc)gnoetry_form_view_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    gnoetry_form_view_type = gtk_type_unique (GTK_TYPE_FRAME, &gnoetry_form_view_info);
  }
  return gnoetry_form_view_type;
}

static void
packer_cb (GnoetryLine *line, gpointer user_data)
{
  GtkBox *box = GTK_BOX (user_data);

  if (line)
    gtk_box_pack_start (box, 
			gnoetry_line_view_new (line),
			FALSE, FALSE, 0);
  else {
    /* pack in an empty box with some padding for the break */
    gtk_box_pack_start (box,
			gtk_hbox_new (FALSE, 0),
			TRUE, TRUE, 8);
  }
}

void
gnoetry_form_view_construct (GnoetryFormView *view, GnoetryForm *form)
{
  GtkWidget *swin;
  GtkWidget *vbox;

  g_return_unless_is_gnoetry_form_view (view);
  g_return_unless_is_gnoetry_form (form);

  g_return_if_fail (view->form == NULL);

  view->form = form;
  gtk_object_ref (GTK_OBJECT (view->form));

  vbox = gtk_vbox_new (FALSE, 2);
  gtk_box_pack_start (GTK_BOX (vbox), gtk_hbox_new (FALSE, 0), TRUE, TRUE, 8); /* Leading padding */
  gnoetry_form_foreach_line (form, packer_cb, vbox);
  gtk_box_pack_start (GTK_BOX (vbox), gtk_hbox_new (FALSE, 0), TRUE, TRUE, 8); /* Trailing padding */

  swin = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (swin), vbox);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (swin),
				  GTK_POLICY_NEVER,
				  GTK_POLICY_AUTOMATIC);

  gtk_container_add (GTK_CONTAINER (view), swin);
  gtk_widget_show_all (vbox);
}

GtkWidget *
gnoetry_form_view_new (GnoetryForm *form)
{
  GtkWidget *w;

  g_return_val_unless_is_gnoetry_form (form, NULL);

  w = GTK_WIDGET (gtk_type_new (gnoetry_form_view_get_type ()));

  gnoetry_form_view_construct (GNOETRY_FORM_VIEW (w), form);

  return w;
}
