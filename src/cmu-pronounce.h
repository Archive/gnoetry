/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * cmu-pronounce.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_CMU_PRONOUNCE_H
#define _INC_CMU_PRONOUNCE_H

#include <glib.h>

enum {
  CMU_AA=0,
  CMU_AE,
  CMU_AH,
  CMU_AO,
  CMU_AW,
  CMU_AY,
  CMU_B,
  CMU_CH,
  CMU_D,
  CMU_DH,
  CMU_EH,
  CMU_ER,
  CMU_EY,
  CMU_F,
  CMU_G,
  CMU_HH,
  CMU_IH,
  CMU_IY,
  CMU_JH,
  CMU_K,
  CMU_L,
  CMU_M,
  CMU_N,
  CMU_NG,
  CMU_OW,
  CMU_OY,
  CMU_P,
  CMU_R,
  CMU_S,
  CMU_SH,
  CMU_T,
  CMU_TH,
  CMU_UH,
  CMU_UW,
  CMU_V,
  CMU_W,
  CMU_Y,
  CMU_Z,
  CMU_ZH,
  CMU_LAST_PHONEME,
  CMU_NO_STRESS,
  CMU_PRIMARY_STRESS,
  CMU_SECONDARY_STRESS,
  CMU_LAST_STRESS
};

typedef struct _PronunciationCMU PronunciationCMU;
struct _PronunciationCMU {
  gchar *word;

  gint syllables;
  gint N; /* number of phonemes */

  guint8* phoneme;

  gint table_pos;

  GList *rhyme_stems;
};

const gchar *cmu_phoneme2str (gint);

void cmu_load_dict (void);
PronunciationCMU *cmu_word_lookup (const gchar *word);
void cmu_clear_rhyme_stems (void);

gint cmu_syllables (const gchar *word);

void cmu_rhymes_foreach (PronunciationCMU *,
			 void (*fn) (PronunciationCMU *, gpointer),
			 gpointer);
gint cmu_rhymes_count (PronunciationCMU *);
PronunciationCMU *cmu_rhymes_choose (PronunciationCMU *);
			 
void show_cmu_rhymes (const gchar *word);




#endif /* _INC_CMU_PRONOUNCE_H */

/* $Id$ */
