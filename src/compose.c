/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * compose.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "babble.h"
#include "compose.h"

gchar **
compose_fixed_syllable_poem (gint *syllable_counts)
{
  BabbleArgs args;
  gint i, N;
  gchar **poem;

  g_return_val_if_fail (syllable_counts, NULL);

  N=0;
  while (syllable_counts[N] > 0)
    ++N;

  poem = g_new0 (gchar *, N+1);
  
  babble_args_init (&args);

  for (i=0; i<N; ++i) {
    args.min_syllables = args.max_syllables = syllable_counts[i];
    poem[i] = babble (&args);
  }
  poem[N] = NULL;

  return poem;
}

gchar **
compose_haiku (void)
{
  gint foo[] = { 5, 7, 5, 0 };

  return compose_fixed_syllable_poem (foo);
}

gchar **
compose_tanka (void)
{
  gint foo[] = { 5, 7, 5, 7, 7, 0 };

  return compose_fixed_syllable_poem (foo);
}

gchar **
compose_renga (void)
{
  gint foo[] = { 5, 7, 5, 7, 7, 5, 7, 5, 0 };

  return compose_fixed_syllable_poem (foo);
}


/* $Id$ */
