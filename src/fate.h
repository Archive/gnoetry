/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * fate.h
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __FATE_H__
#define __FATE_H__

#include <glib.h>

void     fate_tempt            (guint seed);
void     fate_surrender        (void);
guint    fate_unravel          (void);

gboolean fate_coin_toss        (void);
gboolean fate_render_judgement (double probability_of_true);
gint     fate_spin_wheel       (gint number_of_spokes);

#endif /* __FATE_H__ */

