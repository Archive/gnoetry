/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * phrase.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <stdlib.h>
#include <stdarg.h>
#include <ctype.h>
#include "fate.h"
#include "phrase.h"

Phrase *
phrase_new (void)
{
  Phrase *p = g_new0 (Phrase, 1);

  p->words = 0;
  p->syllables = 0;

  return p;
}

Phrase *
phrase_new_head (void)
{
  Phrase *p = phrase_new ();
  phrase_prepend_start (p);
  return p;
}

Phrase *
phrase_new_tail (void)
{
  Phrase *p = phrase_new ();
  phrase_append_stop (p);
  return p;
}

void
phrase_free (Phrase *p)
{
  if (p) {
    g_list_free (p->word_list);
    p->word_list = NULL;
    g_free (p);
  }
}

void
phrase_clear (Phrase *p)
{
  g_return_if_fail (p);
  g_list_free (p->word_list);
  p->word_list = NULL;
  p->words = 0;
  p->syllables = 0;
}

gboolean
phrase_empty (Phrase *p)
{
  g_return_val_if_fail (p, FALSE);
  return p->word_list == NULL;
}

gboolean
phrase_nonempty (Phrase *p)
{
  g_return_val_if_fail (p, FALSE);
  return p->word_list != NULL;
}

gboolean
phrase_is_head (Phrase *p)
{
  g_return_val_if_fail (p, FALSE);

  if (phrase_nonempty (p)) {
    TokenInfo *info = (TokenInfo *) p->word_list->data;

    return (info->code == TOKEN_START);
  }

  return FALSE;
}

gboolean
phrase_is_tail (Phrase *p)
{
  g_return_val_if_fail (p, FALSE);

  if (phrase_nonempty (p)) {
    TokenInfo *info = (TokenInfo *) g_list_last (p->word_list)->data;
    return (info->code == TOKEN_STOP);
  }
  return FALSE;
}

gboolean
phrase_is_sentence (Phrase *p)
{
  g_return_val_if_fail (p, FALSE);
  return phrase_is_head (p) && phrase_is_tail (p);
}

gboolean
phrase_contains_stop (Phrase *p)
{
  GList *i;
  g_return_val_if_fail (p, FALSE);

  for (i = p->word_list; i != NULL; i = g_list_next (i)) {
    TokenInfo *info = (TokenInfo *) i->data;
    g_assert (info);
    if (info->code == TOKEN_STOP)
      return TRUE;
  }
  return FALSE;
}

TokenInfo *
phrase_first_word (Phrase *p)
{
  g_return_val_if_fail (p, NULL);

  return p->word_list ? (TokenInfo *) p->word_list->data : NULL;
}

TokenInfo *
phrase_last_word (Phrase *p)
{
  GList *n;
  g_return_val_if_fail (p, NULL);

  n = g_list_last (p->word_list);
  return n ? (TokenInfo *) n->data : NULL;
}

gint
phrase_words (Phrase *p)
{
  g_return_val_if_fail (p, 0);

  if (p->words <= 0) {
    GList *i = p->word_list;

    p->words = 0;

    for (i = p->word_list; i != NULL; i = g_list_next (i)) {
      TokenInfo *info = (TokenInfo *) i->data;
      p->words += info->words;
    }
  }

  return p->words;
}

gint
phrase_syllables (Phrase *p)
{
  g_return_val_if_fail (p, 0);

  if (p->syllables <= 0) {
    GList *i = p->word_list;

    p->syllables = 0;

    for (i = p->word_list; i != NULL; i = g_list_next (i)) {
      TokenInfo *info = (TokenInfo *) i->data;
      p->words += info->syllables;
    }
  }

  return p->syllables;
}

void
phrase_foreach (Phrase *p, PhraseFunction fn, gpointer user_data)
{
  GList *i;

  g_return_if_fail (p);
  g_return_if_fail (fn);
  
  for (i = p->word_list; i != NULL; i = g_list_next (i)) {
    TokenInfo *info = (TokenInfo *) i->data;
    g_assert (info);
    fn (info, user_data);
  }
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

static void
phrase_added_token (Phrase *p, TokenInfo *info)
{
  g_return_if_fail (p);
  g_return_if_fail (info);

  if (p->words >= 0)
    p->words += info->words;

  if (p->syllables >= 0)
    p->syllables += info->syllables;
}

static void
phrase_removed_token (Phrase *p, TokenInfo *info)
{
  g_return_if_fail (p);
  g_return_if_fail (info);

  if (p->words >= 0)
    p->words -= info->words;
  if (p->syllables >= 0)
    p->syllables -= info->syllables;
}

void
phrase_prepend (Phrase *p, TokenInfo *info)
{
  g_return_if_fail (p);
  g_return_if_fail (info);

  p->word_list = g_list_prepend (p->word_list, info);
  phrase_added_token (p, info);
}

void
phrase_append (Phrase *p, TokenInfo *info)
{
  g_return_if_fail (p);
  g_return_if_fail (info);

  p->word_list = g_list_append (p->word_list, info);
  phrase_added_token (p, info);
}

void
phrase_prepend_start (Phrase *p)
{
  static TokenInfo *info = NULL;

  if (info == NULL) {
    info = get_token_info_by_code (TOKEN_START);
    g_assert (info);
  }

  if (!phrase_is_head (p))
    phrase_prepend (p, info);
}

void
phrase_append_stop (Phrase *p)
{
  static TokenInfo *info = NULL;

  if (info == NULL) {
    info = get_token_info_by_code (TOKEN_STOP);
    g_assert (info);
  }

  if (!phrase_is_tail (p))
    phrase_append (p, info);
}

void
phrase_drop_first_word (Phrase *p)
{
  GList *tmp;
  g_return_if_fail (p);

  if (p->word_list == NULL)
    return;

  tmp = p->word_list;
  phrase_removed_token (p, (TokenInfo *) tmp->data);

  p->word_list = g_list_remove_link (p->word_list, p->word_list);
  g_list_free_1 (tmp);
}

void
phrase_drop_last_word (Phrase *p)
{
  GList *tmp;
  g_return_if_fail (p);

  if (p->word_list == NULL)
    return;

  tmp = g_list_last (p->word_list);
  phrase_removed_token (p, (TokenInfo *) tmp->data);

  p->word_list = g_list_remove_link (p->word_list, tmp);
  g_list_free_1 (tmp);
}

gint
phrase_grow (Phrase *p, PhraseGrowType type, gint N,
	     gint max_words, gint max_syllables,
	     gboolean stop_on_boundary)
{
  const gint threshold=20;
  gboolean being_random, count=0;
  g_return_val_if_fail (p, 0);

  if (max_words < 0 && max_syllables < 0)
    stop_on_boundary = TRUE;

  if (N < 0)
    N = G_MAXINT;
  if (max_words < 0)
    max_words = G_MAXINT;
  if (max_syllables < 0)
    max_syllables = G_MAXINT;

  being_random = (type == PHRASE_GROW_RANDOM);

  while (count < N && max_words >= 0 && max_syllables >= 0) {
    TokenInfo *info = NULL;
    gint guess_count=0;

    /* Make sure that we can actually grow. */
    if (stop_on_boundary) {
      if (being_random) {
	if (phrase_is_sentence (p))
	  goto finished;
      } else {
	if ( (type == PHRASE_GROW_FORWARD && phrase_is_tail (p))
	     || (type == PHRASE_GROW_BACKWARD && phrase_is_head (p)) )
	  goto finished;
      }
    }
   
    /* If we are random, choose a random direction to grow in. */
    if (being_random) {
      if (stop_on_boundary && phrase_is_head (p))
	type = PHRASE_GROW_FORWARD;
      else if (stop_on_boundary && phrase_is_tail (p))
	type = PHRASE_GROW_BACKWARD;
      else
	type = fate_coin_toss () ? PHRASE_GROW_FORWARD : PHRASE_GROW_BACKWARD;
    }

    /* Try to pick a word that fits underneath the word and syllable
       maximums we've established... but limit ourselves to a certain
       number of tries. */
    while (guess_count < threshold) {
      if (type == PHRASE_GROW_FORWARD) {
	info = phrase_last_word (p);
	info = token_info_choose_successor (info);
      } else if (type == PHRASE_GROW_BACKWARD) {
	info = phrase_first_word (p);
	info = token_info_choose_predecessor (info);
      } else {
	g_assert_not_reached ();
      }

      if (info->words <= max_words && info->syllables <= max_syllables) {
	max_words -= info->words;
	max_syllables -= info->syllables;
	break;
      }
      ++guess_count;
    }
    /* If we couldn't do it, we just return with what we have so far. */
    if (guess_count >= threshold || info == NULL)
      goto finished;

    /* Attach the chosen word to the appropriate end of the sentence. */
    if (type == PHRASE_GROW_FORWARD) {
      phrase_append (p, info);
    } else if (type == PHRASE_GROW_BACKWARD) {
      phrase_prepend (p, info);
    } else {
      g_assert_not_reached ();
    }

    ++count;
  }

 finished:
  
  /* Drop trailing start-tokens and leading stop-tokens. */
  if (phrase_nonempty (p)) {
    TokenInfo *info;
    info = phrase_first_word (p);
    if (info->code == TOKEN_STOP)
      phrase_drop_first_word (p);
  }

  if (phrase_nonempty (p)) {
    TokenInfo *info;
    info = phrase_last_word (p);
    if (info->code == TOKEN_START)
         phrase_drop_last_word (p);
  }

  return count;
}


/*
  This is a mechanism for strongly discouraging, while not totally
   forbidding, very short sentences inside of our phrases.

   Takes three probabilities (use a value <0 to get the defaults) and
   then a NULL-terminated list of Phrase-pointers.
*/

/* These are the default probabilities that a phrase with a length 1, 2, or
   3 sentence will be deemed unworthy. */
#define DEFAULT_LEN1_KILLRATE 0.95
#define DEFAULT_LEN2_KILLRATE 0.75
#define DEFAULT_LEN3_KILLRATE 0.30
static gboolean
phrase_sentence_length_validation (double len1_killrate,
				   double len2_killrate,
				   double len3_killrate,
				   Phrase *first, ...)
{
  va_list args;
  gint len=-1;
  Phrase *current = first;

  if (len1_killrate < 0) len1_killrate = DEFAULT_LEN1_KILLRATE;
  if (len2_killrate < 0) len2_killrate = DEFAULT_LEN2_KILLRATE;
  if (len3_killrate < 0) len3_killrate = DEFAULT_LEN3_KILLRATE;

  va_start (args, first);

  while (current) {
    GList *iter = current->word_list;
    
    while (iter) {
      TokenInfo *info = (TokenInfo *) iter->data;
      if (info->code == TOKEN_START)
	len = 0;
      else if (info->code == TOKEN_STOP && len >= 0) {

	if (len <= 3) {
	  double p = 0;

	  if (len == 0)
	    p = 1.0;
	  else if (len == 1)
	    p = len1_killrate;
	  else if (len == 2)
	    p = len2_killrate;
	  else if (len == 3)
	    p = len3_killrate;

	  if (p > 0 && fate_render_judgement (p)) {
	    va_end (args);
	    return FALSE;
	  }
	}

      } else if (len >= 0)
	len += info->words;
    
      iter = g_list_next (iter);
    }
    
    current = va_arg (args, Phrase *);
  }

  va_end (args);
  return TRUE;
}
				   

#define TRY_LIMIT 100000

Phrase *
phrase_new_predecessor (Phrase *p, 
			gint min_words, gint max_words,
			gint min_syllables, gint max_syllables,
			guint flags)
{
  Phrase *n = phrase_new ();
  gint limit = TRY_LIMIT;
  gboolean good = FALSE;

  while (!good && limit > 0) {

    /* Create a phrase */
    phrase_clear (n);
    if (p == NULL || phrase_is_head (p)) {
      phrase_append_stop (n);
      phrase_grow (n, PHRASE_GROW_BACKWARD, -1, max_words, max_syllables,
		   flags & PHRASE_FLAG_STOP_ON_BOUNDARY);
    } else {
      phrase_append (n, phrase_first_word (p));
      phrase_grow (n, PHRASE_GROW_BACKWARD, -1, max_words, max_syllables,
		   flags & PHRASE_FLAG_STOP_ON_BOUNDARY);
      phrase_drop_last_word (n);
    }

    /* Check and see if it meets the criteria */
    good = TRUE;

    if (good 
	&& min_words > 0
	&& phrase_words (n) < min_words)
      good = FALSE;

    if (good
	&& min_syllables > 0 
	&& phrase_syllables (n) < min_syllables)
      good = FALSE;

    if (good 
	&& (flags & PHRASE_FLAG_FORCE_HEAD)
	&& !phrase_is_head (n))
      good = FALSE;

    if (good
	&& (flags & PHRASE_FLAG_REQUIRE_STOP) 
	&& !phrase_contains_stop (n))
      good = FALSE;

    if (good
	&& !phrase_sentence_length_validation (-1, -1, -1, n, p, NULL))
      good = FALSE;

    --limit;
  }

  if (!good) {
    phrase_free (n);
    n = NULL;
  }

  return n;
}

Phrase *
phrase_new_successor (Phrase *p, 
		      gint min_words, gint max_words,
		      gint min_syllables, gint max_syllables,
		      guint flags)
{
  Phrase *n = phrase_new ();
  gint limit = TRY_LIMIT;
  gboolean good = FALSE;
  
  while (!good && limit > 0) {

    /* Create a phrase */
    phrase_clear (n);
    if (p == NULL || phrase_is_tail (p)) {
      phrase_prepend_start (n);
      phrase_grow (n, PHRASE_GROW_FORWARD, -1, max_words, max_syllables,
		   flags & PHRASE_FLAG_STOP_ON_BOUNDARY);
    } else {
      phrase_prepend (n, phrase_last_word (p));
      phrase_grow (n, PHRASE_GROW_FORWARD, -1, max_words, max_syllables,
		   flags & PHRASE_FLAG_STOP_ON_BOUNDARY);
      phrase_drop_first_word (n);
    }

    /* Check and see if it meets the criteria */
    good = TRUE;
    
    if (good
	&& min_words > 0
	&& phrase_words (n) < min_words)
      good = FALSE;

    if (good
	&& min_syllables > 0
	&& phrase_syllables (n) < min_syllables)
      good = FALSE;

    if (good
	&& (flags & PHRASE_FLAG_FORCE_TAIL)
	&& !phrase_is_tail (n))
      good = FALSE;

    if (good 
	&& (flags & PHRASE_FLAG_REQUIRE_STOP)
	&& !phrase_contains_stop (n))
      good = FALSE;

    if (good
	&& ! (p ? phrase_sentence_length_validation (-1, -1, -1, p, n, NULL) :
	      phrase_sentence_length_validation (-1, -1, -1, n, NULL)))
      good = FALSE;

    --limit;
  }

  if (!good) {
    g_message ("over limit!");
    phrase_free (n);
    n = NULL;
  }

  return n;
}


/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

static void
print_fn (TokenInfo *info, gpointer user_data)
{
  g_print ("[%s]", info->word);
}

void
phrase_dump (Phrase *p)
{
  g_return_if_fail (p);
  phrase_foreach (p, print_fn, NULL);
  g_print ("\n");
}

gchar *
phrase_to_string (Phrase *p)
{
  GList *sl = NULL;
  GList *i;
  gint j;
  gchar **strv;
  gchar *pstr;
  gboolean last_was_start, first_token;

  last_was_start = FALSE;
  first_token = TRUE;
  for (i = p->word_list; i != NULL; i = g_list_next (i)) {
    TokenInfo *info = (TokenInfo *) i->data;

    if (info->code != TOKEN_START && info->code != TOKEN_STOP) {
      gchar *str = info->word;
      
      if (*str == '|') {
	sl = g_list_append (sl, g_strdup (str+1));
      } else {
	gchar *cpy;
	
	if (!first_token)
	  sl = g_list_append (sl, g_strdup (" "));
	first_token = FALSE;

	sl = g_list_append (sl, cpy = g_strdup (str));

	if (last_was_start && cpy && *cpy && islower ((gint) *cpy))
	  *cpy = toupper ((gint) *cpy);
	  
      }
    }
    last_was_start = info->code == TOKEN_START;
  }

  strv = g_new0 (gchar *, g_list_length (sl)+1);
  j = 0;
  for (i = sl; i != NULL; i = g_list_next (i)) {
    strv[j] = i->data;
    ++j;
  }
  g_list_free (sl);

  pstr = g_strjoinv ("", strv);
  
  for (j=0; strv[j]; ++j)
    g_free (strv[j]);

  return pstr;
}
