/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * babble.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <ctype.h>
#include "babble.h"

void
babble_args_init (BabbleArgs *args)
{
  g_return_if_fail (args);

  args->direction = BABBLE_FORWARD;
  args->lead_word = NULL;

  args->min_syllables = -1;
  args->max_syllables = -1;
  args->min_words = -1;
  args->max_words = -1;
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

static gchar *bad_fragments[] = {
  ",,", "_a a_", "_i i_", "_a i_", "_i a_",
  NULL
};

static gboolean
pattern_match (const gchar *str, const gchar *pat)
{
  const gchar *s;
  const gchar *p;
  
  for (s=str; *s; ++s) {
    const gchar *ss = s;
    gboolean match=TRUE;
    
    p = pat;
    if (s == str && *p == '_')
      ++p;

    while (*p && *ss && match) {

      if (match && *p == '_' && !isspace (*ss)) {
	match = FALSE;
      }

      if (match && *p != '_' && *p != *ss) {
	match = FALSE;
      }

      ++p;
      ++ss;
    }
	
    if (match && *ss == '\0' && !(*p == '_' && *(p+1) == '\0')) {
      match = FALSE;
    }

    if (match)
      return TRUE;
  }

  return FALSE;
}

static gboolean
good_string (const gchar *str)
{
  gint k;

  for (k=0; bad_fragments[k]; ++k) {
    
    if (pattern_match (str, bad_fragments[k])) 
      return FALSE;
  }

  return TRUE;
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

gchar *
babble (BabbleArgs *args)
{
  BabbleArgs defaults;

  gchar *str = NULL;

  gboolean done = FALSE;
  GList *token_list = NULL;
  GList *iter;

  TokenInfo *lead_word;
  TokenInfo *word;
  TokenInfo *terminal_token = NULL;
  TokenInfo *(*gen_fn) (TokenInfo *) = NULL;

  if (args == NULL) {
    babble_args_init (&defaults);
    args = &defaults;
  }

  lead_word = args->lead_word;

  if (args->direction == BABBLE_FORWARD) {
    terminal_token = get_token_info_by_code (TOKEN_STOP);
    gen_fn = token_info_choose_successor;
    if (lead_word == NULL)
      lead_word = get_token_info_by_code (TOKEN_START);

  } else if (args->direction == BABBLE_BACKWARD) {
    terminal_token = get_token_info_by_code (TOKEN_START);
    gen_fn = token_info_choose_predecessor;
    if (lead_word == NULL)
      lead_word = get_token_info_by_code (TOKEN_STOP);

  } else {
    g_assert_not_reached ();
  }

  while (!done) {

    gint word_count = 0;
    gint syl_count = 0;
    gboolean complete;

    g_list_free (token_list);
    token_list = NULL;

    word = lead_word;
    complete = FALSE;
    while (word != NULL) {

      token_list = g_list_append (token_list, word);

      if (token_info_word (word)[0] != '<') {
	++word_count;
	syl_count += token_info_syllables (word);
      }

      if (args->max_words > 0 && word_count > args->max_words)
	word = NULL;

      if (args->max_syllables > 0 && syl_count > args->max_syllables)
	word = NULL;

      if (word) {
	if (word == terminal_token) {
	  complete = TRUE;
	  word = NULL;
	} else {
	  word = gen_fn (word);
	  g_assert (word);
	}
      }
    }

    if (complete) {

      done = TRUE;

      if (args->min_words > 0 && word_count < args->min_words)
	done = FALSE;

      if (args->max_words > 0 && word_count > args->max_words)
	done = FALSE;

      if (args->min_syllables > 0 && syl_count < args->min_syllables)
	done = FALSE;

      if (args->max_syllables > 0 && syl_count > args->max_syllables)
	done = FALSE;

    }

    if (done && args->direction == BABBLE_BACKWARD)
      token_list = g_list_reverse (token_list);

    g_free (str);
    str = NULL;

    if (done) {
      /* Build our string version */
      for (iter = token_list; iter != NULL; iter = g_list_next (iter)) {
	TokenInfo *info = (TokenInfo *)iter->data;
	
	if (token_info_code (info) >= TOKEN_LAST) {
	  const gchar *s = token_info_word (info);
	  gchar *ns;
	  gboolean glue = FALSE;


	  if (s && *s == '|') {
	    glue = TRUE;
	    ++s;
	  }
	  
	  ns = g_strconcat (str ? str : "",
			    glue || !str ? "" : " ",
			    s, NULL);
	  g_free (str);
	  str = ns;
	}
      }

      if (str == NULL)
	g_warning ("Null string");

      if (!good_string (str))
	done = FALSE;

    }

  }

  if (str && *str && islower (*str)) 
    *str = toupper (*str);
  
  g_list_free (token_list);
  
  return str;
}


/* $Id$ */
