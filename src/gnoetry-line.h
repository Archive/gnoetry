/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * gnoetry-line.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GNOETRY_LINE_H
#define _INC_GNOETRY_LINE_H

#include <gtk/gtk.h>
#include "words.h"

typedef struct _GnoetryLine GnoetryLine;
typedef struct _GnoetryLineClass GnoetryLineClass;

enum {
  LINK_NONE,
  LINK_END_WORD_MATCH,
  LINK_END_WORD_RHYME
};

struct _GnoetryLine {
  GtkObject parent;

  gint min_syl, max_syl;
  gint min_words, max_words;

  gint link_type;
  GnoetryLine *linked_with;

  gint meter_template_length;
  gint *meter_template;

  GList *words;
  gchar *str;

  gboolean line_can_be_long;
};

struct _GnoetryLineClass {
  GtkObjectClass parent_class;

  void (*changed) (GnoetryLine *);
};

#define GNOETRY_TYPE_LINE (gnoetry_line_get_type ())
#define GNOETRY_LINE(obj) (GTK_CHECK_CAST((obj),GNOETRY_TYPE_LINE,GnoetryLine))
#define GNOETRY_LINE0(obj) ((obj) ? (GNOETRY_LINE(obj)) : NULL)
#define GNOETRY_LINE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GNOETRY_TYPE_LINE,GnoetryLineClass))
#define GNOETRY_IS_LINE(obj) (GTK_CHECK_TYPE((obj), GNOETRY_TYPE_LINE))
#define GNOETRY_IS_LINE0(obj) (((obj) == NULL) || (GNOETRY_IS_LINE(obj)))
#define GNOETRY_IS_LINE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GNOETRY_TYPE_LINE))

#define g_return_unless_is_gnoetry_line(x) (g_return_if_fail((x)&&GNOETRY_IS_LINE((x))))
#define g_return_val_unless_is_gnoetry_line(x,y) (g_return_val_if_fail((x)&&GNOETRY_IS_LINE((x)),(y)))

GtkType gnoetry_line_get_type (void);

GnoetryLine *gnoetry_line_new (void);

void gnoetry_line_clear (GnoetryLine *);

void gnoetry_line_set_meter (GnoetryLine *, const gchar *meter_str);
double gnoetry_line_meter_penalty (GnoetryLine *);

void gnoetry_line_generate (GnoetryLine *);
void gnoetry_line_regenerate_related (GnoetryLine *);

const gchar *gnoetry_line_string (GnoetryLine *);

TokenInfo *gnoetry_line_last_word (GnoetryLine *);

#endif /* _INC_GNOETRY_LINE_H */

/* $Id$ */
