/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * gnoetry-viewer.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "gnoetry-viewer.h"

static GtkObjectClass *parent_class = NULL;

enum {
  ARG_0
};

enum {
  LAST_SIGNAL
};

static guint gnoetry_viewer_signals[LAST_SIGNAL] = { 0 };

static void
gnoetry_viewer_get_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
gnoetry_viewer_set_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
gnoetry_viewer_destroy (GtkObject *obj)
{
  if (parent_class->destroy)
    parent_class->destroy (obj);
}

static void
gnoetry_viewer_finalize (GtkObject *obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
gnoetry_viewer_class_init (GnoetryViewerClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;

  parent_class = gtk_type_class (GTK_TYPE_HBOX);

  object_class->get_arg = gnoetry_viewer_get_arg;
  object_class->set_arg = gnoetry_viewer_set_arg;
  object_class->destroy = gnoetry_viewer_destroy;
  object_class->finalize = gnoetry_viewer_finalize;

#if 0
  /* Signal definition template */
  gnoetry_viewer_signals[CHANGED] =
    gtk_signal_new ("changed",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GnoetryViewerClass, changed),
                    gtk_marshal_NONE__NONE, GTK_TYPE_NONE, 0);
#endif

  gtk_object_class_add_signals (object_class, gnoetry_viewer_signals,
                                LAST_SIGNAL);
}

static void
gnoetry_viewer_init (GnoetryViewer *obj)
{

}

GtkType
gnoetry_viewer_get_type (void)
{
  static GtkType gnoetry_viewer_type = 0;
  if (!gnoetry_viewer_type) {
    static const GtkTypeInfo gnoetry_viewer_info = {
      "GnoetryViewer",
      sizeof (GnoetryViewer),
      sizeof (GnoetryViewerClass),
      (GtkClassInitFunc)gnoetry_viewer_class_init,
      (GtkObjectInitFunc)gnoetry_viewer_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    gnoetry_viewer_type = gtk_type_unique (GTK_TYPE_HBOX, &gnoetry_viewer_info);
  }
  return gnoetry_viewer_type;
}

void
gnoetry_viewer_construct (GnoetryViewer *gv)
{
  GtkWidget *sbar;

  g_return_if_fail (gv && GNOETRY_IS_VIEWER (gv));


  sbar = gtk_vscrollbar_new (NULL);
  
  gv->text = GTK_TEXT (gtk_text_new (NULL, 
				     gtk_range_get_adjustment (GTK_RANGE (sbar))));

  gtk_box_pack_start (GTK_BOX (gv),
		      GTK_WIDGET (gv->text),
		      TRUE, TRUE, 2);
  gtk_box_pack_start (GTK_BOX (gv),
		      sbar,
		      FALSE, FALSE, 2);


}

GtkWidget *
gnoetry_viewer_new (void)
{
  GnoetryViewer *gv;

  gv = GNOETRY_VIEWER (gtk_type_new (gnoetry_viewer_get_type ()));
  gnoetry_viewer_construct (gv);

  return GTK_WIDGET (gv);
}

void
gnoetry_viewer_reset (GnoetryViewer *view)
{
  g_return_if_fail (view && GNOETRY_IS_VIEWER (view));
}

static void
insert (GnoetryViewer *view, const gchar *str)
{
  static gboolean inited = FALSE;
  static GdkColor fg_color, bg_color;
  static GdkFont *font;

  if (!inited) {

    fg_color.red = 0;
    fg_color.green = 0;
    fg_color.blue = 0;
    gdk_color_alloc (gdk_colormap_get_system (), &fg_color);

    bg_color.red = 0xffff;
    bg_color.green = 0xffff;
    bg_color.blue = 0xffff;
    gdk_color_alloc (gdk_colormap_get_system (), &bg_color);

    if (font == NULL)
      font = gdk_font_load ("-*-times-medium-r-*-*-18-*-*-*-*-*-iso8859-1");

    if (font == NULL)
      font = gdk_font_load ("-misc-fixed-medium-r-*-*-*-120-*-*-*-*-iso8859-1");
    if (font == NULL)
      font = gdk_font_load ("fixed");
    g_assert (font);

    inited = TRUE;
  }

  gtk_text_insert (view->text, font, &fg_color, &bg_color, str, -1);
}

void
gnoetry_viewer_add_poem (GnoetryViewer *view,
			 const gchar *title,
			 const gchar *type,
			 gchar **text)
{
  gint i;

  g_return_if_fail (view && GNOETRY_IS_VIEWER (view));

  insert (view, "\n\n");

  if (type) {
    insert (view, "(");
    insert (view, type);
    insert (view, ")\n");
  }

  if (title) {
    insert (view, title);
    insert (view, "\n\n");
  }

  for (i=0; text && text[i]; ++i) {
    insert (view, text[i]);
    insert (view, "\n");
  }

  insert (view, "\n");
  
}
