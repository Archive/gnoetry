/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * gnoetry-line.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <ctype.h>
#include "words.h"
#include "phonetics.h"
#include "gnoetry-line.h"

static GtkObjectClass *parent_class = NULL;

enum {
  ARG_0
};

enum {
  CHANGED,
  LAST_SIGNAL
};

static guint gnoetry_line_signals[LAST_SIGNAL] = { 0 };

static void
gnoetry_line_get_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
gnoetry_line_set_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
gnoetry_line_finalize (GtkObject *obj)
{
  GnoetryLine *l = GNOETRY_LINE (obj);

  g_list_free (l->words);
  l->words = NULL;

  g_free (l->str);
  l->str = NULL;

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
gnoetry_line_class_init (GnoetryLineClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;

  parent_class = gtk_type_class (GTK_TYPE_OBJECT);

  object_class->get_arg = gnoetry_line_get_arg;
  object_class->set_arg = gnoetry_line_set_arg;
  object_class->finalize = gnoetry_line_finalize;

  gnoetry_line_signals[CHANGED] =
    gtk_signal_new ("changed",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GnoetryLineClass, changed),
                    gtk_marshal_NONE__NONE, GTK_TYPE_NONE, 0);

  gtk_object_class_add_signals (object_class, gnoetry_line_signals,
                                LAST_SIGNAL);
}

static void
gnoetry_line_init (GnoetryLine *obj)
{

}

GtkType
gnoetry_line_get_type (void)
{
  static GtkType gnoetry_line_type = 0;
  if (!gnoetry_line_type) {
    static const GtkTypeInfo gnoetry_line_info = {
      "GnoetryLine",
      sizeof (GnoetryLine),
      sizeof (GnoetryLineClass),
      (GtkClassInitFunc)gnoetry_line_class_init,
      (GtkObjectInitFunc)gnoetry_line_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    gnoetry_line_type = gtk_type_unique (GTK_TYPE_OBJECT, &gnoetry_line_info);
  }
  return gnoetry_line_type;
}

GnoetryLine *
gnoetry_line_new (void)
{
  return GNOETRY_LINE (gtk_type_new (gnoetry_line_get_type ()));
}

void
gnoetry_line_clear (GnoetryLine *l)
{
  g_return_unless_is_gnoetry_line (l);

  g_list_free (l->words);
  l->words = NULL;
  
  g_free (l->str);
  l->str = NULL;

  gtk_signal_emit (GTK_OBJECT (l), gnoetry_line_signals[CHANGED]);
}

#define HARD_STRESS   '/'
#define MEDIUM_STRESS '-'
#define NO_STRESS     'u'
#define ANY_STRESS    '*'

void
gnoetry_line_set_meter (GnoetryLine *l, const gchar *meter_str)
{
  gint i;

  g_return_unless_is_gnoetry_line (l);

  if (l->meter_template)
    g_free (l->meter_template);

  if (meter_str == NULL) {
    l->min_syl = l->max_syl = l->meter_template_length = 0;
    l->meter_template = NULL;
    return;
  }

  l->meter_template_length = strlen (meter_str);
  l->meter_template = g_new (gint, l->meter_template_length);

  l->min_syl = l->max_syl = l->meter_template_length;

  for (i=0; i<l->meter_template_length; ++i) {
    gint stress;

    switch (meter_str[i]) {

    case HARD_STRESS:
      stress = STRESS_PRIMARY;
      break;

    case MEDIUM_STRESS:
      stress = STRESS_SECONDARY;
      break;

    case NO_STRESS:
      stress = STRESS_NONE;
      break;

    case ANY_STRESS:
      stress = STRESS_WILDCARD;
      break;

    default:
      stress = STRESS_UNKNOWN;
      g_print ("Unknown stress character '%c'\n", meter_str[i]);
      break;
    }

    l->meter_template[i] = stress;
  }
}

double
gnoetry_line_meter_penalty (GnoetryLine *l)
{
  gint i, syl;
  GList *phon_iter, *word_iter;
  TokenInfo *token;
  PhoneticInfo *pi;
  double penalty = 0;

  g_return_val_unless_is_gnoetry_line (l, -1);

  if (l->meter_template == NULL)
    return 0;

  i = 0;
  word_iter = l->words;

  while (i < l->meter_template_length
	 && word_iter != NULL) {
    token = (TokenInfo *) word_iter->data;
    phon_iter = token->phonetics;
    while (phon_iter != NULL) {

      pi = (PhoneticInfo *) phon_iter->data;

      syl = 0;
      while (syl < pi->syllables && i < l->meter_template_length) {
	double x;

	gint have = phonetic_info_syllable_stress (pi, syl);
	gint want = l->meter_template[i];

	x = stress_penalty (have, want);

	//	g_print ("(%d) have:%s want:%s penalty=%g\n", i, stress2str (have), stress2str (want), x);

	penalty += x;
	++i;
	++syl;
      }

      phon_iter = g_list_next (phon_iter);
    }

    word_iter = g_list_next (word_iter);
  }
  //g_print ("total=%g\n\n", penalty);

  return penalty;
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

static gchar *bad_fragments[] = {
  ",,", "_a a_", "_i i_", "_a i_", "_i a_",
  NULL
};

static gboolean
pattern_match (const gchar *str, const gchar *pat)
{
  const gchar *s;
  const gchar *p;
  
  for (s=str; *s; ++s) {
    const gchar *ss = s;
    gboolean match=TRUE;
    
    p = pat;
    if (s == str && *p == '_')
      ++p;

    while (*p && *ss && match) {

      if (match && *p == '_' && !isspace (*ss)) {
	match = FALSE;
      }

      if (match && *p != '_' && *p != *ss) {
	match = FALSE;
      }

      ++p;
      ++ss;
    }
	
    if (match && *ss == '\0' && !(*p == '_' && *(p+1) == '\0')) {
      match = FALSE;
    }

    if (match)
      return TRUE;
  }

  return FALSE;
}

static gboolean
good_string (const gchar *str)
{
  gint k;

  for (k=0; bad_fragments[k]; ++k) {
    
    if (pattern_match (str, bad_fragments[k])) 
      return FALSE;
  }

  return TRUE;
}

static gboolean
gnoetry_line_generate_inner (GnoetryLine *l, TokenInfo *lead_word, gboolean backwards)
{
  gboolean done = FALSE;
  TokenInfo *terminal_token;
  TokenInfo *(*gen_fn) (TokenInfo *);

  gint tries = 0;

  g_return_val_unless_is_gnoetry_line (l, FALSE);

  if (lead_word == NULL) {
    lead_word = get_token_info_by_code (TOKEN_START);
    backwards = FALSE;
  }


  if (backwards) {
    gen_fn = token_info_choose_predecessor;
    terminal_token = get_token_info_by_code (TOKEN_START);
  } else {
    gen_fn = token_info_choose_successor;
    terminal_token = get_token_info_by_code (TOKEN_STOP);
  }

  while (!done) {
    gint word_count = 0, syl_count = 0;
    gboolean complete;
    TokenInfo *word;

    ++tries;
    if (tries > 1000000) {
      g_warning ("I'm having trouble!");
      return FALSE;
    }

    g_list_free (l->words);
    l->words = NULL;

    if (backwards) {
      TokenInfo *punct = get_token_info_by_code (TOKEN_PERIOD);
      l->words = g_list_append (l->words, punct);
      l->words = g_list_append (l->words, get_token_info_by_code (TOKEN_STOP));
    }

    word = lead_word;
    complete = FALSE;
    while (word) {

      if (backwards) 
	l->words = g_list_prepend (l->words, word);
      else
	l->words = g_list_append (l->words, word);

      if (token_info_word (word)[0] != '<') {
	++word_count;
	syl_count += token_info_syllables (word);
      }

      if (l->max_words > 0 && word_count > l->max_words)
	word = NULL;

      if (l->max_syl > 0 && syl_count > l->max_syl)
	word = NULL;

      if (word) {
	if (word == terminal_token) {
	  complete = TRUE;
	  word = NULL;
	} else {
	  word = gen_fn (word);
	  g_assert (word);
	}
      }
    }

    if (complete) {

      done = TRUE;

      if (l->min_words > 0 && word_count < l->min_words)
	done = FALSE;

      if (l->min_syl > 0 && syl_count < l->min_syl)
	done = FALSE;

    }

    /* Check to make sure that rhymes exist for the first of a
       pair of rhyming lines. */
    if (done
	&& l->linked_with
	&& l->linked_with->words == NULL
	&& l->link_type == LINK_END_WORD_RHYME) {
      GList *x = g_list_last (l->words);
      x = g_list_previous (x);
      x = g_list_previous (x);
      
      if (!(x && token_info_has_rhymes ((TokenInfo *) x->data))) 
	done = FALSE;
    }
    
    if (done) {
      GList *i;

      g_free (l->str);
      l->str = NULL;

      for (i = l->words; i; i = g_list_next (i)) {
	TokenInfo *info = (TokenInfo *) i->data;
	const gchar *s = token_info_word (info);
	gchar *ns;
	gboolean glue = FALSE;
	
	if (s && *s != '<') {
	  if (s && *s == '|') {
	    glue = TRUE;
	    ++s;
	  }
	  
	  ns = g_strconcat (l->str ? l->str : "",
			    glue || !l->str ? "" : " ",
			    s, NULL);
	  g_free (l->str);
	  l->str = ns;
	}
      }
      
      if (l->str == NULL || !good_string (l->str))
	done = FALSE;

    }
       
  }

  /* Capitalize the first letter of the sentence */
  if (l->str && *l->str && islower ((gint) *l->str))
    *l->str = toupper ((gint) *l->str);

  return TRUE;
}

#define METER_TRIALS 50
static void
gnoetry_line_generate_quiet (GnoetryLine *l)
{
  TokenInfo *lead_word = NULL;
  gboolean ok = TRUE;
  
  g_return_unless_is_gnoetry_line (l);

  /* Pick linked word. */

  if (l->linked_with && l->linked_with->words) {
    GList *x = g_list_last (l->linked_with->words);
    TokenInfo *link_target;
    
    x = g_list_previous (x);
    x = g_list_previous (x);
    link_target = (TokenInfo *) x->data;
    
    if (l->link_type == LINK_END_WORD_RHYME)
      lead_word = token_info_choose_rhyme (link_target);
    else if (l->link_type == LINK_END_WORD_MATCH)
      lead_word = link_target;

  }

  if (l->meter_template) {
    double best_so_far = 1e+8;
    GList *best_words = NULL;
    gchar *best_str = NULL;
    gint i;
    gint trials = METER_TRIALS;


    if (l->linked_with == NULL)
      trials *= 5;


    for (i=0; i<trials && ok; ++i) {
      double p;

      g_print (".");

      ok = gnoetry_line_generate_inner (l, lead_word, lead_word != NULL);
      
      if (ok) {
	p = gnoetry_line_meter_penalty (l);
	if (p < 0.01) 
	  goto search_complete;

	if (p < best_so_far) {
	  best_so_far = p;

	  g_list_free (best_words);
	  best_words = g_list_copy (l->words);
	  
	  g_free (best_str);
	  best_str = g_strdup (l->str);
	}
      }
    }

    if (ok) {
      g_list_free (l->words);
      l->words = g_list_copy (best_words);

      g_free (l->str);
      l->str = g_strdup (best_str);
    }
    
  search_complete:
    g_list_free (best_words);
    g_free (best_str);

    g_print ("\n");

  } else {
    
    ok = gnoetry_line_generate_inner (l, lead_word, lead_word != NULL);

  }

  if (!ok)
    gnoetry_line_regenerate_related (l);
}

void
gnoetry_line_generate (GnoetryLine *l)
{
  gnoetry_line_generate_quiet (l);
  g_print ("generated: \"%s\"\n", l->str);
  gtk_signal_emit (GTK_OBJECT (l), gnoetry_line_signals[CHANGED]);
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

void
gnoetry_line_regenerate_related (GnoetryLine *l)
{
  GnoetryLine *i;
  GnoetryLine **order;
  gint j, N=0;
  static gboolean inside=FALSE, restart=FALSE;

  g_return_unless_is_gnoetry_line (l);

  if (inside) {
    restart = TRUE;
    return;
  }

  inside = TRUE;

 restart_point:
  restart = FALSE;
  i = l;
  N = 0;
  do {
    g_list_free (i->words);
    i->words = NULL;
    
    g_free (i->str);
    i->str = NULL;

    i = i->linked_with;
    ++N;

  } while (i && i != l);
  
  order = g_new0 (GnoetryLine *, N);
  i = l;
  j = N-1;
  while (j >= 0) {
    order[j] = i;
    --j;
    i = i->linked_with;
  }

  for (j=0; j<N; ++j) {
    gnoetry_line_generate_quiet (order[j]);
    if (restart)
      goto restart_point;
  }

  g_free (order);

  i = l;
  do {
    gtk_signal_emit (GTK_OBJECT (i), gnoetry_line_signals[CHANGED]);
    i = i->linked_with;
  } while (i && i != l);

  inside = FALSE;
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

const gchar *
gnoetry_line_string (GnoetryLine *l)
{
  g_return_val_unless_is_gnoetry_line (l, NULL);

  while (l->str == NULL) 
    gnoetry_line_generate (l);

  return l->str;
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

static gboolean
nonalpha (const gchar *s)
{
  g_return_val_if_fail (s, FALSE);

  while (*s) {
    if (isalnum ((gint) *s))
      return TRUE;
    ++s;
  }
  
  return TRUE;
}

TokenInfo *
gnoetry_line_last_word (GnoetryLine *l)
{
  GList *x;

  g_return_val_unless_is_gnoetry_line (l, NULL);

  if (l->words == NULL)
    return NULL;

  x = g_list_last (l->words);

  while (x) {
    TokenInfo *info = (TokenInfo *) x->data;

    if (info->word[0] == '<' || nonalpha (info->word))
      x = g_list_previous (x);
    else
      return info;
  }

  return NULL;
}
