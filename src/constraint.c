/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * constraint.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include "constraint.h"

Constraint *
constraint_new (ConstraintType type, const gchar *tag)
{
  Constraint *con;

  g_return_val_if_fail (tag != NULL, NULL);

  con = g_new0 (Constraint, 1);
  con->type = type;
  con->tag = g_strdup (tag);

  return con;
}

void
constraint_free (Constraint *con)
{
  if (con) {
    g_free (con->tag);
    g_free (con);
  }
}

ConstraintType
constraint_type (Constraint *con)
{
  g_return_val_if_fail (con != NULL, CONSTRAINT_UNKNOWN);
  return con->type;
}

const gchar *
constraint_tag (Constraint *con)
{
  g_return_val_if_fail (con != NULL, NULL);
  return con->tag;
}

TokenInfo *
constraint_binding (Constraint *con)
{
  g_return_val_if_fail (con != NULL, NULL);
  return con->binding;
}

gboolean
constraint_is_bound (Constraint *con)
{
  g_return_val_if_fail (con != NULL, FALSE);
  return con->binding != NULL;
}

gboolean
constraint_is_free (Constraint *con)
{
  g_return_val_if_fail (con != NULL, FALSE);
  return con->binding == NULL;
}

void
constraint_clear (Constraint *con)
{
  g_return_if_fail (con);
  con->binding = NULL;
}

void
constraint_bind (Constraint *con, TokenInfo *info)
{
  g_return_if_fail (con != NULL);
  g_return_if_fail (info != NULL);
  con->binding = info;
}

TokenInfo *
constraint_solution (Constraint *con)
{
  g_return_val_if_fail (con != NULL, NULL);
  
  if (!constraint_is_bound (con)) {
    g_warning ("Request for solution on unbound constraint \"%s\" ignored.",
	       constraint_tag (con));
    return NULL;
  }

  switch (constraint_type (con)) {

  case CONSTRAINT_RHYME:
    return token_info_choose_rhyme (constraint_binding (con));

  case CONSTRAINT_DUPLICATE:
    return constraint_binding (con);

  default:
    g_warning ("Constraint \"%s\" has unknown type.", constraint_tag (con));
    return NULL;
  }

  g_assert_not_reached ();

  return NULL;
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

ConstraintManager *
constraint_manager_new (void)
{
  ConstraintManager *cmgr = g_new0 (ConstraintManager, 1);
  cmgr->table = g_hash_table_new (g_str_hash, g_str_equal);
  return cmgr;
}

ConstraintManager *
constraint_manager_new_with_outer_frame (ConstraintManager *outer_cmgr)
{
  ConstraintManager *cmgr;
  g_return_val_if_fail (outer_cmgr != NULL, NULL);
  cmgr = constraint_manager_new ();
  cmgr->outer = outer_cmgr;
  return cmgr;
}

static void
cm_free_fn (gpointer key, gpointer val, gpointer user_data)
{
  constraint_free ((Constraint *) val);
}

void
constraint_manager_free (ConstraintManager *cmgr)
{
  if (cmgr) {
    g_hash_table_foreach (cmgr->table, cm_free_fn, NULL);
    g_hash_table_destroy (cmgr->table);
    g_free (cmgr);
  }
}

gboolean
constraint_manager_contains (ConstraintManager *cmgr, const gchar *tag)
{
  g_return_val_if_fail (cmgr != NULL, FALSE);
  g_return_val_if_fail (tag != NULL, FALSE);
  return constraint_manager_get (cmgr, tag) != NULL;
}

Constraint *
constraint_manager_get (ConstraintManager *cmgr, const gchar *tag)
{
  Constraint *con;

  g_return_val_if_fail (cmgr != NULL, NULL);
  g_return_val_if_fail (tag != NULL, NULL);

  con = (Constraint *) g_hash_table_lookup (cmgr->table, tag);

  if (con != NULL)
    return con;
  else if (cmgr->outer)
    return constraint_manager_get (cmgr->outer, tag);
  else
    return NULL;
}

void
constraint_manager_add (ConstraintManager *cmgr, Constraint *con)
{
  g_return_if_fail (cmgr != NULL);
  g_return_if_fail (con != NULL);

  if (constraint_manager_contains (cmgr, constraint_tag (con))) {
    g_warning ("Namespace collision (\"%s\")", constraint_tag (con));
    return;
  }

  g_hash_table_insert (cmgr->table, (gpointer) constraint_tag (con), con);
}
