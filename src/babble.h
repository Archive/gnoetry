/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * babble.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_BABBLE_H
#define _INC_BABBLE_H

#include <glib.h>
#include "words.h"

enum {
  BABBLE_FORWARD,
  BABBLE_BACKWARD
};

typedef struct _BabbleArgs BabbleArgs;
struct _BabbleArgs {

  gint direction;
  
  TokenInfo *lead_word;

  gint min_syllables, max_syllables;
  gint min_words, max_words;

};

void babble_args_init (BabbleArgs *);

gchar *babble (BabbleArgs *args);




#endif /* _INC_BABBLE_H */

/* $Id$ */
