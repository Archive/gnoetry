/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * line-spec.h
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __LINE_SPEC_H__
#define __LINE_SPEC_H__

#include <glib.h>
#include "constraint.h"

typedef enum {
  LINE_POS_WORD,
  LINE_POS_SYLLABLE,
  LINE_POS_ANYWHERE,
  LINE_POS_LAST
} LinePosType;

typedef struct _LineConstraint LineConstaint;
struct _LineConstraint {
  gchar *tag;
  ConstraintType constraint;
  gint pos;
  LinePosType pos_type;
};

LineConstaint *line_constraint_new_final_rhyme (const gchar *tag);
LineConstaint *line_constraint_new_final_duplicate (const gchar *tag);
void           line_constraint_free (LineConstaint *);


#define HARD_STRESS   '/'
#define MEDIUM_STRESS '-'
#define NO_STRESS     'u'
#define ANY_STRESS    '*'

typedef struct _LineSpec LineSpec;
struct _LineSpec {

  gint min_words;
  gint max_words;

  gint min_syllables;
  gint max_syllables;

  gchar *meter;

  GList *constraints;
};

LineSpec *line_spec_new (void);
void      line_spec_free (LineSpec *);

void      line_spec_set_word_limits (LineSpec *, gint min, gint max);
void      line_spec_set_syllable_limits (LineSpec *, gint min, gint max);
void      line_spec_set_meter (LineSpec *, const gchar *meter);

void      line_spec_add_constraint (LineSpec *, LineConstaint *);
void      line_spec_clear_constraints (LineSpec *);


#endif /* __LINE_SPEC_H__ */















