/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * gnoetry-viewer.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GNOETRY_VIEWER_H
#define _INC_GNOETRY_VIEWER_H

#include <gnome.h>

typedef struct _GnoetryViewer GnoetryViewer;
typedef struct _GnoetryViewerClass GnoetryViewerClass;

struct _GnoetryViewer {
  GtkHBox parent;

  GtkText *text;
};

struct _GnoetryViewerClass {
  GtkHBoxClass parent_class;
};

#define GNOETRY_TYPE_VIEWER (gnoetry_viewer_get_type ())
#define GNOETRY_VIEWER(obj) (GTK_CHECK_CAST((obj),GNOETRY_TYPE_VIEWER,GnoetryViewer))
#define GNOETRY_VIEWER0(obj) ((obj) ? (GNOETRY_VIEWER(obj)) : NULL)
#define GNOETRY_VIEWER_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GNOETRY_TYPE_VIEWER,GnoetryViewerClass))
#define GNOETRY_IS_VIEWER(obj) (GTK_CHECK_TYPE((obj), GNOETRY_TYPE_VIEWER))
#define GNOETRY_IS_VIEWER0(obj) (((obj) == NULL) || (GNOETRY_IS_VIEWER(obj)))
#define GNOETRY_IS_VIEWER_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GNOETRY_TYPE_VIEWER))

GtkType gnoetry_viewer_get_type (void);

void gnoetry_viewer_construct (GnoetryViewer *);
GtkWidget *gnoetry_viewer_new (void);

void gnoetry_viewer_reset (GnoetryViewer *);
void gnoetry_viewer_add_poem (GnoetryViewer *,
			      const gchar *title,
			      const gchar *type,
			      gchar **text);


#endif /* _INC_GNOETRY_VIEWER_H */

/* $Id$ */
