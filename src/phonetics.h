/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * phonetics.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <glib.h>

#ifndef _INC_PHONETICS_H
#define _INC_PHONETICS_H

enum {
  PHONEME_UNKNOWN=0,
  PHONEME_AA,
  PHONEME_AE,
  PHONEME_AH,
  PHONEME_AO,
  PHONEME_AW,
  PHONEME_AY,
  PHONEME_B,
  PHONEME_CH,
  PHONEME_D,
  PHONEME_DH,
  PHONEME_EH,
  PHONEME_ER,
  PHONEME_EY,
  PHONEME_F,
  PHONEME_G,
  PHONEME_HH,
  PHONEME_IH,
  PHONEME_IY,
  PHONEME_JH,
  PHONEME_K,
  PHONEME_L,
  PHONEME_M,
  PHONEME_N,
  PHONEME_NG,
  PHONEME_OW,
  PHONEME_OY,
  PHONEME_P,
  PHONEME_R,
  PHONEME_S,
  PHONEME_SH,
  PHONEME_T,
  PHONEME_TH,
  PHONEME_UH,
  PHONEME_UW,
  PHONEME_V,
  PHONEME_W,
  PHONEME_Y,
  PHONEME_Z,
  PHONEME_ZH,
  PHONEME_LAST
};

const gchar *phoneme2str (gint);
gint         str2phoneme (const gchar *);

enum {
  STRESS_NONE,
  STRESS_PRIMARY,
  STRESS_SECONDARY,
  STRESS_UNKNOWN,
  STRESS_WILDCARD
};

const gchar *stress2str     (gint);
gint         char2stress    (gchar);
double       stress_penalty (gint have, gint want);

typedef struct _PhoneticInfo PhoneticInfo;
struct _PhoneticInfo {
  gchar   *word;
  gint     syllables;
  gint     phonemes;
  guint8  *phoneme;
  guint16 *stress;
  
  gint     table_pos;
  GList   *tokens;
};

PhoneticInfo *phonetic_info_new (const gchar *word, const gchar *spelling);
PhoneticInfo *phonetic_info_new_plural (const gchar *word, PhoneticInfo *sing);
PhoneticInfo *phonetic_info_new_stub (const gchar *word);

gint phonetic_info_syllables (PhoneticInfo *);
gint phonetic_info_syllable_stress (PhoneticInfo *, gint);
gint phonetic_info_syllable_position (PhoneticInfo *, gint);
void phonetic_info_dump (PhoneticInfo *);
void phonetic_info_free (PhoneticInfo *);

/* Number of shared tail-syllables. */
gint phonetic_info_rhyme_score (PhoneticInfo *, PhoneticInfo *);

void phonetic_info_load_dict (void);
void phonetic_info_clear_token_lists (void);
PhoneticInfo *phonetic_info_lookup (const gchar *word);

gpointer phonetic_info_choose_rhyme_token (PhoneticInfo *);

#endif /* _INC_PHONETICS_H */

/* $Id$ */
