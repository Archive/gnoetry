/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * cmu-pronounce.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <gnome.h>
#include <ctype.h>
#include <zlib.h>
#include <stdlib.h>
#include "cmu-pronounce.h"

const gchar *
cmu_phoneme2str (gint p)
{
  switch (p) {
  case CMU_AA: return "AA";
  case CMU_AE: return "AE";
  case CMU_AH: return "AH";
  case CMU_AO: return "AO";
  case CMU_AW: return "AW";
  case CMU_AY: return "AY";
  case CMU_B: return "B";
  case CMU_CH: return "CH";
  case CMU_D: return "D";
  case CMU_DH: return "DH";
  case CMU_EH: return "EH";
  case CMU_ER: return "ER";
  case CMU_EY: return "EY";
  case CMU_F: return "F";
  case CMU_G: return "G";
  case CMU_HH: return "HH";
  case CMU_IH: return "IH";
  case CMU_IY: return "IY";
  case CMU_JH: return "JH";
  case CMU_K: return "K";
  case CMU_L: return "L";
  case CMU_M: return "M";
  case CMU_N: return "N";
  case CMU_NG: return "NG";
  case CMU_OW: return "OW";
  case CMU_OY: return "OY";
  case CMU_P: return "P";
  case CMU_R: return "R";
  case CMU_S: return "S";
  case CMU_SH: return "SH";
  case CMU_T: return "T";
  case CMU_TH: return "TH";
  case CMU_UH: return "UH";
  case CMU_UW: return "UW";
  case CMU_V: return "V";
  case CMU_W: return "W";
  case CMU_Y: return "Y";
  case CMU_Z: return "Z";
  case CMU_ZH: return "ZH";
  case CMU_NO_STRESS: return "0";
  case CMU_PRIMARY_STRESS: return "1";
  case CMU_SECONDARY_STRESS: return "2";
  default: return "?";
  }

  return "???";
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

static gint
encode_phoneme (const gchar *str)
{
  gchar c1, c2;

  if (str == NULL || *str == '\0')
    return -1;

  c1 = str[0];
  c2 = str[1];

  switch (c1) {
  case 'A':

    switch (c2) {
    case 'A': return CMU_AA;
    case 'E': return CMU_AE;
    case 'H': return CMU_AH;
    case 'O': return CMU_AO;
    case 'W': return CMU_AW;
    case 'Y': return CMU_AY;
    default: g_assert_not_reached ();
    }

  case 'B':
    g_assert (c2 == '\0');
    return CMU_B;

  case 'C':
    g_assert (c2 == 'H');
    return CMU_CH;
    
  case 'D':
    if (c2 == 'H') 
      return CMU_DH;
    else if (c2 == '\0')
      return CMU_D;
    else
      g_assert_not_reached ();

  case 'E':
    switch (c2) {
    case 'H': return CMU_EH;
    case 'R': return CMU_ER;
    case 'Y': return CMU_EY;
    default: g_assert_not_reached ();
    }

  case 'F':
    g_assert (c2 == '\0');
    return CMU_F;

  case 'G':
    g_assert (c2 == '\0');
    return CMU_G;

  case 'H':
    g_assert (c2 == 'H');
    return CMU_HH;

  case 'I':
    if (c2 == 'H')
      return CMU_IH;
    else if (c2 == 'Y')
      return CMU_IY;
    else
      g_assert_not_reached ();

  case 'J':
    g_assert (c2 == 'H');
    return CMU_JH;

  case 'K':
    g_assert (c2 == '\0');
    return CMU_K;
    
  case 'L':
    g_assert (c2 == '\0');
    return CMU_L;
    
  case 'M':
    g_assert (c2 == '\0');
    return CMU_M;
    
  case 'N':
    if (c2 == 'G')
      return CMU_NG;
    else if (c2 == '\0')
      return CMU_N;
    else
      g_assert_not_reached ();

  case 'O':
    if (c2 == 'W') 
      return CMU_OW;
    else if (c2 == 'Y')
      return CMU_OY;
    else
      g_assert_not_reached ();

  case 'P':
    g_assert (c2 == '\0');
    return CMU_P;
    
  case 'R':
    g_assert (c2 == '\0');
    return CMU_R;
    
  case 'S':
    if (c2 == 'H') 
      return CMU_SH;
    else if (c2 == '\0')
      return CMU_S;
    else
      g_assert_not_reached ();

  case 'T':
    if (c2 == 'H')
      return CMU_TH;
    else if (c2 == '\0')
      return CMU_T;
    else
      g_assert_not_reached ();

  case 'U':
    if (c2 == 'H')
      return CMU_UH;
    else if (c2 == 'W')
      return CMU_UW;
    else 
      g_assert_not_reached ();

  case 'V':
    g_assert (c2 == '\0');
    return CMU_V;

  case 'W':
    g_assert (c2 == '\0');
    return CMU_W;

  case 'Y':
    g_assert (c2 == '\0');
    return CMU_Y;

  case 'Z':
    if (c2 == 'H')
      return CMU_ZH;
    else if (c2 == '\0')
      return CMU_Z;
    else
      g_assert_not_reached ();

  default:
    g_assert_not_reached ();
  }

  g_assert_not_reached ();
  return -1;
}

static PronunciationCMU *
parse_cmu_dict_line (const gchar *str)
{
  PronunciationCMU *p = NULL;
  gchar **strv;
  const gchar *c;
  gint i, j;

  g_return_val_if_fail (str, NULL);

  /* Skip alt pronunications */
  c = str;
  while (*c && !isspace ((gint)*c)) {
    if (*c == '(')
      return NULL;
    ++c;
  }

  strv = g_strsplit (str, " ", 0);

  p = g_new0 (PronunciationCMU, 1);
  p->word = strv[0];
  strv[0] = NULL;

  g_strdown (p->word);

  p->N = 0;
  for (i=1; strv[i]; ++i) {
    if (strv[i][0]) {
      gchar *cc = strv[i];
      ++p->N;

      /* Look for stress marks */
      while (*cc && isalpha ((gint) *cc))
	++cc;
      if (isdigit ((gint) *cc)) {
	++p->syllables;
	++p->N;
      }
    }
  }

  p->phoneme = g_new0 (guint8, p->N);

  j = 0;
  for (i=1; strv[i]; ++i) {
    gchar *cc = strv[i];
    gint pending_stress = -1;

    while (*cc && isalpha ((gint) *cc))
      ++cc;

    if (isdigit ((gint) *cc)) {
      if (*cc == '0') 
	pending_stress = CMU_NO_STRESS;
      else if (*cc == '1')
	pending_stress = CMU_PRIMARY_STRESS;
      else if (*cc == '2')
	pending_stress = CMU_SECONDARY_STRESS;
      else 
	g_assert_not_reached ();
    }
      
    *cc = '\0';

    if (strv[i][0]) {
      gint en;

      en = encode_phoneme (strv[i]);    

      g_assert (en >= 0);

      g_assert (j<p->N);
      p->phoneme[j] = en;
      ++j;

      if (pending_stress >= 0) {
	g_assert (j<p->N);
	p->phoneme[j] = pending_stress;
	++j;
      }
	
    }

  }

  g_strfreev (strv);
  return p;
}

static gint cmu_count = 0;
static GHashTable *cmu_hash = NULL;
static PronunciationCMU **cmu_table = NULL;

static void
cmu_register (PronunciationCMU *p)
{
  if (p == NULL)
    return;

  if (cmu_hash == NULL)
    cmu_hash = g_hash_table_new (g_str_hash, g_str_equal);

  g_hash_table_insert (cmu_hash, p->word, p);
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

/* Build table, sorted by tail-phonemes. */

static gint
table_cmp (gconstpointer a, gconstpointer b)
{
  const PronunciationCMU *p = *(const PronunciationCMU **)a;
  const PronunciationCMU *q = *(const PronunciationCMU **)b;

  gint i, j;

  i = p->N-1;
  j = q->N-1;

  while (i >= 0 && j >= 0 && p->phoneme[i] == q->phoneme[j]) {
    --i;
    --j;
  }

  if (i >= 0 && j >= 0) {

    i = p->phoneme[i];
    j = q->phoneme[j];

  }
    
  return (i > j) - (i < j);
}

static gint
match_count (const PronunciationCMU *p, const PronunciationCMU *q)
{
  gint i, j;
  gint count=0;

  i = p->N-1;
  j = q->N-1;

  while (i >= 0 && j >= 0 && p->phoneme[i] == q->phoneme[j]) {

    ++count;

    --i;
    --j;
  }

  return count;
}

static void
build_cb (gpointer key, gpointer val, gpointer user_data)
{
  gint *i = (gint *)user_data;

  cmu_table[*i] = (PronunciationCMU *)val;
  ++*i;
}

static void
build_table (void)
{
  gint i = 0;

  cmu_table = g_new0 (PronunciationCMU *, g_hash_table_size (cmu_hash));

  g_hash_table_foreach (cmu_hash, build_cb, &i);
  cmu_count = i;

  qsort (cmu_table, cmu_count, sizeof (PronunciationCMU *), table_cmp);

  /* Save sorted pos */
  for (i=0; i<cmu_count; ++i)
    cmu_table[i]->table_pos = i;
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

static void
interrupted_cb (GtkWidget *w, gpointer user_data)
{
  exit (0);
}

void
cmu_load_dict (void)
{
  GtkWidget *info;
  GtkWidget *prog;
  GtkWidget *box;
  GtkWidget *label;
  static gboolean loaded = FALSE;
  gchar *s;
  gint N;

  gzFile in = NULL;
  gchar buffer[512];

  if (loaded)
    return;
  loaded = TRUE;

  info = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_position (GTK_WINDOW (info), GTK_WIN_POS_CENTER);

  s = g_strdup_printf (_("Welcome to Gnoetry %s"), VERSION);
  gtk_window_set_title (GTK_WINDOW (info), s);
  g_free (s);

  box = gtk_vbox_new (FALSE, 3);

  label = gtk_label_new (_("Loading CMU Pronunication Dictionary"));
  gtk_box_pack_start (GTK_BOX (box), label, TRUE, TRUE, 2);

  prog = gtk_progress_bar_new ();
  gtk_progress_bar_set_bar_style (GTK_PROGRESS_BAR (prog),
				  GTK_PROGRESS_CONTINUOUS);
  gtk_box_pack_start (GTK_BOX (box), prog, TRUE, TRUE, 2);

  gtk_container_add (GTK_CONTAINER (info), box);

  gtk_signal_connect (GTK_OBJECT (info),
		      "delete_event",
		      GTK_SIGNAL_FUNC (interrupted_cb),
		      NULL);

  gtk_widget_show_all (info);

  while (gtk_events_pending ())
    gtk_main_iteration ();
    

#ifdef DICTPATH
  in = gzopen (DICTPATH "/cmudict0.3.gz", "r");
#endif
  if (in == NULL)
    in = gzopen("../dict/cmudict0.3.gz", "r");

  if (in == NULL) {
    g_message ("Can't find CMU Pronunciation Dictionary");
    exit (-1);
  }

  N = 0;
  while (gzgets (in, buffer, 512)) {

    if (buffer[0] && buffer[0] != '#' && isalpha (buffer[0])) {
      PronunciationCMU *p = parse_cmu_dict_line (buffer);

      cmu_register (p);

      ++N;

      if ((N & 0x1ff) == 0) {
	/* I've hard-wired the length of the CMU dict in here, which is a
	   pretty silly thing to do. */
	gtk_progress_set_percentage (GTK_PROGRESS (prog), N / 110892.0);
	while (gtk_events_pending ())
	  gtk_main_iteration ();
      }

    }
  }

  gzclose (in);

  build_table ();

  gtk_widget_destroy (info);
  while (gtk_events_pending ())
    gtk_main_iteration ();
  
}

PronunciationCMU *
cmu_word_lookup (const gchar *str)
{
  PronunciationCMU *p;
  gchar *s;

  g_return_val_if_fail (str, NULL);

  cmu_load_dict ();

  s = g_strdup (str);
  g_strdown (s);

  p = (PronunciationCMU *)g_hash_table_lookup (cmu_hash, str);

  g_free (s);
  return p;
}

void
cmu_clear_rhyme_stems (void)
{
  gint i;

  for (i=0; i<cmu_count; ++i) {
    g_list_free (cmu_table[i]->rhyme_stems);
    cmu_table[i]->rhyme_stems = NULL;
  }
}

gint
cmu_syllables (const gchar *str)
{
  PronunciationCMU *p;
  g_return_val_if_fail (str, 0);

  p = cmu_word_lookup (str);
  return p ? p->syllables : 0;
}

void
cmu_rhymes_foreach (PronunciationCMU *p,
		    void (*fn) (PronunciationCMU *, gpointer user_data),
		    gpointer user_data)
{
  gint i, base, limit;

  g_return_if_fail (p);
  g_return_if_fail (fn);

  base = p->table_pos;
  limit = (5*p->N)/8;
  if (limit < 4)
    limit = p->N-1;

  for (i=base+1; i<cmu_count && match_count (p, cmu_table[i]) >= limit; ++i)
    fn (cmu_table[i], user_data);

  for (i=base-1; i>=0 && match_count (p, cmu_table[i]) >= limit; --i)
    fn (cmu_table[i], user_data);
}

static void
count_cb (PronunciationCMU *p, gpointer user_data)
{
  if (p->rhyme_stems)
    ++*(gint *)user_data;
}

gint
cmu_rhymes_count (PronunciationCMU *p)
{
  gint N = 0;
  cmu_rhymes_foreach (p, count_cb, &N);
  return N;
}

static void
list_cb (PronunciationCMU *p, gpointer user_data)
{
  GList **lst = (GList **)user_data;

  if (p->rhyme_stems)
    *lst = g_list_prepend (*lst, p);
}

PronunciationCMU *
cmu_rhymes_choose (PronunciationCMU *p)
{
  GList *lst = NULL;
  gint len;
  PronunciationCMU *q;

  g_return_val_if_fail (p, NULL);

  cmu_rhymes_foreach (p, list_cb, &lst);

  if (lst == NULL)
    return NULL;

  len = g_list_length (lst);
  q = (PronunciationCMU *)g_list_nth_data (lst, random () % len);
  g_list_free (lst);
  
  return q;
}

static void
show_fn (PronunciationCMU *p, gpointer user_data)
{
  g_print ("%s\n", p->word);
}

void
show_cmu_rhymes (const gchar *str)
{
  PronunciationCMU *p;

  p = (PronunciationCMU *)cmu_word_lookup (str);
  if (p == NULL)
    return;

  cmu_rhymes_foreach (p, show_fn, NULL);
}


/* $Id$ */


