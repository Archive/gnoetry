/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * fate.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <math.h>
#include <unistd.h>
#include <time.h>
#include "fate.h"

static guint seed = 0;

void
fate_tempt (guint in_seed)
{
  if (in_seed == 0)
    fate_surrender ();
  else {
    seed = in_seed;
    srandom (seed);
  }
}

void
fate_surrender (void)
{
  FILE *in;
  
  in = fopen ("/dev/urandom", "r");
  if (in != NULL) {
    if (fread (&seed, sizeof (seed), 1, in) != 1)
      seed = 0;
    fclose (in);
  }

  if (seed == 0) {
    seed = (guint) time (NULL);
    seed += (guint) getpid ();
  }

  srandom (seed);
}

guint
fate_unravel (void)
{
  return seed;
}

gboolean
fate_coin_toss (void)
{
  return (random () < (RAND_MAX >> 1));
}

gboolean
fate_render_judgement (double p)
{
  return (random () <= (gint) (p * RAND_MAX));
}

gint
fate_spin_wheel (gint N)
{
  return (gint) floor (N * (random () / (1+(double)RAND_MAX)));
}
