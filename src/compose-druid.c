/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * compose-druid.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <gnome.h>
#include "gnoetry-form.h"
#include "gnoetry-form-view.h"
#include "compose-druid.h"


#define LOGO "ubu-roi-64.png"
static GdkImlibImage *
ubu_roi (void)
{
  static GdkImlibImage *img = NULL;

  if (img == NULL) {
    if (g_file_exists (PIXMAPPATH "/" LOGO)) {
      img = gdk_imlib_load_image (PIXMAPPATH "/" LOGO);
    } else {
      img = gdk_imlib_load_image ("../pixmaps/" LOGO);
    }
  }

  return img;
}

#define WATERMARK "ubu-roi-watermark.png"
static GdkImlibImage *
ubu_roi_watermark (void)
{
  static GdkImlibImage *img = NULL;

  if (img == NULL) {
    if (g_file_exists (PIXMAPPATH "/" WATERMARK)) {
      img = gdk_imlib_load_image (PIXMAPPATH "/" WATERMARK);
    } else {
      img = gdk_imlib_load_image ("../pixmaps/" WATERMARK);
    }
  }

  return img;
}

static GnomeDruidPage *
start_page (void)
{
  GnomeDruidPageStart *page;

  page = GNOME_DRUID_PAGE_START (gnome_druid_page_start_new ());

  gnome_druid_page_start_set_title (page, _("Compose a Poem"));
  gnome_druid_page_start_set_text (page, _("This dialog will take you through the steps required\nto create a new Poem."));
  gnome_druid_page_start_set_logo (page, ubu_roi ());
  gnome_druid_page_start_set_watermark (page, ubu_roi_watermark ());

  return GNOME_DRUID_PAGE (page);
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

struct FormPairs {
  const gchar *name;
  GnoetryForm *(*constructor) (void);
};

struct FormPairs form_options[] = {
  { N_("Japanese Syllabic Forms"), NULL },
  { N_("Haiku (5-7-5)"), gnoetry_form_new_haiku },
  { N_("Tanka (5-7-5 7-7)"), gnoetry_form_new_tanka },
  { N_("Renga (5-7-5 7-7 5-7-5)"), gnoetry_form_new_renga },

  { N_("Basic Rhyming"), NULL },
  { N_("Rhyming: ABAB"), gnoetry_form_new_ABAB },
  { N_("Rhyming: ABAB CDCD"), gnoetry_form_new_ABAB_CDCD },

  { N_("Forms Using Iambic Pentameter"), NULL },
  { N_("Blank Verse (4x4)"), gnoetry_form_new_blank_verse_4x4 },
  { N_("Sonnet"), gnoetry_form_new_sonnet },
  { N_("Italian Sestet (ABCABC)"), gnoetry_form_new_italian_sestet },
  { N_("Sicilian Sestet (ABABAB)"), gnoetry_form_new_sicilian_sestet },
  { N_("Heroic Sestet (ABABCC)"), gnoetry_form_new_heroic_sestet },

  { N_("Other Forms"), NULL },
  { N_("One Five Line Stanza, Non-Rhyming"), gnoetry_form_new_5line },
  { N_("Sapphic"), gnoetry_form_new_sapphic },
  { N_("Hendecasyllabic"), gnoetry_form_new_hendecasyllabic },
  { N_("Sestina (sorry, no Envoy)"), gnoetry_form_new_sestina },
  { N_("Unfiltered Ranting"), gnoetry_form_new_rant },
  { NULL, NULL }
};

static GnoetryForm *
get_form (GnomeDruid *druid)
{
  return GNOETRY_FORM0 (gtk_object_get_data (GTK_OBJECT (druid), "form"));
}

static void
set_form (GnomeDruid *druid, GnoetryForm *form)
{
  GnoetryForm *old_form;

  old_form = get_form (druid);
  if (old_form == form)
    return;
  
  gtk_object_set_data_full (GTK_OBJECT (druid), "form", form,
			    (GtkDestroyNotify) gtk_object_unref);
  gtk_object_ref (GTK_OBJECT (form));
  if (old_form)
    gtk_object_unref (GTK_OBJECT (old_form));
}

static void
form_toggle_cb (GtkWidget *w, gpointer user_data)
{
  const gchar *name;
  GnoetryForm *(*constructor) (void);

  if (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (w)))
    return;

  name = (const gchar *) gtk_object_get_data (GTK_OBJECT (w), "name");
  constructor = (GnoetryForm *(*)(void)) gtk_object_get_data (GTK_OBJECT (w),
							      "constructor");

  set_form (GNOME_DRUID (user_data), constructor ());
}

static GnomeDruidPage *
form_page (GnomeDruid *druid)
{
  GnomeDruidPageStandard *page;
  GtkWidget *box;
  GtkWidget *box2;
  GtkWidget *scrolled;
  GtkRadioButton *first_button = NULL;
  gboolean add_sep = FALSE;
  gint i;

  page = GNOME_DRUID_PAGE_STANDARD (gnome_druid_page_standard_new ());

  gnome_druid_page_standard_set_title (page, _("Choose a Poetic Form"));
  gnome_druid_page_standard_set_logo (page, ubu_roi ());  

  box = gtk_vbox_new (FALSE, 2);
  
  for (i=0; form_options[i].name; ++i) {
    GtkWidget *rb;
    const gchar *form_name = form_options[i].name;
    GnoetryForm *(*constructor)(void) = form_options[i].constructor;

    add_sep = FALSE;

    if (constructor) {
      if (first_button == NULL) {
	rb = gtk_radio_button_new_with_label (NULL, _(form_name));
	first_button = GTK_RADIO_BUTTON (rb);
	set_form (druid, constructor ());
      } else {
	rb = gtk_radio_button_new_with_label_from_widget (first_button,
							  _(form_name));
      }

      gtk_object_set_data (GTK_OBJECT (rb), "name", (gpointer) form_name);
      gtk_object_set_data (GTK_OBJECT (rb), "constructor", constructor);

      gtk_signal_connect (GTK_OBJECT (rb),
			  "toggled",
			  form_toggle_cb,
			  druid);
    } else {
      
      if (i > 0)
	add_sep = TRUE;
      rb = gtk_label_new (form_name);

    }

    if (add_sep)
      gtk_box_pack_start (GTK_BOX (box), gtk_hseparator_new (),
			  FALSE, TRUE, 5);
    gtk_box_pack_start (GTK_BOX (box), rb, FALSE, FALSE, 2);
  }

  box2 = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (box2), box, TRUE, FALSE, 0);

  scrolled = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled),
				  GTK_POLICY_AUTOMATIC,
				  GTK_POLICY_AUTOMATIC);
  gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (scrolled),
					 box2);

  gtk_box_pack_start (GTK_BOX (page->vbox), scrolled, TRUE, TRUE, 2);
  

  return GNOME_DRUID_PAGE (page);
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

static void
edit_map_cb (GtkWidget *w, gpointer user_data)
{
  GnomeDruidPageStandard *page = GNOME_DRUID_PAGE_STANDARD (w);
  GnomeDruid *druid = GNOME_DRUID (user_data);
  GnoetryForm *form = get_form (druid);
  gpointer ptr;
  GtkWidget *editor;

  ptr = gtk_object_get_data (GTK_OBJECT (page), "editor");
  editor = ptr ? GTK_WIDGET (ptr) : NULL;

  if (editor) {
    gtk_container_remove (GTK_CONTAINER (page->vbox), editor);
  }

  editor = gnoetry_form_view_new (form);
  gtk_box_pack_start (GTK_BOX (page->vbox), editor, TRUE, TRUE, 0);
  gtk_widget_show_all (editor);
  gtk_object_set_data (GTK_OBJECT (page), "editor", editor);
}

static GnomeDruidPage *
edit_page (GnomeDruid *druid)
{
  GnomeDruidPageStandard *page;

  page = GNOME_DRUID_PAGE_STANDARD (gnome_druid_page_standard_new ());

  gnome_druid_page_standard_set_title (page, _("Edit the Work-in-Progress"));
  gnome_druid_page_standard_set_logo (page, ubu_roi ());  

  gtk_signal_connect (GTK_OBJECT (page),
		      "map",
		      GTK_SIGNAL_FUNC (edit_map_cb),
		      druid);

  return GNOME_DRUID_PAGE (page);
}


/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

static void
cleanout_container (GtkWidget *w, gpointer user_data)
{
  gtk_container_remove (GTK_CONTAINER (user_data), w);
}

static void
finish_map_cb (GtkWidget *w, gpointer user_data)
{
  GnomeDruidPageStandard *page = GNOME_DRUID_PAGE_STANDARD (w);
  GnomeDruid *druid = GNOME_DRUID (user_data);
  GnoetryForm *form = get_form (druid);
  gchar **strv = gnoetry_form_strv (form);
  gint i;
  GtkWidget *swin;
  GtkWidget *vbox;

  /* Make our druid treat this like a finish page.  The druid API really sucks. */
  gnome_druid_set_show_finish (druid, TRUE);

  if (strv == NULL || strv[0] == NULL)
    return;

  gtk_container_forall (GTK_CONTAINER (page->vbox), cleanout_container, page->vbox);

  swin = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (swin),
				  GTK_POLICY_AUTOMATIC,
				  GTK_POLICY_AUTOMATIC);
  vbox = gtk_vbox_new (FALSE, 2);
  gtk_box_pack_start (GTK_BOX (vbox), gtk_label_new (""), TRUE, TRUE, 0);
  for (i=0; strv[i]; ++i) {
    gtk_box_pack_start (GTK_BOX (vbox), gtk_label_new (strv[i]), FALSE, FALSE, 0);
  }
  gtk_box_pack_start (GTK_BOX (vbox), gtk_label_new (""), TRUE, TRUE, 0);
  
  gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (swin), vbox);
  gtk_widget_show_all (swin);

  gtk_container_add (GTK_CONTAINER (page->vbox), swin);
}

static void
finish_cb (GtkWidget *w, gpointer user_data)
{
  GnomeDruid *druid = GNOME_DRUID (user_data);
  GnoetryForm *form = get_form (druid);

  gpointer ptr = gtk_object_get_data (GTK_OBJECT (druid), "viewer");

  if (ptr) {
    gchar **strv = gnoetry_form_strv (form);
    gchar *type = NULL;

    if (form->form_name && form->form_params)
      type = g_strdup_printf ("%s; %s", form->form_name, form->form_params);
    else if (form->form_name)
      type = g_strdup_printf ("%s", form->form_name);

    gnoetry_viewer_add_poem (GNOETRY_VIEWER (ptr),
			     NULL,
			     type,
			     strv);
    g_strfreev (strv);
    g_free (type);
    
    if (getenv ("GNOETRY_SAVE_AS_LATEX"))
      gnoetry_form_save_as_latex (form); 
  }
}

static GnomeDruidPage *
finish_page (GnomeDruid *druid, GtkWidget *win)
{

  static const gchar *finishers[] = {
    N_("We've Created a Work of Timeless Beauty!"),
    N_("Poetry Will Never Be The Same!"),
    N_("We've Created a Masterpiece!"),
    N_("Generations of Students Will Be\nForced to Memorize This Work!"),
    N_("Emily Dickinson, Eat Your Heart Out!"),
    N_("This Poem Will Be Revered for Generations!"),
    N_("The Staggering Genius of this Work\nHas Rendered Me Speechless!"),
    N_("This is the Best One Yet!"),
    N_("Sheer Brilliance!"),
    N_("Now That is What I Call a Damn Fine Poem!"),
    N_("They Just Keep Getting Better!"),
    N_("There Won't Be a Dry Eye in the House\nWhen You Read This One!"),
    N_("This Isn't a Poem... This is THE Poem."),
    N_("You Are The Bard Of Our Generation!"),
    N_("You Should Submit This One For Publication!"),
    N_("How About Setting This One To Music?"),
    N_("Chant This In A Crowded Subway Car!"),
    N_("This Work Helps Elevate the Human Condition!"),
    N_("If Good Poetry Were A Crime,\nYou'd Be Locked Up For Life!"),
    N_("One Day, You'll Be On A Stamp!"),
    N_("This Work is Truely Ahead of its Time!"),
    N_("Somebody Call The Pulitzer Committee!"),
    N_("If Great Poetry Were Water,\nWe'd All Be Drowning!"),
    N_("You Are Indeed an Unacknowledged\nLegislator of the World!"),
    N_("Wait, This is Too Good...\nAre You Sure You Didn't Plagiarize This?"),
    N_("You'll Always Be My Poet Laureate!"),
    N_("To Stop Composing Now Would Be To\nDeny Humanity Your Gift!"),
    N_("Another Fine One, T.S. My Old Boy!"),
    N_("If Robert Frost Were Still Alive, This\nIs The Sort of Stuff He'd Be Writing!"),
    N_("The Muse Is With Us Today!"),
    N_("Whoa... This Poem Kicks Ass!"),
    N_("That Is One Fine Funky-Fresh Poem!"),
    N_("Roses Are Red\nViolets Are Blue\nThis Poem Is Great\nAnd Thus So Are You!"),
    N_("Three Words: Lucrative Book Contract."),
    N_("You Are The Michael Jordan of Poetry."),
    N_("Having Heard That Poem, I Can Now Die With A Sense\nof Peace And Well-Being."),
    N_("Where Did You Learn to Do That?"),
    N_("Genius.  Pure Genius."),
    N_("You Are the Edna St. Vincent Millay of the New Millenium!"),
    N_("You Are the New Ezra Pound -- But Sexier!"),
    N_("If Sylvia Plath Had Read This, Maybe She Wouldn't\nHave Been So Damn Gloomy All Of The Time!"),
    NULL
  };
  gint finN = 0;
  static gint finIndex = 0;
  static gint *finOrder = NULL;

  GnomeDruidPageStandard *page;

  page = GNOME_DRUID_PAGE_STANDARD (gnome_druid_page_standard_new ());

  if (finN == 0)
    while (finishers[finN]) finN++;

  if (finOrder == NULL) 
    finOrder = g_new (gint, finN);

  /* Reset & Scramble the index */
  if (finIndex == 0) {
    gint j;
    for (j=0; j<finN; ++j)
      finOrder[j] = j;
    for (j=0; j<finN-1; ++j) {
      gint k = j + (random() % (finN-j));
      if (k != j) {
	gint tmp = finOrder[j];
	finOrder[j] = finOrder[k];
	finOrder[k] = tmp;
      }
    }
  }

  gnome_druid_page_standard_set_title (page, _(finishers [finOrder [finIndex]]));
  ++finIndex;
  if (finIndex == finN)
    finIndex = 0;

  gnome_druid_page_standard_set_logo (page, ubu_roi ());  

  gtk_signal_connect (GTK_OBJECT (page),
		      "map",
		      GTK_SIGNAL_FUNC (finish_map_cb),
		      druid);

  gtk_signal_connect (GTK_OBJECT (page),
		      "finish",
		      GTK_SIGNAL_FUNC (finish_cb),
		      druid);

  gtk_signal_connect_object (GTK_OBJECT (page),
			     "finish",
			     GTK_SIGNAL_FUNC (gtk_widget_destroy),
			     GTK_OBJECT (win));

  return GNOME_DRUID_PAGE (page);
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

GtkWidget *
compose_druid_new (GnoetryViewer *viewer)
{
  GtkWidget *win;
  GnomeDruid *druid;

  win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  druid = GNOME_DRUID (gnome_druid_new ());

  gtk_object_set_data (GTK_OBJECT (druid), "viewer", viewer);

  gnome_druid_append_page (druid, start_page ());
  gnome_druid_append_page (druid, form_page (druid));
  gnome_druid_append_page (druid, edit_page (druid));
  gnome_druid_append_page (druid, finish_page (druid, win));

  gtk_container_add (GTK_CONTAINER (win), GTK_WIDGET (druid));

  gtk_signal_connect_object (GTK_OBJECT (druid),
			     "cancel",
			     GTK_SIGNAL_FUNC (gtk_widget_destroy),
			     GTK_OBJECT (win));

  return win;
}



/* $Id$ */
