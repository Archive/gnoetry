/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * words.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_WORDS_H
#define _INC_WORDS_H

#include <glib.h>
#include "phonetics.h"

typedef guint32 token_t;

enum {
  TOKEN_ERROR,
  TOKEN_START,
  TOKEN_STOP,
  TOKEN_PERIOD,
  TOKEN_EXCLAMATION,
  TOKEN_QUESTION,
  TOKEN_ELLIPSES,
  TOKEN_DASH,
  TOKEN_COMMA,
  TOKEN_COLON,
  TOKEN_SEMICOLON,
  TOKEN_LAST
};

typedef struct _TokenInfo TokenInfo;
struct _TokenInfo {
  gint code;

  gchar *word;
  gchar *root_word;

  gint syllables;
  gint words;

  gint pred_count, pred_sz;
  TokenInfo **predv;

  gint succ_count, succ_sz;
  TokenInfo **succv;

  GList *phonetics;
  PhoneticInfo *root_phonetics;
};

void reset_token_info (void);

TokenInfo *declare_token (const gchar *word);
void declare_paring (TokenInfo *first, TokenInfo *second);

TokenInfo *get_token_info_by_code (token_t);
TokenInfo *get_token_info_by_word (const gchar *word);

token_t token_info_code (TokenInfo *);
const gchar *token_info_word (TokenInfo *);
gint token_info_syllables (TokenInfo *);

TokenInfo *token_info_choose_predecessor (TokenInfo *);
TokenInfo *token_info_choose_successor (TokenInfo *);

gboolean token_info_has_rhymes (TokenInfo *);
TokenInfo *token_info_choose_rhyme (TokenInfo *);

void token_info_foreach (void (*fn)(TokenInfo *, gpointer), gpointer userdata);


#endif /* _INC_WORDS_H */

/* $Id$ */
