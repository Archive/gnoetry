/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * gnoetry-text-select.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "text.h"
#include "gnoetry-text-select.h"

static GtkObjectClass *parent_class = NULL;

enum {
  ARG_0
};

enum {
  LAST_SIGNAL
};

static guint gnoetry_text_select_signals[LAST_SIGNAL] = { 0 };

static void
gnoetry_text_select_get_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
gnoetry_text_select_set_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
gnoetry_text_select_destroy (GtkObject *obj)
{
  if (parent_class->destroy)
    parent_class->destroy (obj);
}

static void
gnoetry_text_select_finalize (GtkObject *obj)
{
  /* GnoetryTextSelect *x = GNOETRY_TEXT_SELECT(obj); */

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
gnoetry_text_select_class_init (GnoetryTextSelectClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;

  parent_class = gtk_type_class (GTK_TYPE_HBOX);

  object_class->get_arg = gnoetry_text_select_get_arg;
  object_class->set_arg = gnoetry_text_select_set_arg;
  object_class->destroy = gnoetry_text_select_destroy;
  object_class->finalize = gnoetry_text_select_finalize;

#if 0
  /* Signal definition template */
  gnoetry_text_select_signals[CHANGED] =
    gtk_signal_new ("changed",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GnoetryTextSelectClass, changed),
                    gtk_marshal_NONE__NONE, GTK_TYPE_NONE, 0);
#endif

  gtk_object_class_add_signals (object_class, gnoetry_text_select_signals,
                                LAST_SIGNAL);
}

static void
gnoetry_text_select_init (GnoetryTextSelect *obj)
{

}

GtkType
gnoetry_text_select_get_type (void)
{
  static GtkType gnoetry_text_select_type = 0;
  if (!gnoetry_text_select_type) {
    static const GtkTypeInfo gnoetry_text_select_info = {
      "GnoetryTextSelect",
      sizeof (GnoetryTextSelect),
      sizeof (GnoetryTextSelectClass),
      (GtkClassInitFunc)gnoetry_text_select_class_init,
      (GtkObjectInitFunc)gnoetry_text_select_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    gnoetry_text_select_type = gtk_type_unique (GTK_TYPE_HBOX, &gnoetry_text_select_info);
  }
  return gnoetry_text_select_type;
}

typedef struct _BuildState BuildState;
struct _BuildState {
  GtkTable *table;
  gint r, c, i;
};

static void
attach_hrule (BuildState *bs)
{
  g_assert (bs->i < bs->r);

  gtk_table_attach (bs->table,
		    gtk_hseparator_new (),
		    0, bs->c, bs->i, bs->i+1,
		    GTK_EXPAND | GTK_FILL, 0,
		    0, 2);
  ++bs->i;
}

static void
toggled_cb (GtkWidget *w, gpointer ptr)
{
  TextInfo *info = (TextInfo *) ptr;

  info->active = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (w));
}

static void
build_cb (TextInfo *info, gpointer ptr)
{
  BuildState *bs = (BuildState *) ptr;

  gchar *text;
  GtkWidget *text_toggle;
  GtkWidget *title_label;
  GtkWidget *author_label;
  GtkWidget *size_label;

  if (bs->i == 0) {

    gtk_table_attach_defaults (bs->table, gtk_label_new (_("Title")),
			       2, 3, bs->i, bs->i+1);

    gtk_table_attach_defaults (bs->table, gtk_label_new (_("Author")),
			       4, 5, bs->i, bs->i+1);

    gtk_table_attach_defaults (bs->table, gtk_label_new (_("Length")),
			       6, 7, bs->i, bs->i+1);

    ++bs->i;
    
    attach_hrule (bs);
  }

  text_toggle = gtk_check_button_new ();
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (text_toggle), info->active);

  title_label = gtk_label_new (text_info_title (info));
  author_label = gtk_label_new (text_info_author (info));

  text = g_strdup_printf ("%d", text_info_text_length (info));
  size_label = gtk_label_new (text);
  g_free (text);

  gtk_table_attach_defaults (bs->table,
			     text_toggle,
			     0, 1, bs->i, bs->i+1);

  gtk_signal_connect (GTK_OBJECT (text_toggle),
		      "toggled",
		      toggled_cb,
		      info);

  gtk_table_attach_defaults (bs->table,
			     title_label,
			     2, 3, bs->i, bs->i+1);

  gtk_table_attach_defaults (bs->table,
			     author_label,
			     4, 5, bs->i, bs->i+1);

  gtk_table_attach_defaults (bs->table,
			     size_label,
			     6, 7, bs->i, bs->i+1);

  ++bs->i;

  if (bs->i != bs->r) 
    attach_hrule (bs);
}
  
void
gnoetry_text_select_construct (GnoetryTextSelect *sel)
{
  GtkTable *table;
  gint r, c, i;
  BuildState bs;

  g_return_if_fail (sel && GNOETRY_IS_TEXT_SELECT (sel));

  c = 7;
  r = 2*text_info_count ()+1;
  table = GTK_TABLE (gtk_table_new (r, c, FALSE));

  bs.table = table;
  bs.r = r;
  bs.c = c;
  bs.i = 0;
  text_info_foreach_file (build_cb, &bs);

  for (i=0; i<3; ++i)
    gtk_table_attach (table, gtk_vseparator_new (),
		      1+2*i, 2+2*i, 0, r,
		      0, GTK_EXPAND | GTK_FILL,
		      2, 0);

  gtk_widget_show_all (GTK_WIDGET (table));

  gtk_box_pack_start (GTK_BOX (sel), GTK_WIDGET (table),
		      TRUE, TRUE, 0);
}

GtkWidget *
gnoetry_text_select_new (void)
{
  gpointer ptr;

  ptr = gtk_type_new (gnoetry_text_select_get_type ());

  gnoetry_text_select_construct (GNOETRY_TEXT_SELECT (ptr));

  return GTK_WIDGET (ptr);
}
