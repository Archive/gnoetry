/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * gnoetry-form-view.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GNOETRY_FORM_VIEW_H
#define _INC_GNOETRY_FORM_VIEW_H

#include <gnome.h>
#include "gnoetry-form.h"

typedef struct _GnoetryFormView GnoetryFormView;
typedef struct _GnoetryFormViewClass GnoetryFormViewClass;

struct _GnoetryFormView {
  GtkFrame parent;

  GnoetryForm *form;
};

struct _GnoetryFormViewClass {
  GtkFrameClass parent_class;
};

#define GNOETRY_TYPE_FORM_VIEW (gnoetry_form_view_get_type ())
#define GNOETRY_FORM_VIEW(obj) (GTK_CHECK_CAST((obj),GNOETRY_TYPE_FORM_VIEW,GnoetryFormView))
#define GNOETRY_FORM_VIEW0(obj) ((obj) ? (GNOETRY_FORM_VIEW(obj)) : NULL)
#define GNOETRY_FORM_VIEW_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GNOETRY_TYPE_FORM_VIEW,GnoetryFormViewClass))
#define GNOETRY_IS_FORM_VIEW(obj) (GTK_CHECK_TYPE((obj), GNOETRY_TYPE_FORM_VIEW))
#define GNOETRY_IS_FORM_VIEW0(obj) (((obj) == NULL) || (GNOETRY_IS_FORM_VIEW(obj)))
#define GNOETRY_IS_FORM_VIEW_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GNOETRY_TYPE_FORM_VIEW))

#define g_return_unless_is_gnoetry_form_view(x) (g_return_if_fail((x)&&GNOETRY_IS_FORM_VIEW((x))))
#define g_return_val_unless_is_gnoetry_form_view(x,y) (g_return_val_if_fail((x)&&GNOETRY_IS_FORM_VIEW((x)),(y)))

GtkType gnoetry_form_view_get_type (void);

void gnoetry_form_view_construct (GnoetryFormView *, GnoetryForm *);
GtkWidget *gnoetry_form_view_new (GnoetryForm *);

#endif /* _INC_GNOETRY_FORM_VIEW_H */

/* $Id$ */
