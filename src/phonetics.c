/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * phonetics.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <stdlib.h>
#include <ctype.h>
#include <zlib.h>
#include <gnome.h>
#include "fate.h"
#include "syllables.h"
#include "phonetics.h"

const gchar *
phoneme2str (gint p)
{
  switch (p) {
  case PHONEME_UNKNOWN:
    return "??";
  case PHONEME_AA:
    return "AA";
  case PHONEME_AE:
    return "AE";
  case PHONEME_AH:
    return "AH";
  case PHONEME_AO:
    return "AO";
  case PHONEME_AW:
    return "AW";
  case PHONEME_AY:
    return "AY";
  case PHONEME_B:
    return "B";
  case PHONEME_CH:
    return "CH";
  case PHONEME_D:
    return "D";
  case PHONEME_DH:
    return "DH";
  case PHONEME_EH:
    return "EH";
  case PHONEME_ER:
    return "ER";
  case PHONEME_EY:
    return "EY";
  case PHONEME_F:
    return "F";
  case PHONEME_G:
    return "G";
  case PHONEME_HH:
    return "HH";
  case PHONEME_IH:
    return "IH";
  case PHONEME_IY:
    return "IY";
  case PHONEME_JH:
    return "JH";
  case PHONEME_K:
    return "K";
  case PHONEME_L:
    return "L";
  case PHONEME_M:
    return "M";
  case PHONEME_N:
    return "N";
  case PHONEME_NG:
    return "NG";
  case PHONEME_OW:
    return "OW";
  case PHONEME_OY:
    return "OY";
  case PHONEME_P:
    return "P";
  case PHONEME_R:
    return "R";
  case PHONEME_S:
    return "S";
  case PHONEME_SH:
    return "SH";
  case PHONEME_T:
    return "T";
  case PHONEME_TH:
    return "TH";
  case PHONEME_UH:
    return "UH";
  case PHONEME_UW:
    return "UW";
  case PHONEME_V:
    return "V";
  case PHONEME_W:
    return "W";
  case PHONEME_Y:
    return "Y";
  case PHONEME_Z:
    return "Z";
  case PHONEME_ZH:
    return "ZH";
  }

  return "--";
}

gint
str2phoneme (const gchar *str)
{
  gchar c1, c2;

  if (str == NULL || *str == '\0')
    return PHONEME_UNKNOWN;

  c1 = str[0];
  c2 = str[1];

  switch (c1) {
  case 'A':

    switch (c2) {
    case 'A': return PHONEME_AA;
    case 'E': return PHONEME_AE;
    case 'H': return PHONEME_AH;
    case 'O': return PHONEME_AO;
    case 'W': return PHONEME_AW;
    case 'Y': return PHONEME_AY;
    default: return PHONEME_UNKNOWN;
    }

  case 'B':
    if (c2 != '\0')
      return PHONEME_UNKNOWN;
    return PHONEME_B;

  case 'C':
    if (c2 != 'H')
      return PHONEME_UNKNOWN;
    return PHONEME_CH;
    
  case 'D':
    if (c2 == 'H') 
      return PHONEME_DH;
    else if (c2 == '\0')
      return PHONEME_D;
    else
      return PHONEME_UNKNOWN;

  case 'E':
    switch (c2) {
    case 'H': return PHONEME_EH;
    case 'R': return PHONEME_ER;
    case 'Y': return PHONEME_EY;
    default: return PHONEME_UNKNOWN;
    }

  case 'F':
    if (c2 != '\0')
      return PHONEME_UNKNOWN;
    return PHONEME_F;

  case 'G':
    if (c2 != '\0')
      return PHONEME_UNKNOWN;
    return PHONEME_G;

  case 'H':
    if (c2 != 'H')
      return PHONEME_UNKNOWN;
    return PHONEME_HH;

  case 'I':
    if (c2 == 'H')
      return PHONEME_IH;
    else if (c2 == 'Y')
      return PHONEME_IY;
    else
      return PHONEME_UNKNOWN;

  case 'J':
    if (c2 != 'H')
      return PHONEME_UNKNOWN;
    return PHONEME_JH;

  case 'K':
    if (c2 != '\0')
      return PHONEME_UNKNOWN;
    return PHONEME_K;
    
  case 'L':
    if (c2 != '\0')
      return PHONEME_UNKNOWN;
    return PHONEME_L;
    
  case 'M':
    if (c2 != '\0')
      return PHONEME_UNKNOWN;
    return PHONEME_M;
    
  case 'N':
    if (c2 == 'G')
      return PHONEME_NG;
    else if (c2 == '\0')
      return PHONEME_N;
    else
      return PHONEME_UNKNOWN;

  case 'O':
    if (c2 == 'W') 
      return PHONEME_OW;
    else if (c2 == 'Y')
      return PHONEME_OY;
    else
      return PHONEME_UNKNOWN;

  case 'P':
    if (c2 != '\0')
      return PHONEME_UNKNOWN;
    return PHONEME_P;
    
  case 'R':
    if (c2 != '\0')
      return PHONEME_UNKNOWN;
    return PHONEME_R;
    
  case 'S':
    if (c2 == 'H') 
      return PHONEME_SH;
    else if (c2 == '\0')
      return PHONEME_S;
    else
      return PHONEME_UNKNOWN;

  case 'T':
    if (c2 == 'H')
      return PHONEME_TH;
    else if (c2 == '\0')
      return PHONEME_T;
    else
      return PHONEME_UNKNOWN;

  case 'U':
    if (c2 == 'H')
      return PHONEME_UH;
    else if (c2 == 'W')
      return PHONEME_UW;
    else 
      return PHONEME_UNKNOWN;

  case 'V':
    if (c2 != '\0')
      return PHONEME_UNKNOWN;
    return PHONEME_V;
    
  case 'W':
    if (c2 != '\0')
      return PHONEME_UNKNOWN; 
    return PHONEME_W;

  case 'Y':
    if (c2 != '\0')
      return PHONEME_UNKNOWN;
    return PHONEME_Y;

  case 'Z':
    if (c2 == 'H')
      return PHONEME_ZH;
    else if (c2 == '\0')
      return PHONEME_Z;
    else
      return PHONEME_UNKNOWN;

  default:
    return PHONEME_UNKNOWN;
  }
}

const gchar *
stress2str (gint s)
{
  switch (s) {
  case STRESS_NONE:
    return "0";
  case STRESS_PRIMARY:
    return "1";
  case STRESS_SECONDARY:
    return "2";
  case STRESS_UNKNOWN:
    return "?";
  case STRESS_WILDCARD:
    return "*";
  }

  return "!";
}

gint
char2stress (gchar c)
{
  switch (c) {
  case '0':
    return STRESS_NONE;
  case '1':
    return STRESS_PRIMARY;
  case '2':
    return STRESS_SECONDARY;
  case '*':
    return STRESS_WILDCARD;
  default:
    return STRESS_UNKNOWN;
  }
}

double
stress_penalty (gint have, gint want)
{
  if (   have == want              
      || have == STRESS_WILDCARD
      || want == STRESS_WILDCARD
      || want == STRESS_UNKNOWN) /* Unknown wants get treated like wildcards */
    return 0;

  /* Punish unknown words, but not *too* harshly. */
  if (have == STRESS_UNKNOWN)
    return 3.0;

  if (want == STRESS_NONE) {

    if (have == STRESS_SECONDARY)
      return 1.0;
    else if (have == STRESS_PRIMARY)
      return 3.0;
    else
      g_assert_not_reached ();
  }

  if (want == STRESS_SECONDARY) {
    if (have == STRESS_NONE)
      return 1.0;
    else if (have == STRESS_PRIMARY)
      return 1.0;
    else
      g_assert_not_reached ();
  }

  if (want == STRESS_PRIMARY) {
    if (have == STRESS_NONE)
      return 3.0;
    else if (have == STRESS_SECONDARY)
      return 1.0;
    else
      g_assert_not_reached ();
  }

  g_assert_not_reached ();
  return 100;
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

static void phonetic_info_clear_table (void);
static void phonetic_info_build_table (void);

PhoneticInfo *
phonetic_info_new (const gchar *word, const gchar *spelling)
{
  PhoneticInfo *info;
  gint syl, phon, i, si, pi;
  const gchar *s;
  gchar *spell_copy;
  gchar **spellv;

  g_return_val_if_fail (word != NULL, NULL);
  g_return_val_if_fail (spelling != NULL, NULL);

  while (*spelling && isspace ((gint) *spelling))
    ++spelling;

  info = g_new0 (PhoneticInfo, 1);
  info->word = g_strdup (word);
  g_strdown (info->word);

  /* First, count syllables and phonemes. */
  syl = 0;
  phon = 1;
  s = spelling;
  while (*s) {
    if (isspace ((gint) *s))
      ++phon;
    if (isdigit ((gint) *s))
      ++syl;
    ++s;
  }

  info->syllables = syl;
  info->phonemes = phon;
  info->phoneme = g_new0 (guint8, phon);
  info->stress  = g_new0 (guint16, syl);
  
  spell_copy = g_strdup (spelling);
  spellv = g_strsplit (spell_copy, " ", 0);

  si = 0;
  pi = 0;
  for (i=0; spellv[i]; ++i) {
    gint len = strlen (spellv[i]);

    if (len > 0) {
      gint stress = char2stress (spellv[i][len-1]);
      gint p;

      if (stress != STRESS_UNKNOWN) {
	g_assert (si < syl);
	info->stress[si] = (guint16) ((stress << 8) | (i));
	spellv[i][len-1] = '\0';
	++si;
      }

      p = str2phoneme (spellv[i]);
      if (p == PHONEME_UNKNOWN)
	g_error ("Failed on \"%s\", \"%s\" (%s)", word, spelling, spellv[i]);
      g_assert (pi < phon);
      info->phoneme[pi] = (guint8) p;
      ++pi;
    }
  }
  
  g_free (spell_copy);
  g_free (spellv);
  return info;
}

PhoneticInfo *
phonetic_info_new_plural (const gchar *word, PhoneticInfo *sing)
{
  PhoneticInfo *info;
  gint i;

  g_return_val_if_fail (word != NULL, NULL);
  g_return_val_if_fail (sing != NULL, NULL);
  
  info = g_new0 (PhoneticInfo, 1);
  info->word = g_strdup (word);
  g_strdown (info->word);

  info->syllables = sing->syllables;
  info->phonemes  = sing->phonemes+1;
  info->phoneme   = g_new0 (guint8, info->phonemes);
  info->stress    = g_new0 (guint16, info->syllables);

  for (i = 0; i < sing->phonemes; ++i)
    info->phoneme[i] = sing->phoneme[i];
  info->phoneme[i] = PHONEME_S;

  for (i=0; i < sing->syllables; ++i)
    info->stress[i] = sing->stress[i];

  return info;
}

PhoneticInfo *
phonetic_info_new_stub (const gchar *word)
{
  PhoneticInfo *info;
  gint i;

  g_return_val_if_fail (word != NULL, NULL);

  info = g_new0 (PhoneticInfo, 1);
  info->word = g_strdup (word);
  g_strdown (info->word);
  
  info->syllables = syllable_count_EN (word);
  info->phonemes  = 0;
  info->phoneme   = NULL;
  info->stress    = g_new0 (guint16, info->syllables);
  for (i = 0; i < info->syllables; ++i)
    info->stress[i] = (STRESS_UNKNOWN << 8) | (i);

  return info;
}

gint
phonetic_info_syllables (PhoneticInfo *info)
{
  if (info == NULL)
    return 0;
  return info->syllables;
}

static const gchar* unstressy_forms[] = {
  "the", "of", "and", "or", "in", "a", NULL
};

gint
phonetic_info_syllable_stress (PhoneticInfo *info, gint i)
{
  if (info == NULL || i < 0 || i >= info->syllables)
    return STRESS_UNKNOWN;

  /* One syllable words are seconary. */
  if (info->syllables == 1) {
    gint i=0;
    while (unstressy_forms[i]) {
      if (g_strcasecmp (unstressy_forms[i], info->word) == 0)
	return STRESS_NONE;
      ++i;
    }
    return STRESS_SECONDARY;
  }

  return info->stress[i] >> 8;
}

gint
phonetic_info_syllable_position (PhoneticInfo *info, gint i)
{
  if (info == NULL || i < 0 || i >= info->syllables)
    return -1;
  return info->stress[i] & 0xff;
}

void
phonetic_info_dump (PhoneticInfo *info)
{
  gint i, s=0;

  for (i=0; i<info->phonemes; ++i) {

    if (i)
      g_print (" ");
    g_print (phoneme2str (info->phoneme[i]));

    if (phonetic_info_syllable_position (info, s) == i) {
      g_print (stress2str (phonetic_info_syllable_stress (info, s)));
      ++s;
    }
  }
  g_print ("\n");
}

void
phonetic_info_free (PhoneticInfo *info)
{
  if (info == NULL)
    return;

  g_free (info->word);
  g_free (info->phoneme);
  g_free (info->stress);
  g_free (info);
}

gint
phonetic_info_rhyme_score (PhoneticInfo *a, PhoneticInfo *b)
{
  gint ia, ib, syl;

  g_return_val_if_fail (a != NULL, 0);
  g_return_val_if_fail (b != NULL, 0);

  ia = a->phonemes-1;
  ib = b->phonemes-1;
  if (ia < 0 || ib < 0)
    return 0;

  while (ia >= 0 && ib >= 0 && a->phoneme[ia] == b->phoneme[ib]) {
    --ia;
    --ib;
  }

  syl = 0;
  while (syl < a->syllables 
	 && syl < b->syllables
	 && phonetic_info_syllable_position (a, a->syllables-1-syl) > ia
	 && phonetic_info_syllable_position (b, b->syllables-1-syl) > ib)
    ++syl;

  return syl;
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

static GHashTable *dict_hash = NULL;
static gint dict_size = 0;
static PhoneticInfo **dict_table = NULL;

static gint
phonetic_info_cmp (gconstpointer a, gconstpointer b)
{
  PhoneticInfo *pa = *(PhoneticInfo **) a;
  PhoneticInfo *pb = *(PhoneticInfo **) b;
  gint ia, ib;

  if (pa == NULL && pb == NULL)
    return 0;
  else if (pa == NULL)
    return +1;
  else if (pb == NULL)
    return -1;

  ia = pa->phonemes-1;
  ib = pb->phonemes-1;

  while (ia >= 0 && ib >= 0 && pa->phoneme[ia] == pb->phoneme[ib]) {
    --ia;
    --ib;
  }

  if (ia < 0 && ib < 0)
    return 0;
  else if (ia < 0)
    return +1;
  else if (ib < 0)
    return -1;

  return (pa->phoneme[ia] > pb->phoneme[ib]) - (pa->phoneme[ia] < pb->phoneme[ib]);
}

static void
phonetic_info_register (PhoneticInfo *info)
{
  g_return_if_fail (info != NULL);

  if (dict_hash == NULL)
    dict_hash = g_hash_table_new (g_str_hash, g_str_equal);

  /* Silently skip collisions */
  if (g_hash_table_lookup (dict_hash, info->word))
    return;

  g_hash_table_insert (dict_hash, info->word, info);
  ++dict_size;

  phonetic_info_clear_table ();
}

static void
hash_table_cb (gpointer key, gpointer val, gpointer user_data)
{
  gint* iptr = (gint *) user_data;
  g_return_if_fail (val != NULL);
  dict_table[*iptr] = (PhoneticInfo *) val;
  ++*iptr;
}

static void
phonetic_info_clear_table (void)
{
  g_free (dict_table);
  dict_table = NULL;
}

static void
phonetic_info_build_table (void)
{
  gint i;

  if (dict_table != NULL)
    return;

  dict_table = g_new0 (PhoneticInfo *, dict_size);
  i = 0;
  g_hash_table_foreach (dict_hash, hash_table_cb, &i);

  qsort (dict_table, dict_size, sizeof (PhoneticInfo *), phonetic_info_cmp);
  for (i=0; i<dict_size; ++i)
    dict_table[i]->table_pos = i;
}

static void
interrupted_cb (GtkWidget *w, gpointer user_data)
{
  exit (0);
}


#define DICTFILE "cmudict0.3.gz"
void
phonetic_info_load_dict (void)
{
  GtkWidget *info;
  GtkWidget *prog;
  GtkWidget *box;
  GtkWidget *splash;
  gint N;

  gzFile in = NULL;
  gchar buffer[512];

  if (dict_hash != NULL)
    return;

  info = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_position (GTK_WINDOW (info), GTK_WIN_POS_CENTER);

  gtk_window_set_title (GTK_WINDOW (info), _("Visit www.beardofbees.com"));

  box = gtk_vbox_new (FALSE, 0);
  if (g_file_exists (PIXMAPPATH "/gnoetry-splash.png")) 
    splash = gnome_pixmap_new_from_file (PIXMAPPATH "/gnoetry-splash.png");
  else
    splash = gnome_pixmap_new_from_file ("../pixmaps/gnoetry-splash.png");
  
  gtk_box_pack_start (GTK_BOX (box), splash, TRUE, TRUE, 0);

  prog = gtk_progress_bar_new ();
  gtk_progress_bar_set_bar_style (GTK_PROGRESS_BAR (prog),
				  GTK_PROGRESS_CONTINUOUS);
  gtk_progress_set_format_string (GTK_PROGRESS (prog),
				  "Loading CMU Pronunication Dictionary");
  gtk_progress_set_show_text (GTK_PROGRESS (prog), TRUE);
  gtk_box_pack_start (GTK_BOX (box), prog, TRUE, TRUE, 0);

  gtk_container_add (GTK_CONTAINER (info), box);

  gtk_signal_connect (GTK_OBJECT (info),
		      "delete_event",
		      GTK_SIGNAL_FUNC (interrupted_cb),
		      NULL);

  gtk_widget_show_all (info);

  while (gtk_events_pending ())
    gtk_main_iteration ();

#ifdef DICTPATH
  in = gzopen (DICTPATH "/" DICTFILE, "r");
#endif
  if (in == NULL)
    in = gzopen ("../dict/" DICTFILE, "r");
  
  if (in == NULL)
    g_error ("Can't find CMU Pronunciation Dictionary.");

  N = 0;
  while (gzgets (in, buffer, 512)) {
    gboolean skip = FALSE;
    gchar *spell;

    g_strstrip (buffer);

    ++N;

    skip = buffer[0] == '\0' || buffer[0] == '#' || !isalpha (buffer[0]);
    
    if (!skip) {
      gint i = strlen (buffer)-1;
      while (i >= 0 && !isalnum ((gint) buffer[i])) {
	buffer[i] = '\0';
	--i;
      }
      if (i < 0)
	skip = TRUE;
    }

    if (!skip) {
      gchar *s = buffer;
      while (*s && !skip) {
	if (*s == '(')
	  skip = TRUE;
	++s;
      }
    }
    
    if (!skip) {
      gchar *s;
      for (s = buffer; *s && !isspace ((gint) *s); ++s);
      if (! *s)
	skip = TRUE;
      else {
	*s = '\0';
	spell = s+1;
      }
    }

    if (!skip) {
      PhoneticInfo *info = phonetic_info_new (buffer, spell);
      if (info != NULL)
	phonetic_info_register (info);

      if ((N & 0xff) == 0) {
	/* I've hard-wired the length of the CMU dict in here, which is a
	   pretty silly thing to do. */
	gtk_progress_set_percentage (GTK_PROGRESS (prog), 
				     CLAMP (N / (110892.0-0xff), 0, 1));
	while (gtk_events_pending ())
	  gtk_main_iteration ();
      }

    }

  }

  phonetic_info_build_table ();

  gtk_widget_destroy (info);
  while (gtk_events_pending ())
    gtk_main_iteration ();
}


static void
clear_list_cb (gpointer key, gpointer val, gpointer user_data)
{
  PhoneticInfo *info = (PhoneticInfo *) val;

  g_list_free (info->tokens);
  info->tokens = NULL;
}

void
phonetic_info_clear_token_lists (void)
{
  g_hash_table_foreach (dict_hash, clear_list_cb, NULL);
}


PhoneticInfo *
phonetic_info_lookup (const gchar *word)
{
  gchar *wc = NULL;
  gpointer ptr;
  gint len;
  gboolean apo = FALSE;

  g_return_val_if_fail (word, NULL);

  wc = g_strdup (word);
  g_strdown (wc);

  if (dict_hash == NULL)
    phonetic_info_load_dict ();

  ptr = g_hash_table_lookup (dict_hash, wc);

  if (ptr == NULL) {

    /* Attempt to "de-pluralize" and try again. */
    len = strlen (wc);
    if (len > 0 && wc[len-1] == 's') {
      if (len > 1 && wc[len-2] == '\'') {
	wc[len-2] = '\0';
	apo = TRUE;
      } else {
	wc[len-1] = '\0';
      }
    }

    ptr = g_hash_table_lookup (dict_hash, wc);
    if (ptr) {
      ptr = phonetic_info_new_plural (word, (PhoneticInfo *) ptr);
      if (ptr)
	phonetic_info_register ((PhoneticInfo *) ptr);
    }
  }

  if (ptr == NULL) {
    ptr = phonetic_info_new_stub (word);
  }

  g_free (wc);
  return (PhoneticInfo *)ptr;
}

gpointer
phonetic_info_choose_rhyme_token (PhoneticInfo *p)
{
  gint i, j, a, b, choices=0, pick;

  if (p == NULL || p->phoneme == NULL)
    return NULL;

  a=p->table_pos+1;
  b=p->table_pos-1;

  phonetic_info_build_table ();

  j=p->table_pos-1;
  while (j >= 0 && phonetic_info_rhyme_score (p, dict_table[j]) > 0) {
    if (dict_table[j]->tokens) {
      ++choices;
      a = j;
    }
    --j;
  }

  j = p->table_pos+1;
  while (j < dict_size && phonetic_info_rhyme_score (p, dict_table[j]) > 0) {
    if (dict_table[j]->tokens) {
      ++choices;
      b = j;
    }
    ++j;
  }

  if (choices == 0 || a > b)
    return NULL;

  pick = fate_spin_wheel (choices);
  for (i=a; i <= b && pick >= 0; ++i) {
    if (i != p->table_pos && dict_table[i]->tokens) {
      if (pick == 0) {
	choices = g_list_length (dict_table[i]->tokens);
	g_assert (choices > 0);
	pick = fate_spin_wheel (choices);
	return g_list_nth_data (dict_table[i]->tokens, pick);
      }
      --pick;
    }
  }

  g_warning ("Fell through rhyming loop!");

  return NULL;
}


/* $Id$ */
