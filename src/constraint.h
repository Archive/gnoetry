/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * constraint.h
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __CONSTRAINT_H__
#define __CONSTRAINT_H__

#include <glib.h>
#include "words.h"

typedef enum {
  CONSTRAINT_UNKNOWN,
  CONSTRAINT_RHYME,
  CONSTRAINT_DUPLICATE,
  CONSTRAINT_LAST
} ConstraintType;

typedef struct _Constraint Constraint;
struct _Constraint {
  ConstraintType type;
  gchar *tag;
  TokenInfo *binding;
};

Constraint     *constraint_new      (ConstraintType, const gchar *tag);
void            constraint_free     (Constraint *);

ConstraintType  constraint_type     (Constraint *);
const gchar    *constraint_tag      (Constraint *);
TokenInfo      *constraint_binding  (Constraint *);
gboolean        constraint_is_bound (Constraint *);
gboolean        constraint_is_free  (Constraint *);
void            constraint_clear    (Constraint *);
void            constraint_bind     (Constraint *, TokenInfo *);
TokenInfo      *constraint_solution (Constraint *);



typedef struct _ConstraintManager ConstraintManager;
struct _ConstraintManager {
  GHashTable *table;
  ConstraintManager *outer;
};

ConstraintManager *constraint_manager_new (void);
ConstraintManager *constraint_manager_new_with_outer_frame (ConstraintManager *);
void               constraint_manager_free (ConstraintManager *);

gboolean           constraint_manager_contains (ConstraintManager *,
						const gchar *tag);
Constraint        *constraint_manager_get      (ConstraintManager *,
						const gchar *tag);
void               constraint_manager_add      (ConstraintManager *,
						Constraint *);

#endif /* __CONSTRAINT_H__ */

