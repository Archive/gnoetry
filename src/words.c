/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * words.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>
#include "fate.h"
#include "words.h"
#include "syllables.h"

#define TOKEN_ERROR_TEXT        "<not-a-token>"
#define TOKEN_START_TEXT        "<start>"
#define TOKEN_STOP_TEXT         "<stop>"

/* The rest of this is broken. */
#define TOKEN_PERIOD_TEXT       "|."
#define TOKEN_EXCLAMATION_TEXT  "|!"
#define TOKEN_QUESTION_TEXT     "|?"
#define TOKEN_ELLIPSES_TEXT     "|..."
#define TOKEN_DASH_TEXT         "---"
#define TOKEN_COMMA_TEXT        "|,"
#define TOKEN_COLON_TEXT        "|:"
#define TOKEN_SEMICOLON_TEXT    "|;"

token_t token_next_code = 0;
static GHashTable *token_word_table = NULL;
static GHashTable *token_code_table = NULL;

static void
free_token_info_iter (gpointer key, gpointer val, gpointer user_data)
{
  TokenInfo *info = (TokenInfo *)val;

  g_free (info->word);
  info->word = NULL;
  
  g_free (info->predv);
  g_free (info->succv);
  
  info->predv = info->succv = NULL;

  g_free (info);
}

void
reset_token_info (void)
{
  if (token_word_table == NULL && token_code_table == NULL)
    return;

  g_hash_table_foreach (token_word_table, free_token_info_iter, NULL);

  g_hash_table_destroy (token_word_table);
  g_hash_table_destroy (token_code_table);

  token_word_table = NULL;
  token_code_table = NULL;

  token_next_code = 0;

  phonetic_info_clear_token_lists ();
}

static TokenInfo *
add_token (const gchar *word, token_t code)
{
  TokenInfo *info;
  gchar *clean_word, **cleanv;
  const gchar *src;
  gchar *dest;
  gint i;
  gboolean last_was_space = TRUE;

  g_return_val_if_fail (word != NULL, NULL);
  g_return_val_if_fail (get_token_info_by_code (code) == NULL, NULL);

  info = g_new0 (TokenInfo, 1);
  info->code = code;
  info->word = g_strdup (word);

  if (info->code >= TOKEN_LAST) {
    clean_word = g_malloc0 (strlen (word)+1);
    src = word;
    dest = clean_word;
    while (*src) {
      gboolean is_space = isspace ((gint) *src);
      if ((is_space && !last_was_space) || (isalnum ((gint) *src))) {
	*dest = is_space ? ' ' : *src;
	++dest;
	last_was_space = is_space;
      }
      ++src;
    }
    g_strstrip (clean_word);
    if (clean_word[0] != '\0') {
    
      cleanv = g_strsplit (clean_word, " ", 0);
      for (i=0; cleanv[i]; ++i) {
	PhoneticInfo *pi = phonetic_info_lookup (cleanv[i]);
	if (pi == NULL)
	  g_print ("NULL on %s\n", cleanv[i]);
	else {
	  info->phonetics = g_list_append (info->phonetics, pi);
	  info->root_phonetics = pi;
	  ++info->words;
	  info->syllables += pi->syllables;
	}
      }
      if (i != 0)
	info->root_word = g_strdup (cleanv[i-1]);
      
      if (info->root_phonetics == NULL) 
	g_print ("empty on %s\n", info->word);
      
      info->root_phonetics->tokens = g_list_prepend (info->root_phonetics->tokens, info);
      
      g_free (clean_word);
      g_free (cleanv);
    }
  }

  if (token_next_code <= code)
    token_next_code = code+1;

  g_hash_table_insert (token_word_table, info->word, info);
  g_hash_table_insert (token_code_table, &info->code, info);

  return info;
}

static guint
g_str_nocase_hash (gconstpointer x)
{
  guint hash;
  gchar *str = g_strdup ((const gchar *) x);
  g_strdown (str);
  hash = g_str_hash (str);
  g_free (str);

  return hash;
}

static gint
g_str_nocase_equal (gconstpointer a, gconstpointer b)
{
  gchar *sa = g_strdup ((const gchar *) a);
  gchar *sb = g_strdup ((const gchar *) b);
  gint cmp;

  g_strdown (sa);
  g_strdown (sb);

  cmp = g_str_equal (sa, sb);

  g_free (sa);
  g_free (sb);

  return cmp;
}


static void
init_tables (void)
{
  if (token_word_table == NULL && token_code_table == NULL) {

    token_word_table = g_hash_table_new (g_str_nocase_hash,
					 g_str_nocase_equal);
    token_code_table = g_hash_table_new (g_int_hash, g_int_equal);
    g_assert (token_word_table && token_code_table);

    /* Add all of our punctuation/syntactic tokens to the table. */
    add_token (TOKEN_ERROR_TEXT, TOKEN_ERROR);
    add_token (TOKEN_START_TEXT, TOKEN_START);
    add_token (TOKEN_STOP_TEXT, TOKEN_STOP);
    add_token (TOKEN_PERIOD_TEXT, TOKEN_PERIOD);
    add_token (TOKEN_EXCLAMATION_TEXT, TOKEN_EXCLAMATION);
    add_token (TOKEN_QUESTION_TEXT, TOKEN_QUESTION);
    add_token (TOKEN_ELLIPSES_TEXT, TOKEN_ELLIPSES);
    add_token (TOKEN_DASH_TEXT, TOKEN_DASH);
    add_token (TOKEN_COMMA_TEXT, TOKEN_COMMA);
    add_token (TOKEN_COLON_TEXT, TOKEN_COLON);
    add_token (TOKEN_SEMICOLON_TEXT, TOKEN_SEMICOLON);
  }
}

TokenInfo *
declare_token (const gchar *word)
{
  TokenInfo *info;
  g_return_val_if_fail (word != NULL, NULL);

  if (token_code_table == NULL || token_word_table == NULL)
    init_tables ();

  info = get_token_info_by_word (word);
  if (info == NULL) {

    info = add_token (word, token_next_code);
    
  }

  return info;
}

void
declare_paring (TokenInfo *first, TokenInfo *second)
{
  g_return_if_fail (first && second);

  if (first->succ_count >= first->succ_sz) {
    first->succ_sz = first->succ_sz ? 2*first->succ_sz : 32;
    first->succv = g_renew (TokenInfo *, first->succv, first->succ_sz);
  }

  if (second->pred_count >= second->pred_sz) {
    second->pred_sz = second->pred_sz ? 2*second->pred_sz : 32;
    second->predv = g_renew (TokenInfo *, second->predv, second->pred_sz);
  }

  first->succv[first->succ_count] = second;
  second->predv[second->pred_count] = first;

  ++first->succ_count;
  ++second->pred_count;
}

TokenInfo *
get_token_info_by_code (token_t code)
{
  TokenInfo *info;
  gint cc = code;

  if (token_code_table == NULL || code >= token_next_code)
    return NULL;

  info = (TokenInfo *)g_hash_table_lookup (token_code_table, &cc);

  /* Sanity check */
  if (info) {
    g_assert (info->code == code);
  }

  return info;
}

TokenInfo *
get_token_info_by_word (const gchar *word)
{
  TokenInfo *info;
  gchar *w;

  if (token_word_table == NULL)
    return NULL;

  w = g_strdup (word);

  info = (TokenInfo *)g_hash_table_lookup (token_word_table, w);

  /* Sanity check */
  if (info && g_strcasecmp (w, info->word)) {
    g_warning ("[%s] vs [%s]", w, info->word);
  }

  g_free (w);


  return info;
}

token_t
token_info_code (TokenInfo *info)
{
  if (info == NULL)
    return TOKEN_ERROR;

  return info->code;
}

const gchar *
token_info_word (TokenInfo *info)
{
  g_return_val_if_fail (info, NULL);

  return info->word;
}

gint
token_info_syllables (TokenInfo *info)
{
  g_return_val_if_fail (info, -1);

  return info->syllables;
}

TokenInfo *
token_info_choose_predecessor (TokenInfo *info)
{
  guint i;

  g_return_val_if_fail (info, NULL);

  i = fate_spin_wheel (info->pred_count);
  return info->predv[i];
}

TokenInfo *
token_info_choose_successor (TokenInfo *info)
{
  guint i;

  g_return_val_if_fail (info, NULL);

  i = fate_spin_wheel (info->succ_count);
  return info->succv[i];
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

gboolean
token_info_has_rhymes (TokenInfo *info)
{
  /* Sort of a silly way to do this... */
  return token_info_choose_rhyme (info) != NULL;
}

TokenInfo *
token_info_choose_rhyme (TokenInfo *info)
{
  g_return_val_if_fail (info != NULL, NULL);
  return (TokenInfo *) phonetic_info_choose_rhyme_token (info->root_phonetics);
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

struct tokforeach {
  void (*fn) (TokenInfo *, gpointer);
  gpointer user_data;
};

static void
hash_iter (gpointer key, gpointer val, gpointer user_data)
{
  struct tokforeach *tfe = (struct tokforeach *)user_data;
  tfe->fn ( (TokenInfo *)val, tfe->user_data);
}

void
token_info_foreach (void (*fn) (TokenInfo *, gpointer),
		    gpointer user_data)
{
  struct tokforeach tfe = { fn, user_data };

  g_hash_table_foreach (token_word_table, hash_iter, &tfe);
}


/* $Id$ */
