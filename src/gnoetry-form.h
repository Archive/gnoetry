/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * gnoetry-form.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GNOETRY_FORM_H
#define _INC_GNOETRY_FORM_H

#include <gnome.h>
#include "gnoetry-line.h"

typedef struct _GnoetryForm GnoetryForm;
typedef struct _GnoetryFormClass GnoetryFormClass;

struct _GnoetryForm {
  GtkObject parent;

  gchar *form_name;
  gchar *form_params;

  GList *lines;
};

struct _GnoetryFormClass {
  GtkObjectClass parent_class;

  void (*changed) (GnoetryForm *);
};

#define GNOETRY_TYPE_FORM (gnoetry_form_get_type ())
#define GNOETRY_FORM(obj) (GTK_CHECK_CAST((obj),GNOETRY_TYPE_FORM,GnoetryForm))
#define GNOETRY_FORM0(obj) ((obj) ? (GNOETRY_FORM(obj)) : NULL)
#define GNOETRY_FORM_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GNOETRY_TYPE_FORM,GnoetryFormClass))
#define GNOETRY_IS_FORM(obj) (GTK_CHECK_TYPE((obj), GNOETRY_TYPE_FORM))
#define GNOETRY_IS_FORM0(obj) (((obj) == NULL) || (GNOETRY_IS_FORM(obj)))
#define GNOETRY_IS_FORM_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GNOETRY_TYPE_FORM))

#define g_return_unless_is_gnoetry_form(x) (g_return_if_fail((x)&&GNOETRY_IS_FORM((x))))
#define g_return_val_unless_is_gnoetry_form(x,y) (g_return_val_if_fail((x)&&GNOETRY_IS_FORM((x)),(y)))

GtkType gnoetry_form_get_type (void);

GnoetryForm *gnoetry_form_new (void);

gint gnoetry_form_lines (GnoetryForm *);
gint gnoetry_form_breaks (GnoetryForm *);

void gnoetry_form_set_id (GnoetryForm *,
			  const gchar *name, const gchar *params);

const gchar *gnoetry_form_name (GnoetryForm *);
const gchar *gnoetry_form_params (GnoetryForm *);

void gnoetry_form_clear (GnoetryForm *);

void gnoetry_form_append_line (GnoetryForm *, GnoetryLine *);
void gnoetry_form_append_break (GnoetryForm *);

void gnoetry_form_recalc (GnoetryForm *);

void gnoetry_form_foreach_line (GnoetryForm *,
				void (*fn)(GnoetryLine *, gpointer),
				gpointer user_data);

gchar *gnoetry_form_multiline_string (GnoetryForm *);
gchar **gnoetry_form_strv (GnoetryForm *);
gchar *gnoetry_form_html (GnoetryForm *);

void gnoetry_form_save_as_latex (GnoetryForm *);

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

GnoetryForm *gnoetry_form_new_haiku (void);
GnoetryForm *gnoetry_form_new_tanka (void);
GnoetryForm *gnoetry_form_new_renga (void);

GnoetryForm *gnoetry_form_new_5line (void);

GnoetryForm *gnoetry_form_new_ABAB (void);
GnoetryForm *gnoetry_form_new_ABAB_CDCD (void);

GnoetryForm *gnoetry_form_new_sonnet (void);
GnoetryForm *gnoetry_form_new_sapphic (void);
GnoetryForm *gnoetry_form_new_hendecasyllabic (void);

GnoetryForm *gnoetry_form_new_blank_verse (gint stanzas, gint lines_per_stanza);
GnoetryForm *gnoetry_form_new_blank_verse_4x4 (void);

GnoetryForm *gnoetry_form_new_italian_sestet (void);
GnoetryForm *gnoetry_form_new_sicilian_sestet (void);
GnoetryForm *gnoetry_form_new_heroic_sestet (void);

GnoetryForm *gnoetry_form_new_sestina (void);

GnoetryForm *gnoetry_form_new_rant (void);

#endif /* _INC_GNOETRY_FORM_H */

/* $Id$ */
