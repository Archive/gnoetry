/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * gnoetry-text-info-viewer.h
 *
 * Copyright (C) 2001 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GNOETRY_TEXT_INFO_VIEWER_H
#define _INC_GNOETRY_TEXT_INFO_VIEWER_H

#include <gnome.h>
#include <gnoetry-defs.h>

BEGIN_GNOETRY_DECLS;

typedef struct _GnoetryTextInfoViewer GnoetryTextInfoViewer;
typedef struct _GnoetryTextInfoViewerClass GnoetryTextInfoViewerClass;

struct _GnoetryTextInfoViewer {
  GtkHBox parent;
};

struct _GnoetryTextInfoViewerClass {
  GtkHBoxClass parent_class;
};

#define GNOETRY_TYPE_TEXT_INFO_VIEWER (gnoetry_text_info_viewer_get_type ())
#define GNOETRY_TEXT_INFO_VIEWER(obj) (GTK_CHECK_CAST((obj),GNOETRY_TYPE_TEXT_INFO_VIEWER,GnoetryTextInfoViewer))
#define GNOETRY_TEXT_INFO_VIEWER0(obj) ((obj) ? (GNOETRY_TEXT_INFO_VIEWER(obj)) : NULL)
#define GNOETRY_TEXT_INFO_VIEWER_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GNOETRY_TYPE_TEXT_INFO_VIEWER,GnoetryTextInfoViewerClass))
#define GNOETRY_IS_TEXT_INFO_VIEWER(obj) (GTK_CHECK_TYPE((obj), GNOETRY_TYPE_TEXT_INFO_VIEWER))
#define GNOETRY_IS_TEXT_INFO_VIEWER0(obj) (((obj) == NULL) || (GNOETRY_IS_TEXT_INFO_VIEWER(obj)))
#define GNOETRY_IS_TEXT_INFO_VIEWER_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GNOETRY_TYPE_TEXT_INFO_VIEWER))

#define g_return_unless_is_gnoetry_text_info_viewer(x) (g_return_if_fail((x)&&GNOETRY_IS_TEXT_INFO_VIEWER((x))))
#define g_return_val_unless_is_gnoetry_text_info_viewer(x,y) (g_return_val_if_fail((x)&&GNOETRY_IS_TEXT_INFO_VIEWER((x)),(y)))

GtkType gnoetry_text_info_viewer_get_type (void);

GtkWidget *gnoetry_text_info_viewer_new (void);



END_GNOETRY_DECLS;

#endif /* _INC_GNOETRY_TEXT_INFO_VIEWER_H */

/* $Id$ */
