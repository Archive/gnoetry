/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * line-spec.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <string.h>
#include "line-spec.h"

static LineConstaint *
line_constraint_new (const gchar *tag, ConstraintType constraint,
		     gint pos, LinePosType pos_type)
{
  LineConstaint *lc = g_new0 (LineConstaint, 1);

  lc->tag = g_strdup (tag);
  lc->constraint = constraint;
  lc->pos = pos;
  lc->pos_type = pos_type;

  return lc;
}

LineConstaint *
line_constraint_new_final_rhyme (const gchar *tag)
{
  return line_constraint_new (tag, CONSTRAINT_RHYME, -1, LINE_POS_WORD);
}

LineConstaint *
line_constraint_new_final_duplicate (const gchar *tag)
{
  return line_constraint_new (tag, CONSTRAINT_DUPLICATE, -1, LINE_POS_WORD);
}

void
line_constraint_free (LineConstaint *lc)
{
  if (lc) {
    g_free (lc->tag);
    g_free (lc);
  }
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

LineSpec *
line_spec_new (void)
{
  LineSpec *spec = g_new0 (LineSpec, 1);

  return spec;
}

void
line_spec_free (LineSpec *spec)
{
  if (spec) {

    line_spec_clear_constraints (spec);
    g_free (spec->meter);

    g_free (spec);
  }
}

void
line_spec_set_word_limits (LineSpec *spec, gint min, gint max)
{
  g_return_if_fail (spec);
  spec->min_words = min;
  spec->max_words = max;
}

void
line_spec_set_syllable_limits (LineSpec *spec, gint min, gint max)
{
  g_return_if_fail (spec);
  spec->min_syllables = min;
  spec->max_syllables = max;
}

void
line_spec_set_meter (LineSpec *spec, const gchar *meter)
{
  const gchar *c;
  g_return_if_fail (spec);

  c = meter;
  while (c && *c) {
    if (*c != HARD_STRESS
	&& *c != MEDIUM_STRESS
	&& *c != NO_STRESS
	&& *c != ANY_STRESS) {
      g_warning ("Invalid meter template: \"%s\"", meter);
      return;
    }
      
    ++c;
  }

  g_free (spec->meter);
  spec->meter = g_strdup (meter);
  spec->min_syllables = meter ? strlen (meter) : 0;
  spec->max_syllables = spec->min_syllables;
}

void
line_spec_add_constraint (LineSpec *spec, LineConstaint *con)
{
  g_return_if_fail (spec);
  g_return_if_fail (con);
  spec->constraints = g_list_prepend (spec->constraints, con);
}

void
line_spec_clear_constraints (LineSpec *spec)
{
  g_return_if_fail (spec);
  g_list_foreach (spec->constraints, (GFunc) line_constraint_free, NULL);
  g_list_free (spec->constraints);
  spec->constraints = NULL;
}
		     
