/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * gnoetry.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <gnome.h>
#include "fate.h"
#include "phonetics.h"
#include "words.h"
#include "phrase.h"
#include "text.h"
#include "text-dialog.h"

#include "gnoetry-line.h"
#include "gnoetry-form.h"

#include "compose-druid.h"

#include "gnoetry-viewer.h"

static void
exit_cb (void)
{
  gtk_main_quit ();
}

static void
about_cb (GtkWidget *w, gpointer data)
{
  static GtkWidget *about = NULL;

  const gchar* authors[] = { "Jon Trowbridge <trow@gnu.org>",
			     "Eric Elshtain <epelshta@uchicago.edu>",
			     NULL };
  
  if (about == NULL) {

    about = gnome_about_new (PACKAGE, VERSION,
			     "Copyright (C) 2001 The Free Software Foundation",
			     authors, 
			     "Gnoetry now offers epodes to regular yokels.",
			     NULL);

    gnome_dialog_set_close (GNOME_DIALOG (about), TRUE);

    gtk_signal_connect (GTK_OBJECT (about),
			"destroy",
			GTK_SIGNAL_FUNC (gtk_widget_destroyed),
			&about);

    gnome_dialog_set_parent (GNOME_DIALOG (about), GTK_WINDOW (data));

    gtk_widget_show_all (about);

  } else {

    gdk_window_show (about->window);
    gdk_window_raise (about->window);

  }

}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

static const gchar* about_ubu_text[] = {
  N_("As the True portrait of Monsieur Ubu shows, he is the cause of all of our sorrows over and all of our revolts against biological survival."),
  N_("Pere Ubu is a human metabolism fuelled by near-scientific speculations."),
  N_("Ubu Roi says that in his country no state of being excludes its opposite; everything is not only possible but real.  \"Pschitt!\""),
  NULL };

#define ABOUT_UBU_IMG "ubu-roi-about.png"

static void
about_ubu_cb (GtkWidget *w, gpointer data)
{
  static GtkWidget *about = NULL;

  if (about == NULL) {
    GtkWidget *ubu_box;
    GtkWidget *ubu_text_box;
    GtkWidget *ubu_image = NULL;
    gint i;

    about = gnome_dialog_new (_("About Ubu"),
			      GNOME_STOCK_BUTTON_OK,
			      NULL);

    gtk_signal_connect (GTK_OBJECT (about),
			"destroy",
			GTK_SIGNAL_FUNC (gtk_widget_destroyed),
			&about);

    gnome_dialog_set_close (GNOME_DIALOG (about), TRUE);

    ubu_box = gtk_hbox_new (FALSE, 0);
    ubu_text_box = gtk_vbox_new (FALSE, 4);

    for (i=0; about_ubu_text[i]; ++i) {
      GtkWidget *ubu_text;

      ubu_text = gtk_label_new (_(about_ubu_text[i]));
      gtk_label_set_line_wrap (GTK_LABEL (ubu_text), TRUE);
      gtk_misc_set_alignment (GTK_MISC (ubu_text), 0, 0.5);
      gtk_box_pack_start (GTK_BOX (ubu_text_box), ubu_text,
			TRUE, TRUE, 3);
    }
    

    if (g_file_exists (PIXMAPPATH "/" ABOUT_UBU_IMG))
      ubu_image = gnome_pixmap_new_from_file (PIXMAPPATH "/" ABOUT_UBU_IMG);
    else if (g_file_exists ("../pixmaps/" ABOUT_UBU_IMG))
      ubu_image = gnome_pixmap_new_from_file ("../pixmaps/" ABOUT_UBU_IMG);

    if (ubu_image) 
      gtk_box_pack_start (GTK_BOX (ubu_box), ubu_image,
			  FALSE, FALSE, 5);

    gtk_box_pack_end (GTK_BOX (ubu_box), ubu_text_box,
			FALSE, FALSE, 5);

    gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (about)->vbox),
			ubu_box, TRUE, TRUE, 0);

    gtk_widget_show_all (about);

  } else {

    gdk_window_show (about->window);
    gdk_window_raise (about->window);

  }
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

static const gchar* about_jarry_text[] = {
  N_("Alfred Jarry (1873-1907)"),
  "",
  N_("Poet, Playwright, Novelist, Renowned Libertine, Madman, Philosopher-King and Father of the Science of Pataphysics."),
  NULL };

#define ABOUT_JARRY_IMG "jarry.png"

static void
about_jarry_cb (GtkWidget *w, gpointer data)
{
  static GtkWidget *about = NULL;

  if (about == NULL) {
    GtkWidget *jarry_box;
    GtkWidget *jarry_text_box;
    GtkWidget *jarry_image = NULL;
    gint i;

    about = gnome_dialog_new (_("About Alfred Jarry"),
			      GNOME_STOCK_BUTTON_OK,
			      NULL);

    gtk_signal_connect (GTK_OBJECT (about),
			"destroy",
			GTK_SIGNAL_FUNC (gtk_widget_destroyed),
			&about);

    gnome_dialog_set_close (GNOME_DIALOG (about), TRUE);

    jarry_box = gtk_hbox_new (FALSE, 0);
    jarry_text_box = gtk_vbox_new (FALSE, 4);

    gtk_box_pack_start (GTK_BOX (jarry_text_box), gtk_label_new (""),
			TRUE, TRUE, 0);

    for (i=0; about_jarry_text[i]; ++i) {
      GtkWidget *jarry_text;

      jarry_text = gtk_label_new (_(about_jarry_text[i]));
      gtk_label_set_line_wrap (GTK_LABEL (jarry_text), TRUE);
      gtk_misc_set_alignment (GTK_MISC (jarry_text), 0, 0.5);
      gtk_box_pack_start (GTK_BOX (jarry_text_box), jarry_text,
			FALSE, FALSE, 3);
    }

    gtk_box_pack_start (GTK_BOX (jarry_text_box), gtk_label_new (""),
			TRUE, TRUE, 0);
    

    if (g_file_exists (PIXMAPPATH "/" ABOUT_JARRY_IMG))
      jarry_image = gnome_pixmap_new_from_file (PIXMAPPATH "/" ABOUT_JARRY_IMG);
    else if (g_file_exists ("../pixmaps/" ABOUT_JARRY_IMG))
      jarry_image = gnome_pixmap_new_from_file ("../pixmaps/" ABOUT_JARRY_IMG);

    if (jarry_image) 
      gtk_box_pack_start (GTK_BOX (jarry_box), jarry_image,
			  FALSE, FALSE, 5);

    gtk_box_pack_end (GTK_BOX (jarry_box), jarry_text_box,
			FALSE, FALSE, 5);

    gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (about)->vbox),
			jarry_box, TRUE, TRUE, 0);

    gtk_widget_show_all (about);

  } else {

    gdk_window_show (about->window);
    gdk_window_raise (about->window);

  }
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

static void
change_texts_cb (GtkWidget *w, gpointer data)
{
  show_text_dialog ();
}

static void
compose_cb (GtkWidget *w, gpointer data)
{
  GtkWidget *druid;
  gpointer ptr;

  ptr = gtk_object_get_data (GTK_OBJECT (data), "viewer");

  druid = compose_druid_new (ptr ? GNOETRY_VIEWER (ptr) : NULL);
  gtk_widget_show_all (druid);
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

static GnomeUIInfo file_menu[] = {
  GNOMEUIINFO_MENU_EXIT_ITEM (exit_cb, NULL),
  GNOMEUIINFO_END
};

static GnomeUIInfo compose_menu[] = {

  {GNOME_APP_UI_ITEM, N_("Compose a Poem"), N_("Compose a Poem"),
   compose_cb, NULL, NULL, GNOME_APP_PIXMAP_NONE, NULL,
   0, (GdkModifierType) 0, NULL},

  GNOMEUIINFO_SEPARATOR,

  {GNOME_APP_UI_ITEM, N_("Change Source Texts"), N_("Change Source Texts"),
   change_texts_cb, NULL, NULL, GNOME_APP_PIXMAP_NONE, NULL,
   0, (GdkModifierType) 0, NULL},

  GNOMEUIINFO_END
};

static GnomeUIInfo help_menu[] = {
  GNOMEUIINFO_MENU_NEW_ITEM (N_("About Gnoetry"),
			     N_("About Gnoetry"),
			     about_cb, NULL),
  GNOMEUIINFO_MENU_NEW_ITEM (N_("About Ubu"),
			     N_("About Ubu"),
			     about_ubu_cb, NULL),
  GNOMEUIINFO_MENU_NEW_ITEM (N_("About Alfred Jarry"),
			     N_("About Alfred Jarry"),
			     about_jarry_cb, NULL),
  GNOMEUIINFO_END
};

static GnomeUIInfo main_menu[] = {
  GNOMEUIINFO_SUBTREE (N_("_File"), file_menu),
  GNOMEUIINFO_SUBTREE (N_("_Compose"), compose_menu),
  GNOMEUIINFO_SUBTREE (N_("_Help"), help_menu),
  GNOMEUIINFO_END
};

static GtkWidget *
main_window (void)
{
  GnomeApp *app;
  GtkWidget *view;

  app = GNOME_APP (gnome_app_new (PACKAGE, PACKAGE));

  gnome_app_create_menus_with_data (app, main_menu, app);
  /* gnome_app_install_menu_hints (app, main_menu); */

  gtk_signal_connect (GTK_OBJECT (app),
		      "delete_event",
		      GTK_SIGNAL_FUNC (gtk_main_quit),
		      NULL);

  view = gnoetry_viewer_new ();
  gnome_app_set_contents (app, view);

  gtk_object_set_data (GTK_OBJECT (app), "viewer", view);

  return GTK_WIDGET (app);
}

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

static void
gnoetry_init (void)
{
  const gchar *path;
    
  phonetic_info_load_dict ();

  if (g_file_exists (TEXTPATH))
    path = TEXTPATH;
  else 
    path = "../texts";

  text_info_locate_files (path);

  if (text_info_count () == 0) {
    g_message ("No source texts found!");
    exit (-1);
  }
  
  show_text_dialog ();
}

#define GNOETRY_HACKING_HACK
static void
hacking (void)
{
  gint i;

  for (i=0; i<10; ++i) {
    Phrase *p1 = NULL, *p2 = NULL, *p3 = NULL;
    
    p1 = phrase_new_successor (NULL, -1, -1, 5, 5, PHRASE_FLAG_NONE);
    if (p1)
      p2 = phrase_new_successor (p1, -1, -1, 7, 7, PHRASE_FLAG_NONE);
    if (p2)
      p3 = phrase_new_successor (p2, -1, -1, 5, 5,
				 PHRASE_FLAG_FORCE_TAIL);
    
    if (p1 && p2 && p3) {
      g_print ("%s\n", phrase_to_string (p1));
      g_print ("%s\n", phrase_to_string (p2));
      g_print ("%s\n", phrase_to_string (p3));
    } else {
      g_print ("MISS!");
    }
    
    g_print ("\n\n");
    
  }
}

int
main (int argc, char *argv[])
{
  GtkWidget *window;

  gnome_init (PACKAGE, VERSION, argc, argv);

  fate_surrender ();
  gnoetry_init ();

#ifdef GNOETRY_HACKING_HACK
  if (getenv ("GNOETRY_HACKING")) {
    hacking ();
    exit (0);
  }
#endif

  window = main_window ();
  g_assert (window);

  gtk_widget_set_usize (window, 480, 240);
  gtk_widget_show_all (window);

  gtk_main ();

  return 0;
}


/* $Id$ */
